/* OpenVAS Administrator
 * $Id$
 * Description: Module for Administrator: the Admin library.
 *
 * Authors:
 * Matthew Mundell <matthew.mundell@greenbone.net>
 * Michael Wiegand <michael.wiegand@greenbone.net>
 *
 * Copyright:
 * Copyright (C) 2009,2010 Greenbone Networks GmbH
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * or, at your option, any later version as published by the Free
 * Software Foundation
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file  admin.c
 * @brief The OpenVAS administration library.
 *
 * This file defines an administration library, for building OpenVAS server
 * administration clients.
 */

#include "admin.h"

/** @todo Either use assert or g_assert, instead of mixing them. */

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <gcrypt.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>

#include <openvas/base/openvas_file.h>
#include <openvas/base/settings.h>
#include <openvas/misc/openvas_auth.h>
#include <openvas/misc/openvas_uuid.h>
#include <openvas/base/pwpolicy.h>

#ifdef S_SPLINT_S
#include "splint.h"
#endif

#undef G_LOG_DOMAIN
/**
 * @brief GLib log domain.
 */
#define G_LOG_DOMAIN "ad  admin"

#define RULES_HEADER "# This file is managed by the OpenVAS Administrator.\n# Any modifications must keep to the format that the Administrator expects.\n"

/** @todo Add contention handling at this level. */


/* Everything else. */

/**
 * @brief Convenience function to produce XML output from user list items.
 *
 * This function is used by g_slist_foreach().
 *
 * @param[in]  data       The item's data.
 * @param[in]  user_data  User data passed to g_slist_foreach().
 */
static void
print_user (void *data, void *user_data)
{
  gchar *xml;
  xml =
    g_markup_printf_escaped ("<user><name>%s</name><rules/></user>",
                             (gchar *) data);
  g_string_append ((GString *) user_data, xml);
  g_free (xml);
}

/**
 * @brief Convenience function to produce text output from user list items.
 * This function is used by g_slist_foreach().
 *
 * @param[in]  data       The item's data.
 * @param[in]  user_data  User data passed to g_slist_foreach().
 */
static void
print_user_text (void *data, void *user_data)
{
  gchar *xml;
  xml = g_markup_printf_escaped ("%s\n", (gchar *) data);
  g_string_append ((GString *) user_data, xml);
  g_free (xml);
}

/**
 * @brief Descending strcmp.
 *
 * @param[in]  one  First string.
 * @param[in]  two  Second string.
 *
 * @return Negation of return from strcmp on args.
 */
static int
strcmp_desc (const char *one, const char *two)
{
  return -strcmp (one, two);
}

/**
 * @brief Returns list of user directories (= users) found in given directory.
 *
 * @param[in]  directory  The complete name of the directory.
 * @param[in]  ascending  Ascending order if true, descending order if 0.
 * @param[in]  name       Name of single user to list.  NULL for all users.
 *
 * @return A pointer to a GSList containing the names of the users or NULL if
 *         the directory could not be opened, did not exist or was not a directory.
 *         The list should be freed with g_slist_free when no longer needed.  Each
 *         element of the list should be freed with g_free.
 */
GSList *
openvas_admin_list_users (const gchar * directory, int ascending,
                          const gchar * name)
{
  GSList *users = NULL;

  if (g_file_test (directory, G_FILE_TEST_EXISTS)
      && g_file_test (directory, G_FILE_TEST_IS_DIR))
    {
      const gchar *entry_name = NULL;
      GError *error = NULL;
      GDir *users_dir = NULL;

      users_dir = g_dir_open (directory, 0, &error);
      if (users_dir == NULL)
        {
          g_warning ("%s", error->message);
          g_error_free (error);
        }
      else
        {
          while ((entry_name = g_dir_read_name (users_dir)))
            {
              gchar *user_hash_filename;
              gchar *user_dname_filename;

              if (strcmp (entry_name, "om") == 0)
                continue;

              if (name == NULL || (strcmp (name, entry_name) == 0))
                {
                  user_hash_filename =
                    g_build_filename (directory, entry_name, "auth", "hash",
                                      NULL);
                  user_dname_filename =
                    g_build_filename (directory, entry_name, "auth", "dname",
                                      NULL);
                  if (g_file_test (user_hash_filename, G_FILE_TEST_EXISTS))
                    {
                      users =
                        g_slist_insert_sorted (users,
                                               (gpointer) g_strdup (entry_name),
                                               (GCompareFunc) (ascending ?
                                                               strcmp :
                                                               strcmp_desc));
                    }
                  else
                    if (g_file_test (user_dname_filename, G_FILE_TEST_EXISTS))
                    {
                      users =
                        g_slist_insert_sorted (users,
                                               (gpointer) g_strdup (entry_name),
                                               (GCompareFunc) (ascending ?
                                                               strcmp :
                                                               strcmp_desc));
                    }
                  g_free (user_hash_filename);
                  g_free (user_dname_filename);
                  if (name)
                    break;
                }
            }
          g_dir_close (users_dir);
        }

      return users;
    }
  else
    {
      g_warning ("Could not find %s!", directory);
      return NULL;
    }
}

/** @todo Move to/Use openvas-libraries/misc/openvas_auth module functionality */
/**
 * @brief Get access information for a user.
 *
 * @param[in]   name         The name of the new user.
 * @param[out]  hosts        The hosts the user is allowed/forbidden to scan.
 * @param[out]  hosts_allow  0 forbidden, 1 allowed, 2 all allowed, 3 custom.
 * @param[in]   user_dir     The directory containing the user directories.
 *
 * @return 0 success, -1 error.
 */
int
openvas_admin_user_access (const gchar * name, gchar ** hosts, int *hosts_allow,
                           const gchar * user_dir)
{
  gchar *rules_file, *rules;
  GError *error = NULL;

  assert (name != NULL);
  assert (hosts != NULL);
  assert (hosts_allow != NULL);

  rules_file = g_build_filename (user_dir, name, "auth", "rules", NULL);
  g_file_get_contents (rules_file, &rules, NULL, &error);
  if (error)
    {
      g_warning ("%s", error->message);
      g_error_free (error);
      g_free (rules_file);
      return -1;
    }
  g_free (rules_file);

  if (strlen (rules))
    {
      int count, end = 0;

      /* "# " ("allow " | "deny ") hosts */

      count = sscanf (rules, RULES_HEADER "# allow %*[^\n]%n\n", &end);
      if (count == 0 && end > 0)
        {
          *hosts =
            g_strndup (rules + strlen (RULES_HEADER "# allow "),
                       end - strlen (RULES_HEADER "# allow "));
          *hosts_allow = 1;
          g_free (rules);
          return 0;
        }

      count = sscanf (rules, RULES_HEADER "# deny %*[^\n]%n\n", &end);
      if (count == 0 && end > 0)
        {
          *hosts =
            g_strndup (rules + strlen (RULES_HEADER "# deny "),
                       end - strlen (RULES_HEADER "# deny "));
          *hosts_allow = 0;
          g_free (rules);
          return 0;
        }

      if (strcmp (RULES_HEADER, rules) == 0)
        {
          *hosts = NULL;
          *hosts_allow = 2;
          g_free (rules);
          return 0;
        }

      /* Failed to parse content. */
      *hosts = NULL;
      *hosts_allow = 3;
      g_free (rules);
      return 0;
    }

  *hosts = NULL;
  *hosts_allow = 2;
  g_free (rules);
  return 0;
}

/** @todo Get the 200's in these commands from libs (STATUS_OK). */

/**
 * @brief Produces an OAP LIST_USERS response from a list of users.
 *
 * @param[in]  users  A pointer to a GSList containing user names.
 *
 * @return The OAP response.
 */
GString *
print_users_xml (GSList * users)
{
  GString *response = NULL;
  if (users)
    {
      response = g_string_new ("<get_users_reponse status=\"200\">");
      g_slist_foreach (users, print_user, response);
      response = g_string_append (response, "</get_users_response>");
    }
  else
    {
      response =
        g_string_new ("<get_users_response status=\"200\">" "<users/>"
                      "</get_users_response>");
    }
  return response;
}

/**
 * @brief Convert a list of users into a string.
 *
 * Produces a simple newline separated list of user names from a list of
 * users.
 *
 * @param[in]  users A pointer to a GSList containing user names.
 *
 * @return A newline separated list of user names.
 */
GString *
print_users_text (GSList * users)
{
  GString *response = g_string_new ("");
  if (users)
    {
      g_slist_foreach (users, print_user_text, response);
    }
  return response;
}

/**
 *
 * @brief Validates a username.
 *
 * @param[in]  name  The name.
 *
 * @return 0 if the username is valid, 1 if not.
 */
int
validate_username (const gchar * name)
{
  if (g_regex_match_simple ("^[[:alnum:]-_]+$", name, 0, 0))
    return 0;
  else
    return 1;
}

/**
 * @brief Adds a new user to the OpenVAS installation.
 *
 * @todo Adding users authenticating with certificates is not yet implemented.
 *
 * @param[in]  name         The name of the new user.
 * @param[in]  password     The password of the new user.
 * @param[in]  role         The role of the user.
 * @param[in]  hosts        The host the user is allowed/forbidden to scan.
 * @param[in]  hosts_allow  Whether hosts is allow or forbid.
 * @param[in]  directory    The directory containing the user directories.  It
 *                          will be created if it does not exist already.
 * @param[out] r_errdesc    If not NULL the address of a variable to receive
 *                          a malloced string with the error description.  Will
 *                          always be set to NULL on success.
 *
 * @return 0 if the user has been added successfully, -1 on error, -2 if user
 *         exists already.
 */
int
openvas_admin_add_user (const gchar * name, const gchar * password,
                        const gchar * role, const gchar * hosts,
                        int hosts_allow, const gchar * directory,
                        const array_t * allowed_methods,
                        gchar **r_errdesc)
{
  char *errstr;

  assert (name != NULL);
  assert (password != NULL);
  assert (role != NULL);
  assert (directory != NULL);
  if (r_errdesc)
    *r_errdesc = NULL;

  if (validate_username (name) != 0)
    {
      g_warning ("Invalid characters in user name!");
      if (r_errdesc)
        *r_errdesc = g_strdup ("Invalid characters in user name");
      return -1;
    }

  if (strcmp (name, "om") == 0)
    {
      g_warning ("Attempt to add special \"om\" user!");
      if (r_errdesc)
        *r_errdesc = g_strdup ("Attempt to add special \"om\" user");
      return -1;
    }

  if ((errstr = openvas_validate_password (password, name)))
    {
      g_warning ("new password for '%s' rejected: %s", name, errstr);
      if (r_errdesc)
        *r_errdesc = errstr;
      else
        g_free (errstr);
      return -1;
    }

  if (!g_file_test (directory, G_FILE_TEST_EXISTS))
    {
      if (g_mkdir (directory, 0700))
        {
          g_warning ("Could not create %s!", directory);
          return -1;
        }
    }

  if (g_file_test (directory, G_FILE_TEST_IS_DIR))
    {
      GError *error = NULL;
      gchar *user_dir_name, *user_auth_dir_name;
      gchar *user_hash_file_name, *hashes_out, *uuid_file_name, *contents;
      char *uuid;

      user_dir_name = g_build_filename (directory, name, NULL);

      if (g_file_test (user_dir_name, G_FILE_TEST_EXISTS)
          && g_file_test (user_dir_name, G_FILE_TEST_IS_DIR))
        {
          g_warning ("User %s already exists!", name);
          g_free (user_dir_name);
          return -2;
        }

      /* Make the user directory. */

      if (g_mkdir (user_dir_name, 0700))
        {
          g_warning ("Could not create %s!", user_dir_name);
          g_warning ("Failed to set up user directories for user %s", name);
          g_free (user_dir_name);
          return -1;
        }

      /* Make the auth subdirectory. */

      user_auth_dir_name = g_build_filename (user_dir_name, "auth", NULL);
      if (g_mkdir (user_auth_dir_name, 0700))
        {
          g_warning ("Could not create %s!", user_auth_dir_name);
          if (openvas_file_remove_recurse (user_dir_name))
            g_warning ("Could not remove %s while trying to revert changes!",
                       user_dir_name);
          g_warning ("Failed to set up user directories for user %s", name);
          g_free (user_dir_name);
          g_free (user_auth_dir_name);
          return -1;
        }

      /* Make a UUID, and store it in the file "uuid". */

      uuid = openvas_uuid_make ();
      if (uuid == NULL)
        {
          g_free (user_dir_name);
          g_free (user_auth_dir_name);
          return -1;
        }

      contents = g_strdup_printf ("%s\n", uuid);
      free (uuid);

      uuid_file_name = g_build_filename (user_dir_name, "uuid", NULL);

      if (!g_file_set_contents (uuid_file_name, contents, -1, &error))
        {
          g_warning ("Failed to store UUID: %s", error->message);
          g_error_free (error);
          if (openvas_file_remove_recurse (user_dir_name))
            g_warning ("Could not remove %s while trying to revert changes!",
                       user_dir_name);
          g_free (contents);
          g_free (uuid_file_name);
          g_free (user_dir_name);
          g_free (user_auth_dir_name);
          return -1;
        }
      g_free (contents);
      g_free (uuid_file_name);

      /* Put the password hashes in auth/hash. */

      hashes_out = get_password_hashes (GCRY_MD_MD5, password);
      user_hash_file_name = g_build_filename (user_auth_dir_name, "hash", NULL);
      if (!g_file_set_contents (user_hash_file_name, hashes_out, -1, &error))
        {
          g_warning ("%s", error->message);
          g_error_free (error);
          if (openvas_file_remove_recurse (user_dir_name))
            g_warning ("Could not remove %s while trying to revert changes!",
                       user_dir_name);
          g_free (hashes_out);
          g_free (user_dir_name);
          g_free (user_auth_dir_name);
          g_free (user_hash_file_name);
          return -1;
        }
      g_chmod (user_hash_file_name, 0600);
      g_free (hashes_out);
      g_free (user_hash_file_name);

      /* Create rules according to hosts. */

      if (openvas_auth_store_user_rules (user_dir_name, hosts, hosts_allow) ==
          -1)
        {
          if (openvas_file_remove_recurse (user_dir_name))
            g_warning ("Could not remove %s while trying to revert changes!",
                       user_dir_name);
          g_free (user_dir_name);
          return -1;
        }

      /* Set the role of the user. */

      if (openvas_set_user_role (name, role, NULL))
        {
          if (openvas_file_remove_recurse (user_dir_name))
            g_warning ("Could not remove %s while trying to revert changes!",
                       user_dir_name);
          return -1;
        }

      if (allowed_methods != NULL)
        {
           if (openvas_auth_user_set_allowed_methods (name, allowed_methods) != 1)
             {
               g_error ("Could not set allowed authentication methods for a user");
               return -1;
             }
        }

      return 0;
    }

  g_warning ("Could not access %s!", directory);
  return -1;
}

/**
 * @brief Removes an user from the OpenVAS installation.
 *
 * @param[in]  name       The name of the user to be removed.
 * @param[in]  directory  The directory containing the user directories.
 *
 * @return 0 if the user has been removed successfully, -1 on error,
 *         -2 if failed to find such a user.
 */
int
openvas_admin_remove_user (const gchar * name, const gchar * directory)
{
  assert (name != NULL);

  if (strcmp (name, "om") == 0)
    {
      g_warning ("Attempt to remove special \"om\" user!");
      return -1;
    }

  if (g_file_test (directory, G_FILE_TEST_EXISTS)
      && g_file_test (directory, G_FILE_TEST_IS_DIR))
    {
      gchar *user_dir_name = g_build_filename (directory, name, NULL);

      if (g_file_test (user_dir_name, G_FILE_TEST_EXISTS)
          && g_file_test (user_dir_name, G_FILE_TEST_IS_DIR))
        {
          if (openvas_file_remove_recurse (user_dir_name) == 0)
            {
              g_free (user_dir_name);
              return 0;
            }
          else
            {
              g_warning ("Failed to remove %s!", user_dir_name);
              g_free (user_dir_name);
              return -1;
            }
        }
      else
        {
          g_free (user_dir_name);
          g_warning ("User %s does not exist!", name);
          return -2;
        }
    }
  else
    {
      g_warning ("Could not find %s!", directory);
      return -1;
    }
}

/**
 * @brief Modify a user.
 *
 * @param[in]  name         The name of the new user.
 * @param[in]  password     The password of the new user.  NULL to leave as is.
 * @param[in]  role         The role of the user.  NULL to leave as is.
 * @param[in]  hosts        The host the user is allowed/forbidden to scan.
 *                          NULL to leave as is.
 * @param[in]  hosts_allow  Whether hosts is allow or forbid.
 * @param[in]  directory    The directory containing the user directories.  It
 *                          will be created if it does not exist already.
 * @param[out] r_errdesc    If not NULL the address of a variable to receive
 *                          a malloced string with the error description.  Will
 *                          always be set to NULL on success.
 *
 * @return 0 if the user has been added successfully, -1 on error, -2 for an
 *         unknown role, -3 if user exists already.
 */
int
openvas_admin_modify_user (const gchar * name, const gchar * password,
                           const gchar * role, const gchar * hosts,
                           int hosts_allow, const gchar * directory,
                           const array_t * allowed_methods,
                           gchar **r_errdesc)
{
  char *errstr;

  if (r_errdesc)
    *r_errdesc = NULL;

  if (name && password)
    {
      if ((errstr = openvas_validate_password (password, name)))
        {
          g_warning ("new password for '%s' rejected: %s", name, errstr);
          if (r_errdesc)
            *r_errdesc = errstr;
          else
            g_free (errstr);
          return -1;
        }
    }

  return openvas_user_modify (name, password, role, hosts, hosts_allow,
                              directory, allowed_methods);
}

/**
 * @brief Sets the rules for an existing user.
 *
 * @param[in]  name        The name of the user.
 * @param[in]  rules_file  A file containing the new rules to be applied to the
 *                         user.  Note that the new rules will overwrite all old rules.
 * @param[in]  directory   The directory containing the user directories.
 *
 * @return TRUE if the rules have been changed successfully, FALSE if not.
 */
gboolean
openvas_admin_set_rules (const gchar * name, const gchar * rules_file,
                         const gchar * directory)
{
  if (strcmp (name, "om") == 0)
    {
      g_warning ("Attempt to set rules of special \"om\" user!");
      return FALSE;
    }
  if (name == NULL || rules_file == NULL)
    {
      g_warning ("Missing name or rules_file!");
      return FALSE;
    }
  if (g_file_test (directory, G_FILE_TEST_EXISTS)
      && g_file_test (directory, G_FILE_TEST_IS_DIR))
    {
      gchar *user_dir_name = g_build_filename (directory, name, NULL);

      if (g_file_test (user_dir_name, G_FILE_TEST_EXISTS)
          && g_file_test (user_dir_name, G_FILE_TEST_IS_DIR))
        {
          gchar *new_rules = NULL;
          GError *error = NULL;
          gchar *user_rules_file_name = NULL;

          if (!g_file_test (rules_file, G_FILE_TEST_EXISTS))
            {
              g_warning ("Could not find rules file %s!", rules_file);
              g_free (user_dir_name);
              return FALSE;
            }
          if (!g_file_get_contents (rules_file, &new_rules, NULL, &error))
            {
              g_warning ("Could not read contents of %s!", rules_file);
              g_warning ("%s", error->message);
              g_error_free (error);
              g_free (user_dir_name);
              return FALSE;
            }

          user_rules_file_name =
            g_build_filename (user_dir_name, "auth", "rules", NULL);

          if (!g_file_set_contents
              (user_rules_file_name, new_rules, -1, &error))
            {
              g_warning ("%s", error->message);
              g_error_free (error);
              g_free (user_dir_name);
              g_free (user_rules_file_name);
              return FALSE;
            }
          g_chmod (user_rules_file_name, 0600);

          g_free (user_rules_file_name);
          g_free (user_dir_name);
          return TRUE;
        }
      else
        {
          g_warning ("User %s does not exist!", name);
          g_free (user_dir_name);
          return FALSE;
        }
    }
  else
    {
      g_warning ("Could not find %s!", directory);
      return FALSE;
    }
}

/**
 * @brief Request a feed synchronization script selftest.
 *
 * Ask a feed synchronization script to perform a selftest and report
 * the results.
 *
 * @param[in]   sync_script  The file name of the synchronization script.
 * @param[out]  result       Return location for selftest errors, or NULL.
 *
 * @return TRUE if the selftest was successful, or FALSE if an error occured.
 */
gboolean
openvas_sync_script_perform_selftest (const gchar * sync_script,
                                      gchar ** result)
{
  g_assert (sync_script);
  g_assert_cmpstr (*result, ==, NULL);

  gchar *script_working_dir = g_path_get_dirname (sync_script);

  gchar **argv = (gchar **) g_malloc (3 * sizeof (gchar *));
  argv[0] = g_strdup (sync_script);
  argv[1] = g_strdup ("--selftest");
  argv[2] = NULL;

  gchar *script_out;
  gchar *script_err;
  gint script_exit;
  GError *error = NULL;

  if (!g_spawn_sync
      (script_working_dir, argv, NULL, 0, NULL, NULL, &script_out, &script_err,
       &script_exit, &error))
    {
      if (*result != NULL)
        {
          *result =
            g_strdup_printf ("Failed to execute synchronization " "script: %s",
                             error->message);
        }

      g_free (script_working_dir);
      g_strfreev (argv);
      g_free (script_out);
      g_free (script_err);
      g_error_free (error);

      return FALSE;
    }

  if (script_exit != 0)
    {
      if (script_err != NULL)
        {
          *result = g_strdup_printf ("%s", script_err);
        }

      g_free (script_working_dir);
      g_strfreev (argv);
      g_free (script_out);
      g_free (script_err);

      return FALSE;
    }

  g_free (script_working_dir);
  g_strfreev (argv);
  g_free (script_out);
  g_free (script_err);

  return TRUE;
}

/**
 * @brief Retrieves the ID string of a feed sync script, with basic validation.
 *
 * @param[in]   sync_script     The file name of the synchronization script.
 * @param[out]  identification  Return location of the identification string.
 * @param[in]   feed_type       Could be NVT_FEED, SCAP_FEED or CERT_FEED.
 *
 * @return TRUE if the identification string was retrieved, or FALSE if an
 *         error occured.
 */
gboolean
openvas_get_sync_script_identification (const gchar * sync_script,
                                        gchar ** identification,
                                        int feed_type)
{
  g_assert (sync_script);
  g_assert_cmpstr (*identification, ==, NULL);

  gchar *script_working_dir = g_path_get_dirname (sync_script);

  gchar **argv = (gchar **) g_malloc (3 * sizeof (gchar *));
  argv[0] = g_strdup (sync_script);
  argv[1] = g_strdup ("--identify");
  argv[2] = NULL;

  gchar *script_out;
  gchar *script_err;
  gint script_exit;
  GError *error = NULL;

  gchar **script_identification;

  if (!g_spawn_sync
      (script_working_dir, argv, NULL, 0, NULL, NULL, &script_out, &script_err,
       &script_exit, &error))
    {
      g_warning ("Failed to execute %s: %s", sync_script, error->message);

      g_free (script_working_dir);
      g_strfreev (argv);
      g_free (script_out);
      g_free (script_err);
      g_error_free (error);

      return FALSE;
    }

  if (script_exit != 0)
    {
      g_warning ("%s returned a non-zero exit code.", sync_script);

      g_free (script_working_dir);
      g_strfreev (argv);
      g_free (script_out);
      g_free (script_err);

      return FALSE;
    }

  script_identification = g_strsplit (script_out, "|", 6);

  if ((script_identification[0] == NULL)
      || (feed_type == NVT_FEED
          && g_ascii_strncasecmp (script_identification[0],"NVTSYNC",7))
      || (feed_type == SCAP_FEED
          && g_ascii_strncasecmp (script_identification[0],"SCAPSYNC",7))
      || (feed_type == CERT_FEED
          && g_ascii_strncasecmp (script_identification[0],"CERTSYNC",7))
      || g_ascii_strncasecmp (script_identification[0], script_identification[5], 7))
    {
      g_warning ("%s is not a NVT synchronization script.", sync_script);

      g_free (script_working_dir);
      g_strfreev (argv);
      g_free (script_out);
      g_free (script_err);

      g_strfreev (script_identification);

      return FALSE;
    }

  *identification = g_strdup (script_out);

  g_free (script_working_dir);
  g_strfreev (argv);
  g_free (script_out);
  g_free (script_err);

  g_strfreev (script_identification);

  return TRUE;
}

/**
 * @brief Retrieves description of a feed sync script, with basic validation.
 *
 * @param[in]   sync_script  The file name of the synchronization script.
 * @param[out]  description  Return location of the description string.
 *
 * @return TRUE if the description was retrieved, or FALSE if an error
 *         occured.
 */
gboolean
openvas_get_sync_script_description (const gchar * sync_script,
                                     gchar ** description)
{
  g_assert (sync_script);
  g_assert_cmpstr (*description, ==, NULL);

  gchar *script_working_dir = g_path_get_dirname (sync_script);

  gchar **argv = (gchar **) g_malloc (3 * sizeof (gchar *));
  argv[0] = g_strdup (sync_script);
  argv[1] = g_strdup ("--describe");
  argv[2] = NULL;

  gchar *script_out;
  gchar *script_err;
  gint script_exit;
  GError *error = NULL;

  if (!g_spawn_sync
      (script_working_dir, argv, NULL, 0, NULL, NULL, &script_out, &script_err,
       &script_exit, &error))
    {
      g_warning ("Failed to execute %s: %s", sync_script, error->message);

      g_free (script_working_dir);
      g_strfreev (argv);
      g_free (script_out);
      g_free (script_err);
      g_error_free (error);

      return FALSE;
    }

  if (script_exit != 0)
    {
      g_warning ("%s returned a non-zero exit code.", sync_script);

      g_free (script_working_dir);
      g_strfreev (argv);
      g_free (script_out);
      g_free (script_err);

      return FALSE;
    }

  *description = g_strdup (script_out);

  g_free (script_working_dir);
  g_strfreev (argv);
  g_free (script_out);
  g_free (script_err);

  return TRUE;
}

/**
 * @brief Retrieves the version of a feed handled by the sync, with basic
 * validation.
 *
 * @param[in]   sync_script  The file name of the synchronization script.
 * @param[out]  feed_version  Return location of the feed version string.
 *
 * @return TRUE if the feed version was retrieved, or FALSE if an error
 *         occured.
 */
gboolean
openvas_get_sync_script_feed_version (const gchar * sync_script,
                                      gchar ** feed_version)
{
  g_assert (sync_script);
  g_assert_cmpstr (*feed_version, ==, NULL);

  gchar *script_working_dir = g_path_get_dirname (sync_script);

  gchar **argv = (gchar **) g_malloc (3 * sizeof (gchar *));
  argv[0] = g_strdup (sync_script);
  argv[1] = g_strdup ("--feedversion");
  argv[2] = NULL;

  gchar *script_out;
  gchar *script_err;
  gint script_exit;
  GError *error = NULL;

  if (!g_spawn_sync
      (script_working_dir, argv, NULL, 0, NULL, NULL, &script_out, &script_err,
       &script_exit, &error))
    {
      g_warning ("Failed to execute %s: %s", sync_script, error->message);

      g_free (script_working_dir);
      g_strfreev (argv);
      g_free (script_out);
      g_free (script_err);
      g_error_free (error);

      return FALSE;
    }

  if (script_exit != 0)
    {
      g_warning ("%s returned a non-zero exit code.", sync_script);

      g_free (script_working_dir);
      g_strfreev (argv);
      g_free (script_out);
      g_free (script_err);

      return FALSE;
    }

  *feed_version = g_strdup (script_out);

  g_free (script_working_dir);
  g_strfreev (argv);
  g_free (script_out);
  g_free (script_err);

  return TRUE;
}

/**
 * @brief Forks a child to synchronize the local feed collection.
 *
 * The forked process calls a sync script to sync the feed.
 *
 * @param[in]  sync_script   The file name of the synchronization script.
 * @param[in]  current_user  The user currently authenticated.
 * @param[in]  feed_type     Could be NVT_FEED, SCAP_FEED or CERT_FEED.
 *
 * @return 0 sync requested (parent), 1 sync already in progress (parent),
 *         -1 error (parent), 2 sync complete (child), 11 sync in progress
 *         (child), -10 error (child).
 */
int
openvas_sync_feed (const gchar * sync_script, const gchar * current_user,
                   int feed_type)
{
  int fd, ret = 2;
  gchar *lockfile_name, *lockfile_dirname;
  gchar *script_identification_string = NULL;
  pid_t pid;
  mode_t old_mask;

  g_assert (sync_script);
  g_assert (current_user);

  if (!openvas_get_sync_script_identification
      (sync_script, &script_identification_string, feed_type))
    {
      g_warning ("No valid synchronization script supplied!");
      return -1;
    }

  /* Open the lock file. */

  lockfile_name =
    g_build_filename (g_get_tmp_dir (), "openvas-feed-sync", sync_script, NULL);
  lockfile_dirname = g_path_get_dirname (lockfile_name);
  old_mask = umask (0);
  if (g_mkdir_with_parents (lockfile_dirname,
                            /* "-rwxrwxrwx" */
                            S_IRWXU | S_IRWXG | S_IRWXO))
    {
      umask (old_mask);
      g_warning ("Failed to create lock dir '%s': %s", lockfile_dirname,
                 strerror (errno));
      g_free (lockfile_name);
      g_free (lockfile_dirname);
      return -1;
    }
  umask (old_mask);
  g_free (lockfile_dirname);

  fd =
    open (lockfile_name, O_RDWR | O_CREAT | O_EXCL,
          S_IWUSR | S_IRUSR | S_IROTH | S_IRGRP /* "-rw-r--r--" */ );
  if (fd == -1)
    {
      if (errno == EEXIST)
        return 1;
      g_warning ("Failed to open lock file '%s': %s", lockfile_name,
                 strerror (errno));
      g_free (lockfile_name);
      return -1;
    }

  /* Close and remove the lock file around the fork.  Another process may get
   * the lock here, in which case the child will simply fail to get the
   * lock. */

  if (close (fd))
    {
      g_free (lockfile_name);
      g_warning ("Failed to close lock file: %s", strerror (errno));
      return -1;
    }

  if (unlink (lockfile_name))
    {
      g_free (lockfile_name);
      g_warning ("Failed to remove lock file: %s", strerror (errno));
      return -1;
    }

  /* Setup SIGCHLD for waiting. */

  /* RATS: ignore, this is SIG_DFL damnit. */
  if (signal (SIGCHLD, SIG_DFL) == SIG_ERR)
    {
      g_warning ("Failed to set SIG_DFL");
      return -1;
    }

  /* Fork a child to run the sync while the parent responds to
   * the client. */

  pid = fork ();
  switch (pid)
    {
    case 0:
      /* Child.  Carry on to sync. */
      break;
    case -1:
      /* Parent when error. */
      g_warning ("%s: failed to fork sync child: %s\n", __FUNCTION__,
                 strerror (errno));
      return -1;
      break;
    default:
      /* Parent.  Return in order to respond to client. */
      return 0;
      break;
    }

  /* Open the lock file. */

  fd =
    open (lockfile_name, O_RDWR | O_CREAT | O_EXCL,
          S_IWUSR | S_IRUSR | S_IROTH | S_IRGRP /* "-rw-r--r--" */ );
  if (fd == -1)
    {
      if (errno == EEXIST)
        return 11;
      g_warning ("Failed to open lock file '%s' (child): %s", lockfile_name,
                 strerror (errno));
      g_free (lockfile_name);
      return -10;
    }

  /* Write the current time and user to the lock file. */

  {
    const char *output;
    int count, left;
    time_t now;

    time (&now);
    output = ctime (&now);
    left = strlen (output);
    while (1)
      {
        count = write (fd, output, left);
        if (count < 0)
          {
            if (errno == EINTR || errno == EAGAIN)
              continue;
            g_warning ("%s: write: %s", __FUNCTION__, strerror (errno));
            goto exit;
          }
        if (count == left)
          break;
        left -= count;
        output += count;
      }

    output = current_user;
    left = strlen (output);
    while (1)
      {
        count = write (fd, output, left);
        if (count < 0)
          {
            if (errno == EINTR || errno == EAGAIN)
              continue;
            g_warning ("%s: write: %s", __FUNCTION__, strerror (errno));
            goto exit;
          }
        if (count == left)
          break;
        left -= count;
        output += count;
      }

    while (1)
      {
        count = write (fd, "\n", 1);
        if (count < 0)
          {
            if (errno == EINTR || errno == EAGAIN)
              continue;
            g_warning ("%s: write: %s", __FUNCTION__, strerror (errno));
            goto exit;
          }
        if (count == 1)
          break;
      }
  }

  /* Fork a child to be the sync process. */

  pid = fork ();
  switch (pid)
    {
    case 0:
      {
        /* Child.  Become the sync process. */

        if (freopen ("/tmp/openvasad_sync_out", "w", stdout) == NULL)
          {
            g_warning ("Failed to reopen stdout: %s", strerror (errno));
            exit (EXIT_FAILURE);
          }

        if (freopen ("/tmp/openvasad_sync_err", "w", stderr) == NULL)
          {
            g_warning ("Failed to reopen stderr: %s", strerror (errno));
            exit (EXIT_FAILURE);
          }

        if (execl (sync_script, sync_script, (char *) NULL))
          {
            g_warning ("Failed to execl %s: %s", sync_script, strerror (errno));
            exit (EXIT_FAILURE);
          }
        /*@notreached@ */
        exit (EXIT_FAILURE);
        break;
      }
    case -1:
      /* Parent when error. */

      g_warning ("%s: failed to fork syncer: %s\n", __FUNCTION__,
                 strerror (errno));
      ret = -1;
      goto exit;
      break;
    default:
      {
        int status;

        /* Parent on success.  Wait for child, and handle result. */

        while (wait (&status) < 0)
          {
            if (errno == ECHILD)
              {
                g_warning ("Failed to get child exit status");
                ret = -10;
                goto exit;
              }
            if (errno == EINTR)
              continue;
            g_warning ("wait: %s", strerror (errno));
            ret = -10;
            goto exit;
          }
        if (WIFEXITED (status))
          switch (WEXITSTATUS (status))
            {
            case EXIT_SUCCESS:
              break;
            case EXIT_FAILURE:
            default:
              g_warning ("Error during synchronization.");
              ret = -10;
              break;
            }
        else
          {
            g_message ("Error during synchronization.");
            ret = -10;
          }

        break;
      }
    }

exit:

  /* Close the lock file. */

  if (close (fd))
    {
      g_free (lockfile_name);
      g_warning ("Failed to close lock file (child): %s", strerror (errno));
      return -10;
    }

  /* Remove the lock file. */

  if (unlink (lockfile_name))
    {
      g_free (lockfile_name);
      g_warning ("Failed to remove lock file (child): %s", strerror (errno));
      return -10;
    }

  g_free (lockfile_name);

  return ret;
}

/**
 * @brief Determine if the administrator is synchronizing with a feed.
 *
 * @param[in]   sync_script  The file name of the synchronization script.
 * @param[out]  timestamp    Newly allocated time that sync started, if syncing.
 * @param[out]  user         Newly allocated user who started sync, if syncing.
 *
 * @return 0 success, 1 success when sync in progress, -1 error.
 */
int
openvas_current_sync (const gchar * sync_script, gchar ** timestamp,
                      gchar ** user)
{
  gchar *lockfile_name, *content, **lines;
  GError *error = NULL;

  g_assert (sync_script);

  lockfile_name =
    g_build_filename (g_get_tmp_dir (), "openvas-feed-sync", sync_script, NULL);
  if (!g_file_get_contents (lockfile_name, &content, NULL, &error))
    {
      if (g_error_matches (error, G_FILE_ERROR, G_FILE_ERROR_NOENT)
          || g_error_matches (error, G_FILE_ERROR, G_FILE_ERROR_ACCES))
        {
          g_error_free (error);
          g_free (lockfile_name);
          return 0;
        }

      g_warning ("%s: %s", __FUNCTION__, error->message);
      g_error_free (error);
      g_free (lockfile_name);
      return -1;
    }

  lines = g_strsplit (content, "\n", 2);
  g_free (content);
  if (lines[0] && lines[1])
    {
      *timestamp = g_strdup (lines[0]);
      *user = g_strdup (lines[1]);

      g_free (lockfile_name);
      g_strfreev (lines);
      return 1;
    }

  g_free (lockfile_name);
  g_strfreev (lines);
  return -1;
}

/**
 * @brief Modify the scanner settings.
 *
 * @param[in]  config_file   File holding settings.
 * @param[in]  group         Group in file holiding settings.
 * @param[in]  new_settings  New values for settings.
 *
 * @return 0 success, -1 error.
 */
int
openvas_admin_modify_settings (const char *config_file, const char *group,
                               const array_t * new_settings /* admin_setting_t */ )
{
  if (new_settings)
    {
      int index = 0;
      admin_setting_t *new_setting;
      settings_t settings;

      settings_init (&settings, config_file, group);

      while ((new_setting =
              (admin_setting_t *) g_ptr_array_index (new_settings, index++)))
        settings_set (&settings, new_setting->name, new_setting->value);

      if (settings_save (&settings))
        return -1;

      settings_cleanup (&settings);
    }
  return 0;
}


/* Schema. */

/**
 * @brief Generate the OAP schema.
 *
 * @param[in]  format         Name of schema format, "XML" or NULL for XML.
 * @param[out] output_return  NULL or location for output.
 * @param[out] output_length  NULL or location for length of output.
 * @param[out] extension      NULL or location for report format extension.
 * @param[out] content_type   NULL or location for report format content type.
 *
 * @return 0 success, 1 failed to find schema format, -1 error.
 */
int
openvas_admin_schema (gchar *format, gchar **output_return, gsize *output_length,
                      gchar **extension, gchar **content_type)
{
  /* Pass the XML file to the schema generate script, sending the output
   * to a file. */

  {
    gchar *script, *script_dir;
    gchar *uuid_format;
    char output_dir[] = "/tmp/openvasad_schema_XXXXXX";

    if (mkdtemp (output_dir) == NULL)
      {
        g_warning ("%s: mkdtemp failed\n", __FUNCTION__);
        return -1;
      }

    /* Setup file names. */

    if (format == NULL)
      {
        if (extension)
          *extension = g_strdup ("xml");
        if (content_type)
          *content_type = g_strdup ("text/xml");
        uuid_format = "af659394-181e-11e0-8cd5-002264764cea";
      }
    else if (strcasecmp (format, "HTML") == 0)
      {
        if (extension)
          *extension = g_strdup ("html");
        if (content_type)
          *content_type = g_strdup ("text/html");
        uuid_format = "70847ae0-181f-11e0-979b-002264764cea";
      }
    else if (strcasecmp (format, "RNC") == 0)
      {
        if (extension)
          *extension = g_strdup ("rnc");
        if (content_type)
          *content_type = g_strdup ("text/x-rnc");
        uuid_format = "688cab96-181f-11e0-893a-002264764cea";
      }
    else if (strcasecmp (format, "XML") == 0)
      {
        if (extension)
          *extension = g_strdup ("xml");
        if (content_type)
          *content_type = g_strdup ("text/xml");
        uuid_format = "af659394-181e-11e0-8cd5-002264764cea";
      }
    else
      return 1;

    script_dir = g_build_filename (OPENVAS_DATA_DIR,
                                   "openvasad",
                                   "global_schema_formats",
                                   uuid_format,
                                   NULL);

    script = g_build_filename (script_dir, "generate", NULL);

    if (!g_file_test (script, G_FILE_TEST_EXISTS))
      {
        g_free (script);
        g_free (script_dir);
        if (extension) g_free (*extension);
        if (content_type) g_free (*content_type);
        return -1;
      }

    {
      gchar *output_file, *command;
      char *previous_dir;
      int ret;

      /* Change into the script directory. */

      /** @todo NULL arg is glibc extension. */
      previous_dir = getcwd (NULL, 0);
      if (previous_dir == NULL)
        {
          g_warning ("%s: Failed to getcwd: %s\n",
                     __FUNCTION__,
                     strerror (errno));
          g_free (previous_dir);
          g_free (script);
          g_free (script_dir);
          if (extension) g_free (*extension);
          if (content_type) g_free (*content_type);
          return -1;
        }

      if (chdir (script_dir))
        {
          g_warning ("%s: Failed to chdir: %s\n",
                     __FUNCTION__,
                     strerror (errno));
          g_free (previous_dir);
          g_free (script);
          g_free (script_dir);
          if (extension) g_free (*extension);
          if (content_type) g_free (*content_type);
          return -1;
        }
      g_free (script_dir);

      output_file = g_strdup_printf ("%s/report.out", output_dir);

      /* Call the script. */

      command = g_strdup_printf ("/bin/sh %s " OPENVAS_DATA_DIR
                                 "/openvasad/global_schema_formats"
                                 "/af659394-181e-11e0-8cd5-002264764cea/OAP.xml"
                                 " > %s"
                                 " 2> /dev/null",
                                 script,
                                 output_file);
      g_free (script);

      g_debug ("   command: %s\n", command);

      /* RATS: ignore, command is defined above. */
      if (ret = system (command),
          /** @todo ret is always -1. */
          0 && ((ret) == -1
                || WEXITSTATUS (ret)))
        {
          g_warning ("%s: system failed with ret %i, %i, %s\n",
                     __FUNCTION__,
                     ret,
                     WEXITSTATUS (ret),
                     command);
          if (chdir (previous_dir))
            g_warning ("%s: and chdir failed\n",
                       __FUNCTION__);
          g_free (previous_dir);
          g_free (command);
          g_free (output_file);
          if (extension) g_free (*extension);
          if (content_type) g_free (*content_type);
          return -1;
        }

      {
        GError *get_error;
        gchar *output;
        gsize output_len;

        g_free (command);

        /* Change back to the previous directory. */

        if (chdir (previous_dir))
          {
            g_warning ("%s: Failed to chdir back: %s\n",
                       __FUNCTION__,
                       strerror (errno));
            g_free (previous_dir);
            if (extension) g_free (*extension);
            if (content_type) g_free (*content_type);
            return -1;
          }
        g_free (previous_dir);

        /* Read the script output from file. */

        get_error = NULL;
        g_file_get_contents (output_file,
                             &output,
                             &output_len,
                             &get_error);
        g_free (output_file);
        if (get_error)
          {
            g_warning ("%s: Failed to get output: %s\n",
                       __FUNCTION__,
                       get_error->message);
            g_error_free (get_error);
            if (extension) g_free (*extension);
            if (content_type) g_free (*content_type);
            return -1;
          }

        /* Remove the output directory. */

        openvas_file_remove_recurse (output_dir);

        /* Return the output. */

        if (output_length) *output_length = output_len;

        if (output_return) *output_return = output;
        return 0;
      }
    }
  }
}
