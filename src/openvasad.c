/* OpenVAS Administrator - Main Module
 * $Id$
 * Description: Manages the configuration of an OpenVAS server.
 *
 * Authors:
 * Matthew Mundell <matthew.mundell@greenbone.net>
 * Michael Wiegand <michael.wiegand@greenbone.net>
 *
 * Copyright:
 * Copyright (C) 2009-2013 Greenbone Networks GmbH
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * or, at your option, any later version as published by the Free
 * Software Foundation
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * \mainpage
 *
 * \section Introduction
 * \verbinclude README
 *
 * \section copying License Information
 * \verbinclude COPYING
 */

/**
 * @file  openvasad.c
 * @brief The OpenVAS Administrator
 */
#define _GNU_SOURCE

#include "admin.h"
#include "oapd.h"
#include "oxpd.h"
#include "tracef.h"

#include <assert.h>
#include <glib.h>
#include <glib/gprintf.h>
#include <glib/gstdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#ifndef S_SPLINT_S
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#endif
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <gnutls/gnutls.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <termios.h>  /* for tcsetattr */

#include <openvas/misc/openvas_logging.h>
#include <openvas/misc/openvas_server.h>
#include <openvas/misc/openvas_auth.h>
#include <openvas/base/pidfile.h>
#include <openvas/base/settings.h>
#include <openvas/base/pwpolicy.h>

#ifdef S_SPLINT_S
#include "splint.h"
#endif

/**
 * @brief The name of this program.
 *
 * \todo Use `program_invocation[_short]_name'?
 */
#define PROGNAME "openvasad"

/**
 * @brief The version number of this program.
 */
#ifndef OPENVASAD_VERSION
#define OPENVASAD_VERSION "FIX"
#endif

/**
 * @brief The name of the underlying Operating System.
 */
#ifndef OPENVAS_OS_NAME
#define OPENVAS_OS_NAME "FIX"
#endif

/** @todo Rename SERVER_CERT, etc (squashed names copied from manual). */

/**
 * @brief Location of server certificate.
 */
#ifndef OPENVAS_SERVER_CERTIFICATE
#define OPENVAS_SERVER_CERTIFICATE "/var/lib/openvas/CA/servercert.pem"
#endif

/**
 * @brief Location of server certificate private key.
 */
#ifndef OPENVAS_SERVER_KEY
#define OPENVAS_SERVER_KEY "/var/lib/openvas/private/CA/serverkey.pem"
#endif

/**
 * @brief Location of Certificate Authority certificate.
 */
#ifndef OPENVAS_CA_CERTIFICATE
#define OPENVAS_CA_CERTIFICATE     "/var/lib/openvas/CA/cacert.pem"
#endif

/**
 * @brief Administrator port.
 *
 * Used if /etc/services "oap" and --port are missing.
 */
#define OPENVASAD_PORT 9393

/**
 * @brief Second argument to `listen'.
 */
#define MAX_CONNECTIONS 512

/**
 * @brief The socket accepting OMP connections from clients.
 */
int administrator_socket = -1;

/**
 * @brief The IP address of this program, "the administrator".
 */
struct sockaddr_in administrator_address;

#if LOG
/**
 * @brief The log stream.
 */
FILE *log_stream = NULL;
#endif

/**
 * @brief Verbose output flag.
 *
 * Only consulted if compiled with TRACE non-zero.
 */
int verbose = 0;

/**
 * @brief Logging parameters, as passed to setup_log_handlers.
 */
GSList *log_config = NULL;

/**
 * @brief The client session.
 */
gnutls_session_t client_session;

/**
 * @brief The client credentials.
 */
gnutls_certificate_credentials_t client_credentials;

/**
 * @brief Is this process parent or child?
 */
int is_parent = 1;


/* Forking, serving the client. */

/**
 * @brief Serve the client.
 *
 * Connect to the openvassd server, then call \ref serve_oap to serve the
 * protocol.  Read the first message with \ref read_protocol.
 *
 * In all cases, close client_socket before returning.
 *
 * @param[in]  client_socket  The socket connected to the client.
 *
 * @return EXIT_SUCCESS on success, EXIT_FAILURE on failure.
 */
int
serve_client (int client_socket)
{
  if (openvas_server_attach (client_socket, &client_session))
    {
      g_critical ("%s: failed to attach client session to socket %i\n",
                  __FUNCTION__, client_socket);
      goto fail;
    }

  // FIX get flags first
  /* The socket must have O_NONBLOCK set, in case an "asynchronous network
   * error" removes the data between `select' and `read'. */
  if (fcntl (client_socket, F_SETFL, O_NONBLOCK) == -1)
    {
      g_warning ("%s: failed to set real client socket flag: %s\n",
                 __FUNCTION__, strerror (errno));
      goto fail;
    }

  /* Read a message from the client, and call the appropriate protocol
   * handler. */

  // FIX some of these are client errs, all EXIT_FAILURE
  switch (read_protocol (&client_session, client_socket))
    {
    case PROTOCOL_OMP:
      /* It's up to serve_oap to openvas_server_free on client_socket. */
      if (serve_oap (&client_session, client_socket, &client_credentials))
        return EXIT_FAILURE;
      break;
    case PROTOCOL_CLOSE:
      openvas_server_free (client_socket, client_session, client_credentials);
      g_message ("   EOF while trying to read protocol\n");
      return EXIT_FAILURE;
    case PROTOCOL_TIMEOUT:
      openvas_server_free (client_socket, client_session, client_credentials);
      break;
    default:
      g_warning ("%s: Failed to determine protocol\n", __FUNCTION__);
    }

  return EXIT_SUCCESS;

fail:
  openvas_server_free (client_socket, client_session, client_credentials);
  return EXIT_FAILURE;
}

/**
 * @brief Accept and fork.
 *
 * Accept the client connection and fork a child process to serve the client.
 * The child calls \ref serve_client to do the rest of the work.
 */
void
accept_and_maybe_fork ()
{
  /* Accept the client connection. */
  pid_t pid;
  struct sockaddr_in client_address;
  socklen_t size = sizeof (client_address);
  int client_socket;
  client_address.sin_family = AF_INET;
  while ((client_socket =
          accept (administrator_socket, (struct sockaddr *) &client_address,
                  &size)) == -1)
    {
      if (errno == EINTR)
        continue;
      if (errno == EAGAIN || errno == EWOULDBLOCK)
        /* The connection is gone, return to select. */
        return;
      g_critical ("%s: failed to accept client connection: %s\n", __FUNCTION__,
                  strerror (errno));
      exit (EXIT_FAILURE);
    }

#define FORK 1
#if FORK
  /* Fork a child to serve the client. */
  pid = fork ();
  switch (pid)
    {
    case 0:
      /* Child. */
      {
        int ret;

        is_parent = 0;

        /* RATS: ignore, this is SIG_DFL damnit. */
        if (signal (SIGCHLD, SIG_DFL) == SIG_ERR)
          {
            g_critical ("%s: failed to set client SIGCHLD handler: %s\n",
                        __FUNCTION__, strerror (errno));
            shutdown (client_socket, SHUT_RDWR);
            close (client_socket);
            exit (EXIT_FAILURE);
          }

#endif /* FORK */
        // FIX get flags first
        /* The socket must have O_NONBLOCK set, in case an "asynchronous
         * network error" removes the data between `select' and `read'.
         */
        if (fcntl (client_socket, F_SETFL, O_NONBLOCK) == -1)
          {
            g_critical ("%s: failed to set client socket flag: %s\n",
                        __FUNCTION__, strerror (errno));
            shutdown (client_socket, SHUT_RDWR);
            close (client_socket);
            exit (EXIT_FAILURE);
          }
        /* It's up to serve_client to openvas_server_free on
         * client_socket. */
#if FORK
        ret = serve_client (client_socket);
#else
        serve_client (client_socket);
#endif
#if FORK
        exit (ret);
      }
    case -1:
      /* Parent when error, return to select. */
      g_warning ("%s: failed to fork child: %s\n", __FUNCTION__,
                 strerror (errno));
      close (client_socket);
      break;
    default:
      /* Parent.  Return to select. */
#endif /* FORK */
      close (client_socket);
#if FORK
      break;
    }
#endif /* FORK */
}


/* Maintenance functions. */

/**
 * @brief Clean up for exit.
 *
 * Close sockets and streams.
 */
void
cleanup ()
{
  tracef ("   Cleaning up.\n");
  if (administrator_socket > -1)
    close (administrator_socket);
#if LOG
  if (log_stream != NULL)
    {
      if (fclose (log_stream))
        g_critical ("%s: failed to close log stream: %s\n", __FUNCTION__,
                    strerror (errno));
    }
#endif
  tracef ("   Exiting.\n");
  if (log_config)
    free_log_configuration (log_config);

  /* Delete pidfile if this process is the parent. */
  if (is_parent == 1)
    pidfile_remove ("openvasad");
}

/**
 * @brief Handle a SIGTERM signal.
 *
 * @param[in]  signal  The signal that caused this function to run.
 */
void
handle_sigterm (int signal)
{
  exit (EXIT_SUCCESS);
}

/**
 * @brief Handle a SIGHUP signal.
 *
 * @param[in]  signal  The signal that caused this function to run.
 */
void
handle_sighup (int signal)
{
}

/**
 * @brief Handle a SIGINT signal.
 *
 * @param[in]  signal  The signal that caused this function to run.
 */
void
handle_sigint (int signal)
{
  exit (EXIT_SUCCESS);
}

/**
 * @brief Reads an entire line from a stream, suppressing character output.
 *
 * @param[out]  lineptr  Location of the buffer where the line is stored.
 * @param[out]  n  Size of allocated buffer in lineptr is not null.
 * @param[in] stream  Stream from which the line should be read.
 *
 * This function mimics the behaviour of getline (). Please see the man page of
 * getline () for additional information about the parameters. This function was
 * taken from the example provided in the GNU C Library, for example at
 * http://www.gnu.org/s/libc/manual/html_node/getpass.html.
 *
 * @todo Move this function to openvas-libraries since openvas-cli uses it as
 * well.
 */
ssize_t
read_password (char **lineptr, size_t *n, FILE *stream)
{
  struct termios old, new;
  int nread;

  /* Turn echoing off and fail if we can't. */
  if (tcgetattr (fileno (stream), &old) != 0)
    return -1;
  new = old;
  new.c_lflag &= ~ECHO;
  if (tcsetattr (fileno (stream), TCSAFLUSH, &new) != 0)
    return -1;

  /* Read the password. */
  nread = getline (lineptr, n, stream);

  /* Restore terminal. */
  (void) tcsetattr (fileno (stream), TCSAFLUSH, &old);

  return nread;
}



/* Main. */

/**
 * @brief Main function for the OpenVAS Administrator.
 *
 * @param argc Number of command line arguments.
 * @param argv Array of command line arguments
 *
 * @todo The logging should be made more consistent when not in daemon mode;
 * e.g. g_warnings on EXIT_FAILURE, a separate log handler for console mode
 * and so on.
 *
 * @todo sync_script should be called nvt_script for consistency now that
 * there is more than one synchronization script. Other command line parameters
 * and variable should be changed to reflect this as well.
 *
 * @return EXIT_SUCCESS if execution of the requested action was successful,
 * EXIT_FAILURE if not.
 */
int
main (int argc, char **argv)
{
  int administrator_port;
  gchar *rc_name;

  /* Process options. */

  static gboolean foreground = FALSE;
  static gboolean print_version = FALSE;
  static gboolean enable_modify_settings = FALSE;
  static gboolean disable_password_policy = FALSE;
  static gchar *administrator_address_string = NULL;
  static gchar *administrator_port_string = NULL;
  static gchar *command = NULL;
  static const gchar *users_dir = NULL;
  static const gchar *scanner_config_file = NULL;
  static const gchar *name = NULL;
  static const gchar *role = NULL;
  static const gchar *password = NULL;
  static const gchar *account = NULL;
  static const gchar *rules_file = NULL;
  static const gchar *sync_script = NULL;
  static const gchar *scap_script = NULL;
  static const gchar *cert_script = NULL;
  static gboolean print_feed_version = FALSE;
  static gboolean perform_feed_sync = FALSE;
  static gboolean print_sync_status = FALSE;

  GError *error = NULL;

  GOptionContext *option_context;
  static GOptionEntry option_entries[] = {
    {"version", 'V', 0, G_OPTION_ARG_NONE, &print_version,
     "Print version.", NULL},
    {"verbose", 'v', 0, G_OPTION_ARG_NONE, &verbose,
     "Verbose messages.", NULL},
    {"foreground", 'f', 0, G_OPTION_ARG_NONE, &foreground,
     "Run in foreground.", NULL},
    {"listen", 'a', 0, G_OPTION_ARG_STRING, &administrator_address_string,
     "Listen on <address>.", "<address>"},
    {"port", 'p', 0, G_OPTION_ARG_STRING, &administrator_port_string,
     "Use port number <number>.", "<number>"},
    {"command", 'c', 0, G_OPTION_ARG_STRING, &command,
     "OAP command (e.g. add_user, remove_user, list_users)", "<command>"},
    /** @deprecated Hidden option for backwards compatibility */
    {"name", 'n', G_OPTION_FLAG_HIDDEN, G_OPTION_ARG_STRING, &name,
     "Username when creating, editing or removing a user", "<name>"},
    {"username", 'u', 0, G_OPTION_ARG_STRING, &name,
     "Username when creating, editing or removing a user", "<name>"},
    {"password", 'w', 0, G_OPTION_ARG_STRING, &password,
     "Password for the new user", "<password>"},
    {"role", 'r', 0, G_OPTION_ARG_STRING, &role,
     "Role when creating or modifying a user (User, Admin or Observer)", "<role>"},
    {"account", 't', 0, G_OPTION_ARG_STRING, &account,
     "Username and password for new user (overrides -u and -w)",
     "<username:password>"},
    {"rules-file", 0, 0, G_OPTION_ARG_FILENAME, &rules_file,
     "File containing the rules for the user",
     "<rules-file>"},
#ifndef S_SPLINT_S
    {"users-dir", 0, 0, G_OPTION_ARG_FILENAME, &users_dir,
     "Directory containing the OpenVAS user data (default: " OPENVAS_USERS_DIR
     ")",
     "<users-dir>"},
    {"scanner-config-file", 0, 0, G_OPTION_ARG_FILENAME, &scanner_config_file,
     "File containing the OpenVAS-Scanner configuration (default: "
     OPENVAS_CONFIG_FILE ")",
     "<config-file>"},
#endif /* S_SPLINT_S */
    {"sync-script", 's', 0, G_OPTION_ARG_FILENAME, &sync_script,
     "Script to use for NVT feed synchronization",
     "<sync-script>"},
    {"scap-script", 'A', 0, G_OPTION_ARG_FILENAME, &scap_script,
     "Script to use for SCAP feed synchronization",
     "<scap-script>"},
    {"cert-script", 'C', 0, G_OPTION_ARG_FILENAME, &cert_script,
     "Script to use for CERT feed synchronization",
     "<cert-script>"},
    {"feed-version", 'F', 0, G_OPTION_ARG_NONE, &print_feed_version,
     "Print version of the installed NVT feed.", NULL},
    {"sync-feed", 'S', 0, G_OPTION_ARG_NONE, &perform_feed_sync,
     "Synchronize the installed NVT feed.", NULL},
    {"print-sync-status", 'T', 0, G_OPTION_ARG_NONE, &print_sync_status,
     "Print the synchronization status of the installed NVT feed.", NULL},
    {"enable-modify-settings", '\0', 0, G_OPTION_ARG_NONE,
     &enable_modify_settings, "Enable the OAP MODIFY_SETTINGS command.", NULL},
    {"disable-password-policy", '\0', 0, G_OPTION_ARG_NONE,
     &disable_password_policy, "Do not restrict passwords to the policy.",
     NULL},
    {NULL}
  };

  option_context = g_option_context_new ("- Administrator of the Open Vulnerability Assessment System");
  g_option_context_add_main_entries (option_context, option_entries, NULL);
  if (!g_option_context_parse (option_context, &argc, &argv, &error))
    {
      g_option_context_free (option_context);
      g_error ("%s\n\n", error->message);
    }
  g_option_context_free (option_context);

  /* @todo Check that the options make sense, for example --name needs --command
   *       and --foreground excludes --name, etc. */

  if (print_version)
    {
      printf ("OpenVAS Administrator %s\n", OPENVASAD_VERSION);
      printf ("Copyright (C) 2013 Greenbone Networks GmbH\n");
      printf ("License GPLv2+: GNU GPL version 2 or later\n");
      printf
        ("This is free software: you are free to change and redistribute it.\n"
         "There is NO WARRANTY, to the extent permitted by law.\n\n");
      return EXIT_SUCCESS;
    }

  if (print_feed_version)
    {
      if (sync_script == NULL)
        {
          if (verbose)
            g_warning
              ("You need to provide a script to retrieve the feed version.");
          exit (EXIT_FAILURE);
        }

      gchar *script_feed_version = NULL;

      if (openvas_get_sync_script_feed_version
          (sync_script, &script_feed_version))
        {
          printf ("%s\n", g_strstrip (script_feed_version));
          exit (EXIT_SUCCESS);
        }
      else
        {
          if (verbose)
            g_message ("The synchronization script %s failed to return a "
                       "feed_version!", sync_script);
          exit (EXIT_FAILURE);
        }
    }

  /* Disable the password policy if requested.  */
  if (disable_password_policy)
    openvas_disable_password_policy ();

  if (perform_feed_sync)
    {
      if (sync_script == NULL)
        {
          if (verbose)
            g_warning ("You need to provide a script to use for feed"
                       " synchronization.");
          exit (EXIT_FAILURE);
        }

      switch (openvas_sync_feed
              (sync_script, name ? name : "openvasad command line user",
               NVT_FEED))
        {
        case 0:
          {
            int status;

            /* Parent on success. */

            while (wait (&status) < 0)
              {
                if (errno == ECHILD)
                  {
                    if (verbose)
                      g_warning ("Failed to get child exit status");
                    exit (EXIT_FAILURE);
                  }
                if (errno == EINTR)
                  continue;
                if (verbose)
                  g_warning ("wait: %s", strerror (errno));
                exit (EXIT_FAILURE);
              }
            if (WIFEXITED (status))
              switch (WEXITSTATUS (status))
                {
                case EXIT_SUCCESS:
                  if (verbose)
                    g_message ("Feed synchronization successful.");
                  exit (EXIT_SUCCESS);
                  break;
                case 2:
                  if (verbose)
                    g_warning ("Feed is already synchronizing, exiting.");
                  exit (EXIT_FAILURE);
                  break;
                case EXIT_FAILURE:
                  if (verbose)
                    g_message ("Error during synchronization.");
                  exit (EXIT_FAILURE);
                  break;
                }
            if (verbose)
              g_message ("Error during synchronization.");
            exit (EXIT_FAILURE);
          }
          break;
        case 1:
          /* Parent. */
          if (verbose)
            g_warning ("Feed is already synchronizing, exiting.");
          exit (EXIT_FAILURE);
          break;
        case 2:
          /* Child on success. */
          exit (EXIT_SUCCESS);
          break;
        case 11:
          /* Child, someone else got the lock. */
          exit (2);
          break;
        case -10:
          /* Child error. */
          exit (EXIT_FAILURE);
          break;
        default:
          assert (0);
        case -1:
          /* Parent error. */
          if (verbose)
            g_warning ("Failed to execute synchronization script (%s)!",
                       sync_script);
          exit (EXIT_FAILURE);
          break;
        }
      exit (EXIT_FAILURE);
    }

  if (print_sync_status)
    {
      if (sync_script == NULL)
        {
          if (verbose)
            g_warning
              ("You need to provide a script to retrieve the synchronization status.");
          exit (EXIT_FAILURE);
        }

      gchar *timestamp = NULL;
      gchar *sync_user = NULL;

      switch (openvas_current_sync (sync_script, &timestamp, &sync_user))
        {
        case 0:
          /* Feed is currently not being synced */
          printf ("No feed synchronization in progress.\n");
          exit (EXIT_SUCCESS);
          break;
        case 1:
          /* Sync in progress */
          printf ("The feed is currently being synchronized. "
                  "Synchronization started by %s at %s.\n",
                  g_strstrip (sync_user), g_strstrip (timestamp));
          g_free (sync_user);
          g_free (timestamp);
          exit (EXIT_SUCCESS);
        case -1:
          /* Error */
          if (verbose)
            g_warning ("Failed to determine synchronization status.");
          exit (EXIT_FAILURE);
        default:
          break;
        }

    }

  if (command)
    {
      GString *response;

      /* Run a single OAP command and exit. */

      /* Log everything to stdout and stderr. */
      g_log_set_default_handler ((GLogFunc) openvas_log_func, NULL);

      infof ("   OpenVAS Administrator\n");

      if (users_dir == NULL)
        {
          infof ("   users_dir not set, setting to default\n");
          users_dir = OPENVAS_USERS_DIR;
        }

      infof ("   Using directory %s as the users directory\n", users_dir);

      if (scanner_config_file == NULL)
        {
          infof ("   scanner_config_file not set, setting to default\n");
          scanner_config_file = OPENVAS_CONFIG_FILE;
        }

      infof ("   Using %s as the scanner config file\n", scanner_config_file);

      /* account overrides name and password.  If an account is specified, the
       * username and password are extracted and copied into name and password
       * respectively. */
      if (account)
        {
          gchar *s = (gchar *) account;
          while (s[0] && s[0] != ':')
            s++;
          if (s[0])
            {
              s[0] = '\0';
              s++;
            }
          if (account[0])
            name = account;
          if (s[0])
            password = s;
        }

      /** @todo Some of these respond with XML, others with text messages. */

      response = g_string_new (NULL);
      g_strstrip (command);
      if (g_ascii_strcasecmp (command, "list_users") == 0)
        {
          GSList *user_list = openvas_admin_list_users (users_dir, 1, NULL);
          g_string_free (response, TRUE);
          response = print_users_text (user_list);
          g_slist_free (user_list);
        }
      /** @todo For consistency with protocol, rename to "create_user". */
      else if (g_ascii_strcasecmp (command, "add_user") == 0)
        {
          gchar *rules;

          if (name == NULL)
            {
              g_warning ("You need to provide a username to"
                         " create a new user.");
              exit (EXIT_FAILURE);
            }

          if (password == NULL)
            {
              gchar *pw = NULL;
              size_t n;

              printf ("Enter password: ");
              fflush (stdout);
              int ret = read_password (&pw, &n, stdin);
              printf ("\n");

              if (ret < 0)
                {
                  g_warning ("Failed to read password from console!");
                  exit (EXIT_FAILURE);
                }

              /* Remove the trailing newline character. */
              if (ret)
                pw[ret - 1] = '\0';

              if (strlen (pw) > 0)
                password = pw;
              else
                {
                  g_warning ("You need to provide a password to"
                             " create a new user.");
                  exit (EXIT_FAILURE);
                }
            }

          if (rules_file == NULL)
            {
              rules = NULL;
              g_message ("No rules file provided, the new user will have no"
                         " restrictions.");
            }
          else
            {
              if (!g_file_test (rules_file, G_FILE_TEST_EXISTS))
                {
                  g_warning ("Could not find rules file %s!", rules_file);
                  exit (EXIT_FAILURE);
                }
              if (!g_file_get_contents (rules_file, &rules, NULL, &error))
                {
                  g_warning ("Could not read contents of %s!", rules_file);
                  g_warning ("%s", error->message);
                  g_error_free (error);
                  exit (EXIT_FAILURE);
                }
            }

          // TODO Add source modification to standalone app. (i.e. do not
          //      pass NULL as pre-final argument).
          if (openvas_admin_add_user (name, password, role ? role : "User",
                                      NULL, 0, users_dir, NULL, NULL))
            {
              g_free (rules);
              g_warning ("Failed to create user %s!", name);
              exit (EXIT_FAILURE);
            }
          else
            {
              g_free (rules);
              g_message ("User %s has been successfully created.", name);
              exit (EXIT_SUCCESS);
            }
        }
      else if (g_ascii_strcasecmp (command, "remove_user") == 0)
        {
          if (name == NULL)
            {
              g_warning ("You need to provide the name of the user to be"
                         " deleted");
              exit (EXIT_FAILURE);
            }
          if (openvas_admin_remove_user (name, users_dir))
            {
              g_warning ("Failed to remove user %s!", name);
              exit (EXIT_FAILURE);
            }
          else
            {
              g_message ("User %s has been successfully removed.", name);
              exit (EXIT_SUCCESS);
            }
        }
      else if (g_ascii_strcasecmp (command, "set_rules") == 0)
        {
          if (name == NULL || rules_file == NULL)
            {
              g_warning ("You need to provide both the name of the user and"
                         " the file containing the new rules to set new rules.");
              exit (EXIT_FAILURE);
            }
          if (openvas_admin_set_rules (name, rules_file, users_dir))
            {
              g_message
                ("The rules for user %s have been successfully changed.", name);
              exit (EXIT_SUCCESS);
            }
          else
            {
              g_warning ("Failed to change the rules for user %s!", name);
              exit (EXIT_FAILURE);
            }
        }
      else if (g_ascii_strcasecmp (command, "set_role") == 0)
        {
          if (name == NULL || role == NULL)
            {
              g_warning ("You need to provide both the name of the user and"
                         " the new role for the user.");
              exit (EXIT_FAILURE);
            }
          if (strcmp (name, "om") == 0)
            {
              g_warning ("Attempt to set role of special \"om\" user.");
              exit (EXIT_FAILURE);
            }
          if (openvas_set_user_role (name, role, NULL))
            {
              g_warning ("Failed to change the role of user %s!", name);
              exit (EXIT_FAILURE);
            }
          else
            {
              g_message ("The role of user %s has been successfully changed.",
                         name);
              exit (EXIT_SUCCESS);
            }
        }
      else if (g_ascii_strcasecmp (command, "sync_feed") == 0)
        {
          if (sync_script == NULL)
            {
              g_warning ("You need to provide a script to use for feed"
                         " synchronization.");
              exit (EXIT_FAILURE);
            }

          switch (openvas_sync_feed
                  (sync_script, name ? name : "openvasad command line user",
                   NVT_FEED))
            {
            case 0:
              {
                int status;

                /* Parent on success. */

                while (wait (&status) < 0)
                  {
                    if (errno == ECHILD)
                      {
                        g_warning ("Failed to get child exit status");
                        exit (EXIT_FAILURE);
                      }
                    if (errno == EINTR)
                      continue;
                    g_warning ("wait: %s", strerror (errno));
                    exit (EXIT_FAILURE);
                  }
                if (WIFEXITED (status))
                  switch (WEXITSTATUS (status))
                    {
                    case EXIT_SUCCESS:
                      g_message ("Feed synchronization successful.");
                      exit (EXIT_SUCCESS);
                      break;
                    case 2:
                      g_warning ("Feed is already synchronizing, exiting.");
                      exit (EXIT_FAILURE);
                      break;
                    case EXIT_FAILURE:
                      g_message ("Error during synchronization.");
                      exit (EXIT_FAILURE);
                      break;
                    }
                g_message ("Error during synchronization.");
                exit (EXIT_FAILURE);
              }
              break;
            case 1:
              /* Parent. */
              g_warning ("Feed is already synchronizing, exiting.");
              exit (EXIT_FAILURE);
              break;
            case 2:
              /* Child on success. */
              exit (EXIT_SUCCESS);
              break;
            case 11:
              /* Child, someone else got the lock. */
              exit (2);
              break;
            case -10:
              /* Child error. */
              exit (EXIT_FAILURE);
              break;
            default:
              assert (0);
            case -1:
              /* Parent error. */
              g_warning ("Failed to execute synchronization script (%s)!",
                         sync_script);
              exit (EXIT_FAILURE);
              break;
            }
        }
      else if (g_ascii_strcasecmp (command, "test_sync_script") == 0)
        {
          if (sync_script == NULL)
            {
              g_warning ("You need to provide a script to use for "
                         " testing.");
              exit (EXIT_FAILURE);
            }

          gchar *selftest_result = NULL;

          if (openvas_sync_script_perform_selftest
              (sync_script, &selftest_result))
            {
              g_message ("The synchronization script %s is fully functional.",
                         sync_script);
              exit (EXIT_SUCCESS);
            }
          else
            {
              g_message ("The synchronization script %s reported the following"
                         " issues:\n%s", sync_script, selftest_result);
              g_free (selftest_result);
              exit (EXIT_FAILURE);
            }
        }
      else if (g_ascii_strcasecmp (command, "describe_sync_script") == 0)
        {
          if (sync_script == NULL)
            {
              g_warning ("You need to provide a script to describe.");
              exit (EXIT_FAILURE);
            }

          gchar *script_description = NULL;

          if (openvas_get_sync_script_description
              (sync_script, &script_description))
            {
              g_message ("The synchronization script %s returned the following "
                         "description:\n%s", sync_script, script_description);
              exit (EXIT_SUCCESS);
            }
          else
            {
              g_message ("The synchronization script %s failed to return a "
                         "description!", sync_script);
              g_free (script_description);
              exit (EXIT_FAILURE);
            }
        }
      else if (g_ascii_strcasecmp (command, "get_feed_version") == 0)
        {
          if (sync_script == NULL)
            {
              g_warning
                ("You need to provide a script to retrieve the feed version.");
              exit (EXIT_FAILURE);
            }

          gchar *script_feed_version = NULL;

          if (openvas_get_sync_script_feed_version
              (sync_script, &script_feed_version))
            {
              g_message ("The synchronization script %s returned the following "
                         "feed_version:\n%s", sync_script, script_feed_version);
              exit (EXIT_SUCCESS);
            }
          else
            {
              g_message ("The synchronization script %s failed to return a "
                         "feed_version!", sync_script);
              g_free (script_feed_version);
              exit (EXIT_FAILURE);
            }
        }
      else if (g_ascii_strcasecmp (command, "get_settings") == 0)
        {
          settings_iterator_t settings;

          if (init_settings_iterator (&settings, scanner_config_file, "Misc"))
            {
              g_warning ("Failed to initialise settings iterator!");
              exit (EXIT_FAILURE);
            }
          else
            {
              while (settings_iterator_next (&settings))
                g_string_append_printf (response, "%s=%s\n",
                                        settings_iterator_name (&settings),
                                        settings_iterator_value (&settings));
              cleanup_settings_iterator (&settings);
            }
        }
      else
        {
          g_warning ("Invalid command!");
          exit (EXIT_FAILURE);
        }
      g_printf ("%s\n", response->str);
      g_string_free (response, TRUE);

      return EXIT_SUCCESS;
    }

  /* Run the daemon. */

  /* Setup logging. */

  rc_name = g_build_filename (OPENVAS_SYSCONF_DIR, "openvasad_log.conf", NULL);
  if (g_file_test (rc_name, G_FILE_TEST_EXISTS))
    log_config = load_log_configuration (rc_name);
  g_free (rc_name);
  setup_log_handlers (log_config);

  infof ("   OpenVAS Administrator\n");

  if (users_dir == NULL)
    {
      infof ("   users_dir not set, setting to default\n");
      users_dir = OPENVAS_USERS_DIR;
    }

  infof ("   Using directory %s as the users directory\n", users_dir);

  if (sync_script == NULL)
    {
      infof ("   sync_script not set, setting to default\n");
      sync_script = OPENVAS_SYNC_SCRIPT_DIR "/openvas-nvt-sync";
    }

  if (scap_script == NULL)
    {
      infof ("   scap_script not set, setting to default\n");
      scap_script = OPENVAS_SYNC_SCRIPT_DIR "/openvas-scapdata-sync";
    }

  if (cert_script == NULL)
    {
      infof ("   cert_script not set, setting to default\n");
      cert_script = OPENVAS_SYNC_SCRIPT_DIR "/openvas-certdata-sync";
    }

  infof ("   Using %s as the nvt synchronization script\n", sync_script);
  infof ("   Using %s as the scap synchronization script\n", scap_script);
  infof ("   Using %s as the cert synchronization script\n", cert_script);

  if (scanner_config_file == NULL)
    {
      infof ("   scanner_config_file not set, setting to default\n");
      scanner_config_file = OPENVAS_CONFIG_FILE;
    }

  infof ("   Using %s as the scanner config file\n", scanner_config_file);

  if (administrator_port_string)
    {
      administrator_port = atoi (administrator_port_string);
      if (administrator_port <= 0 || administrator_port >= 65536)
        {
          g_critical ("%s: port must be a number between 0 and 65536\n",
                      __FUNCTION__);
          free_log_configuration (log_config);
          exit (EXIT_FAILURE);
        }
      administrator_port = htons (administrator_port);
    }
  else
    {
      struct servent *servent = getservbyname ("oap", "tcp");
      if (servent)
        // FIX free servent?
        administrator_port = servent->s_port;
      else
        administrator_port = htons (OPENVASAD_PORT);
    }

  /* Initialize authentication system. */
  openvas_auth_init ();

  if (foreground == FALSE)
    {
      /* Fork into the background. */
      pid_t pid = fork ();
      switch (pid)
        {
        case 0:
          /* Child. */
          break;
        case -1:
          /* Parent when error. */
          g_critical ("%s: failed to fork into background: %s\n", __FUNCTION__,
                      strerror (errno));
          free_log_configuration (log_config);
          exit (EXIT_FAILURE);
          break;
        default:
          /* Parent. */
          free_log_configuration (log_config);
          exit (EXIT_SUCCESS);
          break;
        }
    }

  /* Initialise OAP daemon. */

  switch (init_oapd
          (log_config, users_dir, sync_script, scap_script, cert_script,
           scanner_config_file, enable_modify_settings))
    {
    case 0:
      break;
    case -1:
    default:
      g_critical ("%s: failed to initialise OAP daemon\n", __FUNCTION__);
      free_log_configuration (log_config);
      exit (EXIT_FAILURE);
    }

  /* Register the `cleanup' function. */

  if (atexit (&cleanup))
    {
      g_critical ("%s: failed to register `atexit' cleanup function\n",
                  __FUNCTION__);
      free_log_configuration (log_config);
      exit (EXIT_FAILURE);
    }

  /* Create the administrator socket. */

  administrator_socket = socket (PF_INET, SOCK_STREAM, 0);
  if (administrator_socket == -1)
    {
      g_critical ("%s: failed to create administrator socket: %s\n",
                  __FUNCTION__, strerror (errno));
      exit (EXIT_FAILURE);
    }

  if (fcntl (administrator_socket, F_SETFD, FD_CLOEXEC, 1) == -1)
    {
      g_critical ("%s: failed to set FD_CLOEXEC on administrator socket: %s\n",
                  __FUNCTION__, strerror (errno));
      exit (EXIT_FAILURE);
    }

  {
    int optval = 1;
    if (setsockopt
        (administrator_socket, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof (int)))
      {
        g_critical ("%s: failed to set SO_REUSEADDR on socket: %s\n",
                    __FUNCTION__, strerror (errno));
        exit (EXIT_FAILURE);
      }
  }

#if LOG
  /* Open the log file. */

  if (g_mkdir_with_parents (OPENVAS_LOG_DIR, 0755)      /* "rwxr-xr-x" */
      == -1)
    {
      g_critical ("%s: failed to create log directory: %s\n", __FUNCTION__,
                  strerror (errno));
      exit (EXIT_FAILURE);
    }

  log_stream = fopen (LOG_FILE, "w");
  if (log_stream == NULL)
    {
      g_critical ("%s: failed to open log file: %s\n", __FUNCTION__,
                  strerror (errno));
      exit (EXIT_FAILURE);
    }
#endif

  /* Register the signal handlers. */

  /* Warning from RATS heeded (signals now use small, separate handlers)
   * hence annotations. */
  if (signal (SIGTERM, handle_sigterm) == SIG_ERR       /* RATS: ignore */
      || signal (SIGINT, handle_sigint) == SIG_ERR      /* RATS: ignore */
      || signal (SIGHUP, handle_sighup) == SIG_ERR      /* RATS: ignore */
      || signal (SIGCHLD, SIG_IGN) == SIG_ERR)  /* RATS: ignore */
    {
      g_critical ("%s: failed to register signal handler\n", __FUNCTION__);
      exit (EXIT_FAILURE);
    }

  /* Setup security. */

  if (openvas_server_new
      (GNUTLS_SERVER, OPENVAS_CA_CERTIFICATE, OPENVAS_SERVER_CERTIFICATE,
       OPENVAS_SERVER_KEY, &client_session, &client_credentials))
    {
      g_critical ("%s: client server initialisation failed\n", __FUNCTION__);
      exit (EXIT_FAILURE);
    }

  /* Bind the administrator socket to a port. */

  /* The socket must have O_NONBLOCK set, in case an "asynchronous network
   * error" removes the connection between `select' and `accept'. */
  if (fcntl (administrator_socket, F_SETFL, O_NONBLOCK) == -1)
    {
      g_critical ("%s: failed to set administrator socket flag: %s\n",
                  __FUNCTION__, strerror (errno));
      exit (EXIT_FAILURE);
    }

  administrator_address.sin_family = AF_INET;
  administrator_address.sin_port = administrator_port;
  if (administrator_address_string)
    {
      if (!inet_aton
          (administrator_address_string, &administrator_address.sin_addr))
        {
          g_critical ("%s: failed to create administrator address %s\n",
                      __FUNCTION__, administrator_address_string);
          exit (EXIT_FAILURE);
        }
    }
  else
    administrator_address.sin_addr.s_addr = INADDR_ANY;

  if (bind
      (administrator_socket, (struct sockaddr *) &administrator_address,
       sizeof (administrator_address)) == -1)
    {
      g_critical ("%s: failed to bind administrator socket: %s\n", __FUNCTION__,
                  strerror (errno));
      close (administrator_socket);
      exit (EXIT_FAILURE);
    }

  infof ("   Administrator bound to address %s port %i\n",
         administrator_address_string ? administrator_address_string : "*",
         ntohs (administrator_address.sin_port));

  /* Enable connections to the socket. */

  if (listen (administrator_socket, MAX_CONNECTIONS) == -1)
    {
      g_critical ("%s: failed to listen on administrator socket: %s\n",
                  __FUNCTION__, strerror (errno));
      close (administrator_socket);
      exit (EXIT_FAILURE);
    }

  /* Set our pidfile. */

  if (pidfile_create ("openvasad"))
    exit (EXIT_FAILURE);

  /* Loop waiting for connections and passing the work to
   * `accept_and_maybe_fork'.
   *
   * FIX This could just loop accept_and_maybe_fork.  Might the administrator
   *     want to communicate with anything else here, like the server?
   */

  while (1)
    {
      int ret, nfds;
      fd_set readfds, exceptfds;

      FD_ZERO (&readfds);
      FD_SET (administrator_socket, &readfds);
      FD_ZERO (&exceptfds);
      FD_SET (administrator_socket, &exceptfds);
      nfds = administrator_socket + 1;

      ret = select (nfds, &readfds, NULL, &exceptfds, NULL);

      if (ret == -1)
        {
          if (errno == EINTR)
            continue;
          g_critical ("%s: select failed: %s\n", __FUNCTION__,
                      strerror (errno));
          exit (EXIT_FAILURE);
        }
      if (ret > 0)
        {
          if (FD_ISSET (administrator_socket, &exceptfds))
            {
              g_critical ("%s: exception in select\n", __FUNCTION__);
              exit (EXIT_FAILURE);
            }
          if (FD_ISSET (administrator_socket, &readfds))
            accept_and_maybe_fork ();
        }
    }

  exit (EXIT_SUCCESS);
}
