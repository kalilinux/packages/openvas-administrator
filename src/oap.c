/* OpenVAS Administrator
 * $Id$
 * Description: Module for OpenVAS Administrator: the OAP library.
 *
 * Authors:
 * Matthew Mundell <matthew.mundell@greenbone.net>
 *
 * Copyright:
 * Copyright (C) 2009,2010 Greenbone Networks GmbH
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * or, at your option, any later version as published by the Free
 * Software Foundation
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file  oap.c
 * @brief The OpenVAS Administrator OAP library.
 *
 * This file defines an OpenVAS Administrator Protocol (OAP) library, for
 * implementing OpenVAS administrators such as the OpenVAS Administrator daemon.
 *
 * The library provides \ref process_oap_client_input.
 * This function parses a given string of OAP XML and performs administration
 * tasks in reaction to the OAP commands in the string.
 */

/**
 * @internal
 * The OAP-"Processor" is always in a state (\ref client_state_t
 * \ref client_state ) and currently looking at the opening of an OAP element
 * (\ref oap_xml_handle_start_element ), at the text of an OAP element
 * (\ref oap_xml_handle_text ) or at the closing of an OAP element
 * (\ref oap_xml_handle_end_element ).
 *
 * The state usually represents the current location of the parser within the
 * xml (oap) tree. There has to be one state for every oap element.
 *
 * State transitions occur in the start and end element handler callbacks.
 *
 * Generally, the strategy is to wait until the closing of an element before
 * doing any action or sending a response. Also, error cases are to be detected
 * in the end element handler.
 *
 * If data has to be stored, it goes to \ref command_data (_t) , which is a
 * union.
 * More specific incarnations of this union are e.g. \ref create_user_data (_t)
 * , where the data to create a new user is stored (until the end element of
 * that command is reached).
 *
 * For implementing new commands that have to store data (i.e. not
 * "<help_extended/>"), \ref command_data has to be freed and NULL'ed in case
 * of errors and the \ref current_state has to be reset.
 * It can then be assumed that it is NULL'ed at the start of every new
 * command element. To implement a new start element handler, be sure to just
 * copy an existing case and keep its structure.
 *
 * Implementationwise it is easier to represent values in in attributes than
 * in text or further elements.
 * E.g.
 * <key_value_pair key="k" value="v"/>
 * is obviously easier to handle than
 * <key><attribute name="k"/><value>v</value></key>
 * .
 *
 * However, it is preferred to avoid attributes and use the text of elements
 * instead, like in
 * <key_value_pair><key>k</key><value>v</value></key_value_pair>
 * .
 *
 * If new elements are built of multiple words, separate the words with an
 * underscore.
 */

/** @todo Various functions are duplicates of openvas-manager.  Merge. */

#include "admin.h"
#include "oap.h"
#include "tracef.h"

#include <assert.h>
#include <string.h>

#include <openvas/base/credentials.h>
#include <openvas/base/openvas_file.h>  /* for openvas_file_read_b64_encode */
#include <openvas/base/openvas_string.h>        /* for openvas_append_text */
#include <openvas/base/settings.h>
#include <openvas/misc/openvas_auth.h>
#include <openvas/misc/openvas_logging.h>

#ifdef S_SPLINT_S
#include "splint.h"
#endif

#undef G_LOG_DOMAIN
/**
 * @brief GLib log domain.
 */
#define G_LOG_DOMAIN "ad    oap"

/**
 * @brief The users directory for this daemon.
 */
static const gchar *users_dir = NULL;

/**
 * @brief The nvt synchronization script for this daemon.
 */
static const gchar *sync_script = NULL;

/**
 * @brief The scap synchronization script for this daemon.
 */
static const gchar *scap_script = NULL;

/**
 * @brief The CERT synchronization script for this daemon.
 */
static const gchar *cert_script = NULL;

/**
 * @brief The scanner configuration file for this daemon.
 */
static const gchar *scanner_config_file = NULL;

/**
 * @brief Current credentials during any OAP command.
 */
credentials_t current_credentials;


/* Authenticate. */

/**
 * @brief Authenticate credentials.
 *
 * @param[in]  credentials  Credentials.
 *
 * @return 0 authentication success, 1 authentication failure, -1 error.
 */
int
authenticate (credentials_t * credentials)
{
  if (credentials->username && credentials->password)
    {
      int fail;

      if (strcmp (credentials->username, "om") == 0)
        return 1;

      fail =
        openvas_authenticate (credentials->username, credentials->password);
      if (fail == 0)
        {
          /* User could authenticate. Is she also admin? */
          /* Note that the order of these checks matter, because information
           * about the role is only available AFTER successfull authentication
           * in the case of remote authentication (e.g. ldap). */
          if (openvas_is_user_admin (credentials->username))
            return 0;
          else
            return 1;
        }
      return fail;
    }
  return 1;
}


/* Help message. */

/** @warning tests/oap_help_0.c has to be adjusted if help_text is modified. */

static char *help_text = "\n"
  "    AUTHENTICATE     Authenticate with the administrator.\n"
  "    COMMANDS         Run a list of commands.\n"
  "    CREATE_USER      Create a new user.\n"
  "    DELETE_USER      Delete an existing user.\n"
  "    DESCRIBE_AUTH    Get details about the used authentication methods.\n"
  "    DESCRIBE_FEED    Get details of the NVT feed this administrator uses.\n"
  "    DESCRIBE_SCAP    Get details of the SCAP feed this administrator uses.\n"
  "    DESCRIBE_CERT    Get details of the CERT feed this administrator uses.\n"
  "    GET_SETTINGS     Get scanner settings.\n"
  "    GET_USERS        Get all users.\n"
  "    GET_VERSION      Get the OpenVAS Administrator Protocol version.\n"
  "    HELP             Get this help text.\n"
  "    MODIFY_AUTH      Modify the authentication methods.\n" "%s"
  "    MODIFY_USER      Modify a user.\n"
  "    SYNC_SCAP        Synchronize with a SCAP feed.\n"
  "    SYNC_CERT        Synchronize with a CERT feed.\n"
  "    SYNC_FEED        Synchronize with an NVT feed.\n";

static char *help_modify_settings =
  "    MODIFY_SETTINGS  Modify the scanner settings.\n";


/* Status codes. */

/** @todo Share with manager. */

/* HTTP status codes used:
 *
 *     200 OK
 *     201 Created
 *     202 Accepted
 *     400 Bad request
 *     401 Must auth
 *     404 Missing
 *     409 Busy/Conflict
 */

/**
 * @brief Response code for a syntax error.
 */
#define STATUS_ERROR_SYNTAX            "400"

/**
 * @brief Response code when authorisation is required.
 */
#define STATUS_ERROR_MUST_AUTH         "401"

/**
 * @brief Response code when authorisation is required.
 */
#define STATUS_ERROR_MUST_AUTH_TEXT    "Authenticate first"

/**
 * @brief Response code for forbidden access.
 */
#define STATUS_ERROR_ACCESS            "403"

/**
 * @brief Response code text for forbidden access.
 */
#define STATUS_ERROR_ACCESS_TEXT       "Access to resource forbidden"

/**
 * @brief Response code for a missing resource.
 */
#define STATUS_ERROR_MISSING           "404"

/**
 * @brief Response code text for a missing resource.
 */
#define STATUS_ERROR_MISSING_TEXT      "Resource missing"

/**
 * @brief Response code for a busy resource.
 */
#define STATUS_ERROR_BUSY              "409"

/**
 * @brief Response code text for a busy resource.
 */
#define STATUS_ERROR_BUSY_TEXT         "Resource busy"

/**
 * @brief Response code when authorisation failed.
 */
#define STATUS_ERROR_AUTH_FAILED       "400"

/**
 * @brief Response code text when authorisation failed.
 */
#define STATUS_ERROR_AUTH_FAILED_TEXT  "Authentication failed"

/**
 * @brief Response code on success.
 */
#define STATUS_OK                      "200"

/**
 * @brief Response code text on success.
 */
#define STATUS_OK_TEXT                 "OK"

/**
 * @brief Response code on success, when a resource is created.
 */
#define STATUS_OK_CREATED              "201"

/**
 * @brief Response code on success, when a resource is created.
 */
#define STATUS_OK_CREATED_TEXT         "OK, resource created"

/**
 * @brief Response code on success, when the operation will finish later.
 */
#define STATUS_OK_REQUESTED            "202"

/**
 * @brief Response code text on success, when the operation will finish later.
 */
#define STATUS_OK_REQUESTED_TEXT       "OK, request submitted"

/**
 * @brief Response code for an internal error.
 */
#define STATUS_INTERNAL_ERROR          "500"

/**
 * @brief Response code text for an internal error.
 */
#define STATUS_INTERNAL_ERROR_TEXT     "Internal error"

/**
 * @brief Response code when a service is down.
 */
#define STATUS_SERVICE_DOWN            "503"

/**
 * @brief Response code text when a service is down.
 */
#define STATUS_SERVICE_DOWN_TEXT       "Service temporarily down"


/* Command data passed between parser callbacks. */

typedef struct
{
  int sort_order;
  char *hosts;
  int hosts_allow;
  char *name;
  char *password;
  char *role;
  gchar *current_source;
  array_t *sources;
} create_user_data_t;

/**
 * @brief Reset CREATE_USER data.
 */
static void
create_user_data_reset (create_user_data_t * data)
{
  g_free (data->name);
  g_free (data->password);
  g_free (data->role);
  if (data->sources)
    {
      array_free (data->sources);
    }
  g_free (data->current_source);
  memset (data, 0, sizeof (create_user_data_t));
}

typedef struct
{
  int sort_order;
  char *name;
} delete_user_data_t;

/**
 * @brief Reset DELETE_USER data.
 */
static void
delete_user_data_reset (delete_user_data_t * data)
{
  g_free (data->name);
  memset (data, 0, sizeof (delete_user_data_t));
}

typedef struct
{
  gchar *name;
  int sort_order;
} get_users_data_t;

typedef struct
{
  char *format;
} help_data_t;

/**
 * @brief Reset HELP data.
 */
static void
help_data_reset (help_data_t * data)
{
  g_free (data->format);
  memset (data, 0, sizeof (help_data_t));
}

/**
 * @brief Reset GET_USERS data.
 */
static void
get_users_data_reset (get_users_data_t * data)
{
  g_free (data->name);
  memset (data, 0, sizeof (get_users_data_t));
}

typedef struct
{
  int sort_order;
  gchar *hosts;
  int hosts_allow;
  gboolean modify_password;
  gchar *name;
  gchar *password;
  gchar *role;
  array_t *sources;
  gchar *current_source;
} modify_user_data_t;


/**
 * @brief Reset MODIFY_USER data.
 */
static void
modify_user_data_reset (modify_user_data_t * data)
{
  g_free (data->name);
  g_free (data->password);
  g_free (data->role);
  // TODO: Evaluate memleak: hosts
  if (data->sources)
    {
      array_free (data->sources);
    }
  g_free (data->current_source);
  memset (data, 0, sizeof (modify_user_data_t));
}

/**
 * @brief Create a setting.
 *
 * @param[in]  name   Name of setting.  Freed by modify_settings_data_reset.
 * @param[in]  value  Value of setting.  Freed by modify_settings_data_reset.
 *
 * @return Freshly allocated setting.
 */
static admin_setting_t *
setting_new (gchar * name, gchar * value)
{
  admin_setting_t *setting = g_malloc (sizeof (admin_setting_t));
  setting->name = name;
  setting->value = value;
  return setting;
}

/**
 * @brief Holds attributes and elements of an oap*setting command.
 */
typedef struct
{
  array_t *settings;            /* admin_setting_t */
  gchar *setting_name;
  gchar *setting_value;
} modify_settings_data_t;

/**
 * @brief Reset MODIFY_SETTINGS data.
 */
static void
modify_settings_data_reset (modify_settings_data_t * data)
{
  if (data->settings)
    {
      int index = 0;
      const admin_setting_t *setting;

      while ((setting =
              (admin_setting_t *) g_ptr_array_index (data->settings, index++)))
        {
          g_free (setting->name);
          g_free (setting->value);
        }
      array_free (data->settings);
    }
  g_free (data->setting_name);
  g_free (data->setting_value);
  memset (data, 0, sizeof (modify_settings_data_t));
}

/**
 * @brief A simple key/value-pair.
 */
typedef struct
{
  gchar *key;                   ///< The key.
  gchar *value;                 ///< The value.
} auth_conf_setting_t;

/** @todo resolve forward decl. */
int find_attribute (const gchar ** attribute_names,
                    const gchar ** attribute_values, const char *attribute_name,
                    const gchar ** attribute_value);

/**
 * @brief Forms a authentication configuration key/value-pair from xml element.
 *
 * Parameters fit glibs xml_start_element callback data.
 *
 * Expected input is generated from an element like:
 * @code <auth_conf_setting key="key" value="value"> @endcode
 *
 * @param[in]  element_name      Name of current element.
 * @param[in]  attribute_names   Attribute names.
 * @param[in]  attribute_values  Attribute names.
 *
 * @return 0 if input is not generated from a auth_conf_setting xml element as
 *         expected, pointer to freshly allocated auth_conf_setting_t that
 *         owns it key and value string otherwise.
 */
static auth_conf_setting_t *
auth_conf_setting_from_xml (const gchar * element_name,
                            const gchar ** attribute_names,
                            const gchar ** attribute_values)
{
  auth_conf_setting_t *kvp = NULL;

  if (strcasecmp (element_name, "auth_conf_setting") == 0)
    {
      const gchar *key_attr;
      const gchar *val_attr;
      if (find_attribute (attribute_names, attribute_values, "key", &key_attr)
          && find_attribute (attribute_names, attribute_values, "value",
                             &val_attr))
        {
          kvp = g_malloc0 (sizeof (auth_conf_setting_t));
          kvp->key = g_strdup (key_attr);
          kvp->value = g_strdup (val_attr);
        }
    }

  return kvp;
}

/** @brief Authentication method settings. */
typedef struct
{
  gchar *group_name;            ///< Name of the current group
  GSList *settings;             ///< List of auth_conf_setting_t.
} auth_group_t;

/** @brief MODIFY_AUTH parameters. */
typedef struct
{
  GSList *groups;               ///< List of auth_group_t
  GSList *curr_group_settings;  ///< Settings of currently parsed group.
  gchar *curr_group_name;       ///< Name of currently parsed group.
} modify_auth_data_t;


/**
 * @brief Frees all data related tp a modify_auth command and nulls the command
 * @brief data struct.
 *
 * @param[in,out]  data  Data to free and null.
 */
void
modify_auth_data_reset (modify_auth_data_t * data)
{
  GSList *item = data->groups;
  GSList *subitem = NULL;
  while (item)
    {
      auth_group_t *group = (auth_group_t *) item->data;
      g_free (group->group_name);
      /* Free settings. */
      subitem = group->settings;
      while (subitem)
        {
          auth_conf_setting_t *kvp = (auth_conf_setting_t *) subitem->data;
          g_free (kvp->key);
          g_free (kvp->value);
          g_free (kvp);
          subitem = g_slist_next (subitem);
        }
      item = g_slist_next (item);
    }
  g_slist_free (data->groups);

  if (data->curr_group_settings)
    {
      item = data->curr_group_settings;
      while (item)
        {
          /* Free settings. */
          auth_conf_setting_t *kvp = (auth_conf_setting_t *) item->data;
          g_free (kvp->key);
          g_free (kvp->value);
          g_free (kvp);
          item = g_slist_next (item);
        }
      g_slist_free (data->curr_group_settings);
    }
  g_free (data->curr_group_name);
  memset (data, 0, sizeof (modify_auth_data_t));
}

/**
 * @brief Holds the request data (e.g. parsed attributes) for a command.
 */
typedef union
{
  create_user_data_t create_user;
  delete_user_data_t delete_user;
  get_users_data_t get_users;
  help_data_t help;
  modify_auth_data_t modify_auth;
  modify_settings_data_t modify_settings;
  modify_user_data_t modify_user;
} command_data_t;

/**
 * @brief Initialise command data.
 */
static void
command_data_init (command_data_t * data)
{
  memset (data, 0, sizeof (command_data_t));
}


/* Global variables. */

/**
 * @brief Hack for returning forked process status from the callbacks.
 */
int current_error;

/**
 * @brief Hack for returning fork status to caller.
 */
int forked;

/**
 * @brief Flag indicating whether MODIFY_SETTINGS command is served.
 */
gboolean enable_modify_settings = FALSE;

/**
 * @brief Parser callback data.
 */
command_data_t command_data;

/**
 * @brief Parser callback data for CREATE_USER.
 */
create_user_data_t *create_user_data = &(command_data.create_user);

/**
 * @brief Parser callback data for DELETE_USER.
 */
delete_user_data_t *delete_user_data = &(command_data.delete_user);

/**
 * @brief Parser callback data for GET_USERS.
 */
get_users_data_t *get_users_data = &(command_data.get_users);

/**
 * @brief Parser callback data for HELP.
 */
help_data_t *help_data = &(command_data.help);

/**
 * @brief Parser callback data for MODIFY_AUTH.
 */
modify_auth_data_t *modify_auth_data = &(command_data.modify_auth);

/**
 * @brief Parser callback data for MODIFY_SETTINGS.
 */
modify_settings_data_t *modify_settings_data = &(command_data.modify_settings);

/**
 * @brief Parser callback data for MODIFY_USER.
 */
modify_user_data_t *modify_user_data = &(command_data.modify_user);

/**
 * @brief Buffer of output to the client.
 */
char to_client[TO_CLIENT_BUFFER_SIZE];

/**
 * @brief The start of the data in the \ref to_client buffer.
 */
buffer_size_t to_client_start = 0;
/**
 * @brief The end of the data in the \ref to_client buffer.
 */
buffer_size_t to_client_end = 0;

/**
 * @brief Client input parsing context.
 */
static /*@null@*//*@only@*/ GMarkupParseContext *xml_context = NULL;

/**
 * @brief Client input parser.
 */
static GMarkupParser xml_parser;


/* Client state. */

/**
 * @brief Possible states of the client.
 */
typedef enum
{
  CLIENT_TOP,
  CLIENT_AUTHENTIC,

  CLIENT_AUTHENTICATE,
  CLIENT_AUTHENTIC_COMMANDS,
  CLIENT_COMMANDS,
  CLIENT_CREATE_USER,
  CLIENT_CREATE_USER_HOSTS,
  CLIENT_CREATE_USER_NAME,
  CLIENT_CREATE_USER_PASSWORD,
  CLIENT_CREATE_USER_ROLE,
  CLIENT_CREATE_USER_SOURCES,
  CLIENT_CREATE_USER_SOURCES_SOURCE,
  CLIENT_CREDENTIALS,
  CLIENT_CREDENTIALS_PASSWORD,
  CLIENT_CREDENTIALS_USERNAME,
  CLIENT_DELETE_USER,
  CLIENT_DESCRIBE_AUTH,
  CLIENT_DESCRIBE_FEED,
  CLIENT_DESCRIBE_SCAP,
  CLIENT_DESCRIBE_CERT,
  CLIENT_GET_SETTINGS,
  CLIENT_GET_USERS,
  CLIENT_HELP,
  CLIENT_MODIFY_AUTH,
  CLIENT_MODIFY_AUTH_GROUP,
  CLIENT_MODIFY_AUTH_GROUP_AUTHCONFSETTING,
  CLIENT_MODIFY_SETTINGS,
  CLIENT_MODIFY_SETTINGS_SETTING,
  CLIENT_MODIFY_SETTINGS_SETTING_NAME,
  CLIENT_MODIFY_SETTINGS_SETTING_VALUE,
  CLIENT_MODIFY_USER,
  CLIENT_MODIFY_USER_HOSTS,
  CLIENT_MODIFY_USER_NAME,
  CLIENT_MODIFY_USER_PASSWORD,
  CLIENT_MODIFY_USER_ROLE,
  CLIENT_MODIFY_USER_SOURCES,
  CLIENT_MODIFY_USER_SOURCES_SOURCE,
  CLIENT_SYNC_FEED,
  CLIENT_SYNC_SCAP,
  CLIENT_SYNC_CERT,
  CLIENT_VERSION,
  CLIENT_VERSION_AUTHENTIC
} client_state_t;

/**
 * @brief The state of the client.
 */
static client_state_t client_state = CLIENT_TOP;

/**
 * @brief Set the client state.
 */
static void
set_client_state (client_state_t state)
{
  client_state = state;
  tracef ("   client state set: %i\n", client_state);
}


/* Communication. */

/**
 * @brief Send a response message to the client.
 *
 * Queue a message in \ref to_client.
 *
 * @param[in]  msg  The message, a string.
 *
 * @return TRUE if out of space in to_client, else FALSE.
 */
static gboolean
send_to_client (char *msg)
{
  assert (to_client_end <= TO_CLIENT_BUFFER_SIZE);
  if (((buffer_size_t) TO_CLIENT_BUFFER_SIZE) - to_client_end < strlen (msg))
    {
      tracef ("   send_to_client out of space (%i < %zu)\n",
              ((buffer_size_t) TO_CLIENT_BUFFER_SIZE) - to_client_end,
              strlen (msg));
      return TRUE;
    }
  memmove (to_client + to_client_end, msg, strlen (msg));
  tracef ("-> client: %s\n", msg);
  to_client_end += strlen (msg);
  return FALSE;
}

/**
 * @brief Send an XML element error response message to the client.
 *
 * @param[in]  command  Command name.
 * @param[in]  element  Element name.
 *
 * @return TRUE if out of space in to_client, else FALSE.
 */
static gboolean
send_element_error_to_client (const char *command, const char *element)
{
  gchar *msg;
  gboolean ret;

  msg =
    g_strdup_printf ("<%s_response status=\"" STATUS_ERROR_SYNTAX
                     "\" status_text=\"Bogus element: %s\"/>", command,
                     element);
  ret = send_to_client (msg);
  g_free (msg);
  return ret;
}

/**
 * @brief Send an XML find error response message to the client.
 *
 * @param[in]  command  Command name.
 * @param[in]  type     Resource type.
 * @param[in]  id       Resource ID.
 *
 * @return TRUE if out of space in to_client, else FALSE.
 */
static gboolean
send_find_error_to_client (const char *command, const char *type,
                           const char *id)
{
  gchar *msg;
  gboolean ret;

  msg =
    g_strdup_printf ("<%s_response status=\"" STATUS_ERROR_MISSING
                     "\" status_text=\"Failed to find %s '%s'\"/>", command,
                     type, id);
  ret = send_to_client (msg);
  g_free (msg);
  return ret;
}

/**
 * @brief Set an out of space parse error on a GError.
 *
 * @param [out]  error  The error.
 */
static void
error_send_to_client (GError ** error)
{
  tracef ("   send_to_client out of space in to_client\n");
  g_set_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
               "Administrator out of space for reply to client.");
}


/* XML parser handlers. */

/**
 * @brief Expand to XML for a STATUS_ERROR_SYNTAX response.
 *
 * @param  tag   Name of the command generating the response.
 * @param  text  Text for the status_text attribute of the response.
 */
#define XML_ERROR_SYNTAX(tag, text)                      \
 "<" tag "_response"                                     \
 " status=\"" STATUS_ERROR_SYNTAX "\""                   \
 " status_text=\"" text "\"/>"

/**
 * @brief Expand to XML for a STATUS_ERROR_SYNTAX response.
 *
 * This is a variant of the \ref XML_ERROR_SYNTAX macro to allow for a
 * runtime defined syntax_text attribute value.
 *
 * @param  tag   Name of the command generating the response.
 * @param text   Value for the status_text attribute of the response.
 *               The function takes care of proper quoting.
 *
 * @return A malloced XML string.  The caller must use g_free to
 *         release it.
 */
static char *
make_xml_error_syntax (const char *tag, const char *text)
{
  char *textbuf;
  char *ret;

  textbuf = g_markup_escape_text (text, -1);
  ret = g_strdup_printf ("<%s_response status=\"" STATUS_ERROR_SYNTAX "\""
                         " status_text=\"%s\"/>", tag, textbuf);
  g_free (textbuf);
  return ret;
}


/**
 * @brief Expand to XML for a STATUS_ERROR_MISSING response.
 *
 * @param  tag  Name of the command generating the response.
 */
#define XML_ERROR_MISSING(tag)                           \
 "<" tag "_response"                                     \
 " status=\"" STATUS_ERROR_MISSING "\""                  \
 " status_text=\"" STATUS_ERROR_MISSING_TEXT "\"/>"

/**
 * @brief Expand to XML for a STATUS_ERROR_BUSY response.
 *
 * @param  tag  Name of the command generating the response.
 */
#define XML_ERROR_BUSY(tag)                              \
 "<" tag "_response"                                     \
 " status=\"" STATUS_ERROR_BUSY "\""                     \
 " status_text=\"" STATUS_ERROR_BUSY_TEXT "\"/>"

/**
 * @brief Expand to XML for a STATUS_ERROR_AUTH_FAILED response.
 *
 * @param  tag  Name of the command generating the response.
 */
#define XML_ERROR_AUTH_FAILED(tag)                       \
 "<" tag "_response"                                     \
 " status=\"" STATUS_ERROR_AUTH_FAILED "\""              \
 " status_text=\"" STATUS_ERROR_AUTH_FAILED_TEXT "\"/>"

/**
 * @brief Expand to XML for a STATUS_OK response.
 *
 * @param  tag  Name of the command generating the response.
 */
#define XML_OK(tag)                                      \
 "<" tag "_response"                                     \
 " status=\"" STATUS_OK "\""                             \
 " status_text=\"" STATUS_OK_TEXT "\"/>"

/**
 * @brief Expand to XML for a STATUS_OK_CREATED response.
 *
 * @param  tag  Name of the command generating the response.
 */
#define XML_OK_CREATED(tag)                              \
 "<" tag "_response"                                     \
 " status=\"" STATUS_OK_CREATED "\""                     \
 " status_text=\"" STATUS_OK_CREATED_TEXT "\"/>"

/**
 * @brief Expand to XML for a STATUS_OK_REQUESTED response.
 *
 * @param  tag  Name of the command generating the response.
 */
#define XML_OK_REQUESTED(tag)                            \
 "<" tag "_response"                                     \
 " status=\"" STATUS_OK_REQUESTED "\""                   \
 " status_text=\"" STATUS_OK_REQUESTED_TEXT "\"/>"

/**
 * @brief Expand to XML for a STATUS_INTERNAL_ERROR response.
 *
 * @param  tag  Name of the command generating the response.
 */
#define XML_INTERNAL_ERROR(tag)                          \
 "<" tag "_response"                                     \
 " status=\"" STATUS_INTERNAL_ERROR "\""                 \
 " status_text=\"" STATUS_INTERNAL_ERROR_TEXT "\"/>"

/**
 * @brief Expand to XML for a STATUS_SERVICE_DOWN response.
 *
 * @param  tag  Name of the command generating the response.
 */
#define XML_SERVICE_DOWN(tag)                            \
 "<" tag "_response"                                     \
 " status=\"" STATUS_SERVICE_DOWN "\""                   \
 " status_text=\"" STATUS_SERVICE_DOWN_TEXT "\"/>"

/** @todo move to openvas-libraries/xml? */
/**
 * @brief Find an attribute in a parser callback list of attributes.
 *
 * @param[in]   attribute_names   List of names.
 * @param[in]   attribute_values  List of values.
 * @param[in]   attribute_name    Name of sought attribute.
 * @param[out]  attribute_value   Attribute value return.
 *
 * @see copy_attribute_value if you need a copy of the attributes value.
 *
 * @return 1 if found, else 0.
 */
int
find_attribute (const gchar ** attribute_names, const gchar ** attribute_values,
                const char *attribute_name, const gchar ** attribute_value)
{
  while (*attribute_names && *attribute_values)
    if (strcmp (*attribute_names, attribute_name))
      attribute_names++, attribute_values++;
    else
      {
        *attribute_value = *attribute_values;
        return 1;
      }
  return 0;
}

/**
 * @brief Find an attribute in a parser callback list of attributes and copies
 * @brief its value.
 *
 * @param[in]   attribute_names   List of names.
 * @param[in]   attribute_values  List of values.
 * @param[in]   attribute_name    Name of sought attribute.
 * @param[out]  attribute         Location to copy attribute value to, if any.
 *                                Will be set to NULL, if attribute not found.
 *
 * @see find_attribute_value if you do not need a copy of the attributes value.
 *
 * @return TRUE if attribute was found, FALSE otherwise.
 */
static gboolean
copy_attribute_value (const gchar ** attribute_names,
                      const gchar ** attribute_values,
                      const char *attribute_name, gchar ** attribute_value)
{
  const gchar *return_attr;
  /* If attribute not found, just return FALSE. */
  if (find_attribute
      (attribute_names, attribute_values, attribute_name, &return_attr) == 0)
    {
      *attribute_value = NULL;
      return FALSE;
    }

  *attribute_value = g_strdup (return_attr);
  return TRUE;
}

/** @cond STATIC */

/**
 * @brief Send response message to client, returning on fail.
 *
 * Queue a message in \ref to_client with \ref send_to_client.  On failure
 * call \ref error_send_to_client on a GError* called "error" and do a return.
 *
 * @param[in]   msg    The message, a string.
 */
#define SEND_TO_CLIENT_OR_FAIL(msg)                                          \
  do                                                                         \
    {                                                                        \
      if (send_to_client (msg))                                              \
        {                                                                    \
          error_send_to_client (error);                                      \
          return;                                                            \
        }                                                                    \
    }                                                                        \
  while (0)

/**
 * @brief Send response message to client, returning on fail.
 *
 * Queue a message in \ref to_client with \ref send_to_client.  On failure
 * call \ref error_send_to_client on a GError* called "error" and do a return.
 *
 * @param[in]   format    Format string for message.
 * @param[in]   args      Arguments for format string.
 */
#define SENDF_TO_CLIENT_OR_FAIL(format, args...)                             \
  do                                                                         \
    {                                                                        \
      gchar* msg = g_markup_printf_escaped (format , ## args);               \
      if (send_to_client (msg))                                              \
        {                                                                    \
          g_free (msg);                                                      \
          error_send_to_client (error);                                      \
          return;                                                            \
        }                                                                    \
      g_free (msg);                                                          \
    }                                                                        \
  while (0)

/** @endcond */

/**
 * @brief Handle the start of an OAP XML element.
 *
 * React to the start of an XML element according to the current value
 * of \ref client_state, usually adjusting \ref client_state to indicate
 * the change (with \ref set_client_state).  Call \ref send_to_client to
 * queue any responses for the client.
 *
 * Set error parameter on encountering an error.
 *
 * @param[in]  context           Parser context.
 * @param[in]  element_name      XML element name.
 * @param[in]  attribute_names   XML attribute name.
 * @param[in]  attribute_values  XML attribute values.
 * @param[in]  user_data         Dummy parameter.
 * @param[in]  error             Error parameter.
 */
static void
oap_xml_handle_start_element ( /*@unused@ */ GMarkupParseContext * context,
                              const gchar * element_name,
                              const gchar ** attribute_names,
                              const gchar ** attribute_values,
                              /*@unused@ */ gpointer user_data,
                              GError ** error)
{
  tracef ("   XML  start: %s (%i)\n", element_name, client_state);

  switch (client_state)
    {
    case CLIENT_TOP:
      if (strcasecmp ("GET_VERSION", element_name) == 0)
        {
          set_client_state (CLIENT_VERSION);
          break;
        }
      /*@fallthrough@*/
    case CLIENT_COMMANDS:
      if (strcasecmp ("AUTHENTICATE", element_name) == 0)
        {
          assert (current_credentials.username == NULL);
          assert (current_credentials.password == NULL);
          set_client_state (CLIENT_AUTHENTICATE);
        }
      else if (strcasecmp ("COMMANDS", element_name) == 0)
        {
          SENDF_TO_CLIENT_OR_FAIL ("<commands_response" " status=\"" STATUS_OK
                                   "\" status_text=\"" STATUS_OK_TEXT "\">");
          set_client_state (CLIENT_COMMANDS);
        }
      else
        {
          // TODO: If one of other commands, STATUS_ERROR_MUST_AUTH
          if (send_to_client
               (XML_ERROR_SYNTAX ("oap",
                                  "First command must be AUTHENTICATE,"
                                  " COMMANDS or GET_VERSION")))
            {
              error_send_to_client (error);
              return;
            }
          if (client_state == CLIENT_COMMANDS)
            send_to_client ("</commands_response>");
          g_set_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ELEMENT,
                       "Must authenticate first.");
        }
      break;

    case CLIENT_AUTHENTIC:
    case CLIENT_AUTHENTIC_COMMANDS:
      if (strcasecmp ("AUTHENTICATE", element_name) == 0)
        {
          /* Any config backend cleanup goes here. */
          free_credentials (&current_credentials);
          set_client_state (CLIENT_AUTHENTICATE);
        }
      else if (strcasecmp ("COMMANDS", element_name) == 0)
        {
          SEND_TO_CLIENT_OR_FAIL ("<commands_response" " status=\"" STATUS_OK
                                  "\" status_text=\"" STATUS_OK_TEXT "\">");
          set_client_state (CLIENT_AUTHENTIC_COMMANDS);
        }
      else if (strcasecmp ("CREATE_USER", element_name) == 0)
        set_client_state (CLIENT_CREATE_USER);
      else if (strcasecmp ("DELETE_USER", element_name) == 0)
        {
          const gchar *attribute;
          if (find_attribute
              (attribute_names, attribute_values, "name", &attribute))
            openvas_append_string (&delete_user_data->name, attribute);
          set_client_state (CLIENT_DELETE_USER);
        }
      else if (strcasecmp ("DESCRIBE_FEED", element_name) == 0)
        set_client_state (CLIENT_DESCRIBE_FEED);
      else if (strcasecmp ("DESCRIBE_SCAP", element_name) == 0)
        set_client_state (CLIENT_DESCRIBE_SCAP);
      else if (strcasecmp ("DESCRIBE_CERT", element_name) == 0)
        set_client_state (CLIENT_DESCRIBE_CERT);
      else if (strcasecmp ("DESCRIBE_AUTH", element_name) == 0)
        set_client_state (CLIENT_DESCRIBE_AUTH);
      else if (strcasecmp ("GET_USERS", element_name) == 0)
        {
          const gchar *attribute;
          if (find_attribute
              (attribute_names, attribute_values, "name", &attribute))
            openvas_append_string (&get_users_data->name, attribute);
          if (find_attribute
              (attribute_names, attribute_values, "sort_order", &attribute))
            get_users_data->sort_order = strcmp (attribute, "descending");
          else
            get_users_data->sort_order = 1;
          set_client_state (CLIENT_GET_USERS);
        }
      else if (strcasecmp ("HELP", element_name) == 0)
        {
          const gchar *attribute;
          if (find_attribute
              (attribute_names, attribute_values, "format", &attribute))
            openvas_append_string (&help_data->format, attribute);
          set_client_state (CLIENT_HELP);
        }
      else if (strcasecmp ("GET_SETTINGS", element_name) == 0)
        set_client_state (CLIENT_GET_SETTINGS);
      else if (strcasecmp ("GET_VERSION", element_name) == 0)
        set_client_state (CLIENT_VERSION_AUTHENTIC);
      else if (strcasecmp ("MODIFY_AUTH", element_name) == 0)
        set_client_state (CLIENT_MODIFY_AUTH);
      else if (enable_modify_settings
               && (strcasecmp ("MODIFY_SETTINGS", element_name) == 0))
        {
          modify_settings_data->settings = make_array ();
          set_client_state (CLIENT_MODIFY_SETTINGS);
        }
      else if (strcasecmp ("MODIFY_USER", element_name) == 0)
        set_client_state (CLIENT_MODIFY_USER);
      else if (strcasecmp ("SYNC_FEED", element_name) == 0)
        set_client_state (CLIENT_SYNC_FEED);
      else if (strcasecmp ("SYNC_SCAP", element_name) == 0)
        set_client_state (CLIENT_SYNC_SCAP);
      else if (strcasecmp ("SYNC_CERT", element_name) == 0)
        set_client_state (CLIENT_SYNC_CERT);
      else
        {
          if (send_to_client (XML_ERROR_SYNTAX ("oap", "Bogus command name")))
            {
              error_send_to_client (error);
              return;
            }
          g_set_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ELEMENT,
                       "Error");
        }
      break;

    case CLIENT_AUTHENTICATE:
      if (strcasecmp ("CREDENTIALS", element_name) == 0)
        set_client_state (CLIENT_CREDENTIALS);
      else
        {
          if (send_element_error_to_client ("authenticate", element_name))
            {
              error_send_to_client (error);
              return;
            }
          free_credentials (&current_credentials);
          set_client_state (CLIENT_TOP);
          g_set_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ELEMENT,
                       "Error");
        }
      break;

    case CLIENT_CREATE_USER:
      if (strcasecmp ("HOSTS", element_name) == 0)
        {
          const gchar *attribute;
          if (find_attribute
              (attribute_names, attribute_values, "allow", &attribute))
            create_user_data->hosts_allow = strcmp (attribute, "0");
          else
            create_user_data->hosts_allow = 1;
          set_client_state (CLIENT_CREATE_USER_HOSTS);
        }
      else if (strcasecmp ("NAME", element_name) == 0)
        set_client_state (CLIENT_CREATE_USER_NAME);
      else if (strcasecmp ("PASSWORD", element_name) == 0)
        set_client_state (CLIENT_CREATE_USER_PASSWORD);
      else if (strcasecmp ("ROLE", element_name) == 0)
        set_client_state (CLIENT_CREATE_USER_ROLE);
      else if (strcasecmp ("SOURCES", element_name) == 0)
        {
          create_user_data->sources = make_array ();
          set_client_state (CLIENT_CREATE_USER_SOURCES);
        }
      else
        {
          if (send_element_error_to_client ("create_user", element_name))
            {
              error_send_to_client (error);
              return;
            }
          set_client_state (CLIENT_AUTHENTIC);
          g_set_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ELEMENT,
                       "Error");
        }
      break;
    case CLIENT_CREATE_USER_SOURCES:
      if (strcasecmp ("SOURCE", element_name) == 0)
	set_client_state (CLIENT_CREATE_USER_SOURCES_SOURCE);
      else
        {
          if (send_element_error_to_client ("create_user", element_name))
            {
              error_send_to_client (error);
              return;
            }
          set_client_state (CLIENT_AUTHENTIC);
          g_set_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ELEMENT,
                       "Error");
        }
      break;

    case CLIENT_CREDENTIALS:
      if (strcasecmp ("USERNAME", element_name) == 0)
        set_client_state (CLIENT_CREDENTIALS_USERNAME);
      else if (strcasecmp ("PASSWORD", element_name) == 0)
        set_client_state (CLIENT_CREDENTIALS_PASSWORD);
      else
        {
          if (send_element_error_to_client ("authenticate", element_name))
            {
              error_send_to_client (error);
              return;
            }
          free_credentials (&current_credentials);
          set_client_state (CLIENT_TOP);
          g_set_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ELEMENT,
                       "Error");
        }
      break;

    case CLIENT_DELETE_USER:
      if (send_element_error_to_client ("delete_user", element_name))
        {
          error_send_to_client (error);
          return;
        }
      set_client_state (CLIENT_AUTHENTIC);
      g_set_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ELEMENT,
                   "Error");
      break;

    case CLIENT_GET_USERS:
      {
        if (send_element_error_to_client ("get_users", element_name))
          {
            error_send_to_client (error);
            return;
          }
        set_client_state (CLIENT_AUTHENTIC);
        g_set_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ELEMENT,
                     "Error");
      }
      break;

    case CLIENT_HELP:
      {
        if (send_element_error_to_client ("help", element_name))
          {
            error_send_to_client (error);
            return;
          }
        set_client_state (CLIENT_AUTHENTIC);
        g_set_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ELEMENT,
                     "Error");
      }
      break;

    case CLIENT_MODIFY_SETTINGS:
      if (strcasecmp ("SETTING", element_name) == 0)
        set_client_state (CLIENT_MODIFY_SETTINGS_SETTING);
      else
        {
          if (send_element_error_to_client ("modify_settings", element_name))
            {
              error_send_to_client (error);
              return;
            }
          set_client_state (CLIENT_AUTHENTIC);
          g_set_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ELEMENT,
                       "Error");
        }
      break;

    case CLIENT_MODIFY_SETTINGS_SETTING:
      if (strcasecmp ("NAME", element_name) == 0)
        set_client_state (CLIENT_MODIFY_SETTINGS_SETTING_NAME);
      else if (strcasecmp ("VALUE", element_name) == 0)
        set_client_state (CLIENT_MODIFY_SETTINGS_SETTING_VALUE);
      else
        {
          if (send_element_error_to_client ("modify_settings", element_name))
            {
              error_send_to_client (error);
              return;
            }
          set_client_state (CLIENT_AUTHENTIC);
          g_set_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ELEMENT,
                       "Error");
        }
      break;


      /** @internal The modify_auth command.
       * @code
       * <modify_auth>
       *   <group name="groupname">
       *   <auth_conf_setting key="groupname-key1" value="Value1"/>
       *   <auth_conf_setting key="groupname-key2" value="Value2"/>
       *  </group>
       * </modify_auth>
       * @endcode:
       *
       * Corresponding states:
       * CLIENT_MODIFY_AUTH
       *   CLIENT_MODIFY_AUTH_GROUP
       */
      /** @todo the MODIFY_AUTH states could send more specific error messages
         (especially syntax errors instead of "bogus element" errors). */
    case CLIENT_MODIFY_AUTH:
      {
        if (strcasecmp ("GROUP", element_name) == 0)
          {
            copy_attribute_value (attribute_names, attribute_values, "name",
                                  &(modify_auth_data->curr_group_name));
            set_client_state (CLIENT_MODIFY_AUTH_GROUP);
          }
        else
          {
            modify_auth_data_reset (modify_auth_data);
            /* Not a "group" element. */
            if (send_element_error_to_client ("modify_auth", element_name))
              {
                error_send_to_client (error);
                return;
              }
            set_client_state (CLIENT_AUTHENTIC);
            g_set_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ELEMENT,
                         "Error");
          }

        break;
      }


    case CLIENT_MODIFY_AUTH_GROUP:
      {
        if (strcasecmp ("AUTH_CONF_SETTING", element_name) == 0)
          {
            set_client_state (CLIENT_MODIFY_AUTH_GROUP_AUTHCONFSETTING);
            auth_conf_setting_t *setting =
              auth_conf_setting_from_xml (element_name,
                                          attribute_names,
                                          attribute_values);
            modify_auth_data->curr_group_settings =
              g_slist_prepend (modify_auth_data->curr_group_settings, setting);
          }
        else
          {
            /* Group without settings or without proper setting
               (auth_conf_setting) element, handle as error. */
            modify_auth_data_reset (modify_auth_data);
            if (send_element_error_to_client ("group", element_name))
              {
                error_send_to_client (error);
                return;
              }
            set_client_state (CLIENT_AUTHENTIC);
            g_set_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ELEMENT,
                         "Error");
          }

        break;
      }

    case CLIENT_MODIFY_AUTH_GROUP_AUTHCONFSETTING:
      {
        /* AUTH_CONF_SETTING should not have any children. */
        if (send_element_error_to_client ("auth_conf_setting", element_name))
          {
            error_send_to_client (error);
            return;
          }
        modify_auth_data_reset (modify_auth_data);
        set_client_state (CLIENT_AUTHENTIC);
        g_set_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ELEMENT,
                     "Error");
        break;
      }


    case CLIENT_MODIFY_USER:
      if (strcasecmp ("HOSTS", element_name) == 0)
        {
          const gchar *attribute;
          if (find_attribute
              (attribute_names, attribute_values, "allow", &attribute))
            modify_user_data->hosts_allow = strcmp (attribute, "0");
          else
            modify_user_data->hosts_allow = 1;
          /* Init, so that openvas_admin_modify_user clears hosts if this
           * entity is empty. */
          openvas_append_string (&modify_user_data->hosts, "");
          set_client_state (CLIENT_MODIFY_USER_HOSTS);
        }
      else if (strcasecmp ("NAME", element_name) == 0)
        set_client_state (CLIENT_MODIFY_USER_NAME);
      else if (strcasecmp ("PASSWORD", element_name) == 0)
        {
          const gchar *attribute;
          if (find_attribute
              (attribute_names, attribute_values, "modify", &attribute))
            modify_user_data->modify_password = strcmp (attribute, "0");
          else
            modify_user_data->modify_password = 1;
          set_client_state (CLIENT_MODIFY_USER_PASSWORD);
        }
      else if (strcasecmp ("ROLE", element_name) == 0)
        {
          /* Init, so that openvas_admin_modify_user gets an empty string
           * if the entity is empty. */
          openvas_append_string (&modify_user_data->role, "");
          set_client_state (CLIENT_MODIFY_USER_ROLE);
        }
      else if (strcasecmp ("SOURCES", element_name) == 0)
        {
          modify_user_data->sources = make_array ();
          set_client_state (CLIENT_MODIFY_USER_SOURCES);
        }
      else
        {
          if (send_element_error_to_client ("modify_user", element_name))
            {
              error_send_to_client (error);
              return;
            }
          set_client_state (CLIENT_AUTHENTIC);
          g_set_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ELEMENT,
                       "Error");
        }
      break;

    case CLIENT_MODIFY_USER_SOURCES:
      if (strcasecmp ("SOURCE", element_name) == 0)
       {
         set_client_state (CLIENT_MODIFY_USER_SOURCES_SOURCE);
       }
      else
        {
          if (send_element_error_to_client ("modify_user_sources", element_name))
            {
              error_send_to_client (error);
              return;
            }
          set_client_state (CLIENT_AUTHENTIC);
          g_set_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ELEMENT,
                       "Error");
        }
      break;

    default:
      assert (0);
      // FIX respond fail to client
      g_set_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
                   "Administrator programming error.");
      break;
    }

  return;
}

/**
 * @brief Produce an stripped-down xml representation of a keyfile.
 *
 * For
 * @code
 * [first group]
 * # this comment will be lost
 * weather=good
 * example=bad
 * [empty group]
 * @endcode
 * the output will look like
 * @code
 * <group name="first group">
 * <auth_conf_setting key="weather" value="good"/>
 * <auth_conf_setting key="example" value="bad"/>
 * </group>
 * <group name="empty group">
 * </group>
 * @endcode
 * .
 *
 * @param[in]  filename  Keyfile to read.
 *
 * @return NULL in case of any error, otherwise xml representation of keyfile
 *         (free with g_free).
 */
static gchar *
keyfile_to_auth_conf_settings_xml (const gchar * filename)
{
  GKeyFile *key_file = g_key_file_new ();
  GString *response = NULL;
  gchar **groups = NULL;
  gchar **group = NULL;

  if (g_key_file_load_from_file (key_file, filename, G_KEY_FILE_NONE, NULL) ==
      FALSE)
    {
      g_key_file_free (key_file);
      // Alert
      return NULL;
    }

  response = g_string_new ("");

  groups = g_key_file_get_groups (key_file, NULL);

  group = groups;
  while (*group != NULL)
    {
      gchar **keys = g_key_file_get_keys (key_file, *group, NULL, NULL);
      gchar **key = keys;
      g_string_append_printf (response, "<group name=\"%s\">\n", *group);
      while (*key != NULL)
        {
          gchar *value = g_key_file_get_value (key_file, *group, *key, NULL);
          if (value)
            g_string_append_printf (response,
                                    "<auth_conf_setting key=\"%s\" value=\"%s\"/>",
                                    *key, value);
          g_free (value);
          key++;
        }
      g_strfreev (keys);
      g_string_append (response, "</group>");
      group++;
    }

  g_strfreev (groups);
  g_key_file_free (key_file);

  return g_string_free (response, FALSE);
}

/**
 * @brief Handle the end of an OAP XML element.
 *
 * React to the end of an XML element according to the current value
 * of \ref client_state, usually adjusting \ref client_state to indicate
 * the change (with \ref set_client_state).  Call \ref send_to_client to queue
 * any responses for the client.  Call the task utilities to adjust the
 * tasks (for example \ref start_task, \ref stop_task, \ref set_task_parameter,
 * \ref delete_task and \ref find_task).
 *
 * Set error parameter on encountering an error.
 *
 * @param[in]  context           Parser context.
 * @param[in]  element_name      XML element name.
 * @param[in]  user_data         Dummy parameter.
 * @param[in]  error             Error parameter.
 */
static void
oap_xml_handle_end_element ( /*@unused@ */ GMarkupParseContext * context,
                            const gchar * element_name,
                            /*@unused@ */ gpointer user_data,
                            GError ** error)
{
  tracef ("   XML    end: %s\n", element_name);
  switch (client_state)
    {
    case CLIENT_TOP:
      assert (0);
      break;

    case CLIENT_AUTHENTICATE:
      switch (authenticate (&current_credentials))
        {
        case 0:
          /* Any config backend initialisation goes here. */
          SEND_TO_CLIENT_OR_FAIL (XML_OK ("authenticate"));
          set_client_state (CLIENT_AUTHENTIC);
          break;
        case 1:
          free_credentials (&current_credentials);
          SEND_TO_CLIENT_OR_FAIL (XML_ERROR_AUTH_FAILED ("authenticate"));
          set_client_state (CLIENT_TOP);
          break;
        case -1:
        default:
          free_credentials (&current_credentials);
          SEND_TO_CLIENT_OR_FAIL (XML_INTERNAL_ERROR ("authenticate"));
          set_client_state (CLIENT_TOP);
          break;
        }
      break;

    case CLIENT_AUTHENTIC:
    case CLIENT_COMMANDS:
    case CLIENT_AUTHENTIC_COMMANDS:
      assert (strcasecmp ("COMMANDS", element_name) == 0);
      SENDF_TO_CLIENT_OR_FAIL ("</commands_response>");
      break;

    case CLIENT_CREATE_USER:
      {
        gchar *errdesc;

        assert (strcasecmp ("CREATE_USER", element_name) == 0);
        assert (users_dir);

        if (create_user_data->name == NULL
            || strlen (create_user_data->name) == 0)
          SEND_TO_CLIENT_OR_FAIL (XML_ERROR_SYNTAX
                                  ("create_user",
                                   "CREATE_USER requires a name"));
        else
          switch (openvas_admin_add_user
                  (create_user_data->name,
                   create_user_data->password ? create_user_data->password : "",
                   create_user_data->role ? create_user_data->role : "User",
                   create_user_data->hosts, create_user_data->hosts_allow,
                   users_dir, create_user_data->sources,
                   &errdesc))
            {
            case 0:
              SEND_TO_CLIENT_OR_FAIL (XML_OK_CREATED ("create_user"));
              g_log ("event user", G_LOG_LEVEL_MESSAGE,
                     "User %s with role %s has been created",
                     create_user_data->name, create_user_data->role);
              break;
            case -2:
              SEND_TO_CLIENT_OR_FAIL (XML_ERROR_SYNTAX
                                      ("create_user", "User already exists"));
              break;
            case -1:
              if (errdesc)
                {
                  char *buf = make_xml_error_syntax ("create_user", errdesc);
                  SEND_TO_CLIENT_OR_FAIL (buf);
                  g_free (buf);
                  break;
                }
              /* Fall through.  */
            default:
              SEND_TO_CLIENT_OR_FAIL (XML_INTERNAL_ERROR ("create_user"));
              break;
            }
        create_user_data_reset (create_user_data);
        set_client_state (CLIENT_AUTHENTIC);
        g_free (errdesc);
        break;
      }
    case CLIENT_CREATE_USER_HOSTS:
      assert (strcasecmp ("HOSTS", element_name) == 0);
      set_client_state (CLIENT_CREATE_USER);
      break;
    case CLIENT_CREATE_USER_NAME:
      assert (strcasecmp ("NAME", element_name) == 0);
      set_client_state (CLIENT_CREATE_USER);
      break;
    case CLIENT_CREATE_USER_PASSWORD:
      assert (strcasecmp ("PASSWORD", element_name) == 0);
      set_client_state (CLIENT_CREATE_USER);
      break;
    case CLIENT_CREATE_USER_ROLE:
      assert (strcasecmp ("ROLE", element_name) == 0);
      set_client_state (CLIENT_CREATE_USER);
      break;
    case CLIENT_CREATE_USER_SOURCES:
      assert (strcasecmp ("SOURCES", element_name) == 0);
      array_terminate (create_user_data->sources);
      set_client_state (CLIENT_CREATE_USER);
      break;
    case CLIENT_CREATE_USER_SOURCES_SOURCE:
      assert (strcasecmp ("SOURCE", element_name) == 0);
      if (create_user_data->current_source)
        array_add (create_user_data->sources,
                   g_strdup (create_user_data->current_source));
      g_free (create_user_data->current_source);
      create_user_data->current_source = NULL;
      set_client_state (CLIENT_CREATE_USER_SOURCES);
      break;

    case CLIENT_CREDENTIALS:
      assert (strcasecmp ("CREDENTIALS", element_name) == 0);
      set_client_state (CLIENT_AUTHENTICATE);
      break;

    case CLIENT_CREDENTIALS_USERNAME:
      assert (strcasecmp ("USERNAME", element_name) == 0);
      set_client_state (CLIENT_CREDENTIALS);
      break;

    case CLIENT_CREDENTIALS_PASSWORD:
      assert (strcasecmp ("PASSWORD", element_name) == 0);
      set_client_state (CLIENT_CREDENTIALS);
      break;

    case CLIENT_DELETE_USER:
      if (delete_user_data->name)
        {
          assert (users_dir);
          if (strcmp (current_credentials.username, delete_user_data->name) ==
              0)
            SEND_TO_CLIENT_OR_FAIL (XML_ERROR_SYNTAX
                                    ("delete_user",
                                     "Attempt to delete current user"));
          else
            switch (openvas_admin_remove_user
                    (delete_user_data->name, users_dir))
              {
              case 0:
                SEND_TO_CLIENT_OR_FAIL (XML_OK ("delete_user"));
                g_log ("event user", G_LOG_LEVEL_MESSAGE,
                       "User %s has been removed", delete_user_data->name);
                break;
              case -2:
                SEND_TO_CLIENT_OR_FAIL (XML_ERROR_SYNTAX
                                        ("delete_user", "Failed to find user"));
                break;
              default:
              case -1:
                SEND_TO_CLIENT_OR_FAIL (XML_INTERNAL_ERROR ("delete_user"));
                break;
              }
        }
      else
        SEND_TO_CLIENT_OR_FAIL (XML_ERROR_SYNTAX
                                ("delete_user",
                                 "DELETE_USER requires a name attribute"));
      delete_user_data_reset (delete_user_data);
      set_client_state (CLIENT_AUTHENTIC);
      break;

    case CLIENT_HELP:
      if (help_data->format == NULL
          || (strcmp (help_data->format, "text") == 0))
        {
          SEND_TO_CLIENT_OR_FAIL ("<help_response"
                                  " status=\"" STATUS_OK "\""
                                  " status_text=\"" STATUS_OK_TEXT "\">");
          if (enable_modify_settings)
            SENDF_TO_CLIENT_OR_FAIL (help_text, help_modify_settings);
          else
            SENDF_TO_CLIENT_OR_FAIL (help_text, "");
          SEND_TO_CLIENT_OR_FAIL ("</help_response>");
        }
      else
        {
          gchar *extension, *content_type, *output;
          gsize output_len;

          switch (openvas_admin_schema (help_data->format,
                                        &output,
                                        &output_len,
                                        &extension,
                                        &content_type))
            {
              case 0:
                break;
              case 1:
                assert (help_data->format);
                if (send_find_error_to_client ("help",
                                               "schema_format",
                                               help_data->format))
                  {
                    error_send_to_client (error);
                    return;
                  }
                help_data_reset (help_data);
                set_client_state (CLIENT_AUTHENTIC);
                return;
                break;
              default:
                SEND_TO_CLIENT_OR_FAIL (XML_INTERNAL_ERROR ("help"));
                help_data_reset (help_data);
                set_client_state (CLIENT_AUTHENTIC);
                return;
                break;
            }

          SENDF_TO_CLIENT_OR_FAIL ("<help_response"
                                   " status=\"" STATUS_OK "\""
                                   " status_text=\"" STATUS_OK_TEXT "\">"
                                   "<schema"
                                   " format=\"%s\""
                                   " extension=\"%s\""
                                   " content_type=\"%s\">",
                                   help_data->format
                                    ? help_data->format
                                    : "XML",
                                   extension,
                                   content_type);
          g_free (extension);
          g_free (content_type);

          if (output && strlen (output))
            {
              /* Encode and send the output. */

              if (help_data->format
                  && strcasecmp (help_data->format, "XML"))
                {
                  gchar *base64;

                  base64 = g_base64_encode ((guchar*) output, output_len);
                  if (send_to_client (base64))
                    {
                      g_free (output);
                      g_free (base64);
                      error_send_to_client (error);
                      return;
                    }
                  g_free (base64);
                }
              else
                {
                  /* Special case the XML schema, bah. */
                  if (send_to_client (output))
                    {
                      g_free (output);
                      error_send_to_client (error);
                      return;
                    }
                }
            }
          g_free (output);
          SEND_TO_CLIENT_OR_FAIL ("</schema>"
                                  "</help_response>");
        }
      help_data_reset (help_data);
      set_client_state (CLIENT_AUTHENTIC);
      break;

    case CLIENT_DESCRIBE_FEED:
      {
        gchar *feed_description = NULL;
        gchar *feed_identification = NULL;
        gchar *feed_version = NULL;

        assert (current_credentials.username);

        if (openvas_get_sync_script_description (sync_script, &feed_description)
            && openvas_get_sync_script_identification (sync_script,
                                                       &feed_identification,
                                                       NVT_FEED)
            && openvas_get_sync_script_feed_version (sync_script,
                                                     &feed_version))
          {
            gchar *user, *timestamp;
            int syncing;
            gchar **ident = g_strsplit (feed_identification, "|", 6);
            gchar *selftest_result = NULL;

            syncing = openvas_current_sync (sync_script, &timestamp, &user);
            if (syncing < 0 || ident[0] == NULL || ident[1] == NULL
                || ident[2] == NULL || ident[3] == NULL)
              {
                g_strfreev (ident);
                SEND_TO_CLIENT_OR_FAIL (XML_INTERNAL_ERROR ("describe_feed"));
              }
            else
              {

                SENDF_TO_CLIENT_OR_FAIL ("<describe_feed_response" " status=\""
                                         STATUS_OK "\"" " status_text=\""
                                         STATUS_OK_TEXT "\">" "<feed>"
                                         "<name>%s</name>"
                                         "<version>%s</version>"
                                         "<description>%s</description>",
                                         ident[3], feed_version,
                                         feed_description);
                g_strfreev (ident);
                if (openvas_sync_script_perform_selftest
                    (sync_script, &selftest_result) == FALSE)
                  {
                    SENDF_TO_CLIENT_OR_FAIL ("<sync_not_available>"
                                             "<error>%s</error>"
                                             "</sync_not_available>",
                                             selftest_result ? selftest_result :
                                             "");
                    g_free (selftest_result);
                  }

                if (syncing > 0)
                  {
                    SENDF_TO_CLIENT_OR_FAIL ("<currently_syncing>"
                                             "<timestamp>%s</timestamp>"
                                             "<user>%s</user>"
                                             "</currently_syncing>",
                                             timestamp ? timestamp : "",
                                             user ? user : "");
                    g_free (timestamp);
                    g_free (user);
                  }
                SEND_TO_CLIENT_OR_FAIL ("</feed>" "</describe_feed_response>");
              }

            g_free (feed_identification);
            g_free (feed_version);
          }
        else
          {
            SEND_TO_CLIENT_OR_FAIL ("<describe_feed_response" " status=\""
                                    STATUS_OK "\"" " status_text=\""
                                    STATUS_OK_TEXT "\">");
            SEND_TO_CLIENT_OR_FAIL ("<feed>");
            SEND_TO_CLIENT_OR_FAIL ("<name></name>");
            SEND_TO_CLIENT_OR_FAIL ("<description></description>");
            SEND_TO_CLIENT_OR_FAIL ("</feed>");
            SEND_TO_CLIENT_OR_FAIL ("</describe_feed_response>");
          }
        g_free (feed_description);
        set_client_state (CLIENT_AUTHENTIC);
        break;
      }

    case CLIENT_DESCRIBE_SCAP:
      {
        gchar *scap_description = NULL;
        gchar *scap_identification = NULL;
        gchar *scap_version = NULL;

        assert (current_credentials.username);

        if (openvas_get_sync_script_description (scap_script, &scap_description)
            && openvas_get_sync_script_identification (scap_script,
                                                       &scap_identification,
                                                       SCAP_FEED)
            && openvas_get_sync_script_feed_version (scap_script,
                                                     &scap_version))
          {
            gchar *user, *timestamp;
            int syncing;
            gchar **ident = g_strsplit (scap_identification, "|", 6);
            gchar *selftest_result = NULL;

            syncing = openvas_current_sync (scap_script, &timestamp, &user);
            if (syncing < 0 || ident[0] == NULL || ident[1] == NULL
                || ident[2] == NULL || ident[3] == NULL)
              {
                g_strfreev (ident);
                SEND_TO_CLIENT_OR_FAIL (XML_INTERNAL_ERROR ("describe_scap"));
              }
            else
              {

                SENDF_TO_CLIENT_OR_FAIL ("<describe_scap_response" " status=\""
                                         STATUS_OK "\"" " status_text=\""
                                         STATUS_OK_TEXT "\">" "<scap>"
                                         "<name>%s</name>"
                                         "<version>%s</version>"
                                         "<description>%s</description>",
                                         ident[3], scap_version,
                                         scap_description);
                g_strfreev (ident);
                if (openvas_sync_script_perform_selftest
                    (scap_script, &selftest_result) == FALSE)
                  {
                    SENDF_TO_CLIENT_OR_FAIL ("<sync_not_available>"
                                             "<error>%s</error>"
                                             "</sync_not_available>",
                                             selftest_result ? selftest_result :
                                             "");
                    g_free (selftest_result);
                  }

                if (syncing > 0)
                  {
                    SENDF_TO_CLIENT_OR_FAIL ("<currently_syncing>"
                                             "<timestamp>%s</timestamp>"
                                             "<user>%s</user>"
                                             "</currently_syncing>",
                                             timestamp ? timestamp : "",
                                             user ? user : "");
                    g_free (timestamp);
                    g_free (user);
                  }
                SEND_TO_CLIENT_OR_FAIL ("</scap>" "</describe_scap_response>");
              }

            g_free (scap_identification);
            g_free (scap_version);
          }
        else
          {
            SEND_TO_CLIENT_OR_FAIL ("<describe_scap_response" " status=\""
                                    STATUS_OK "\"" " status_text=\""
                                    STATUS_OK_TEXT "\">");
            SEND_TO_CLIENT_OR_FAIL ("<scap>");
            SEND_TO_CLIENT_OR_FAIL ("<name></name>");
            SEND_TO_CLIENT_OR_FAIL ("<description></description>");
            SEND_TO_CLIENT_OR_FAIL ("</scap>");
            SEND_TO_CLIENT_OR_FAIL ("</describe_scap_response>");
          }
        g_free (scap_description);
        set_client_state (CLIENT_AUTHENTIC);
        break;
      }

    case CLIENT_DESCRIBE_CERT:
      {
        gchar *cert_description = NULL;
        gchar *cert_identification = NULL;
        gchar *cert_version = NULL;

        assert (current_credentials.username);

        if (openvas_get_sync_script_description (cert_script, &cert_description)
            && openvas_get_sync_script_identification (cert_script,
                                                       &cert_identification,
                                                       CERT_FEED)
            && openvas_get_sync_script_feed_version (cert_script,
                                                     &cert_version))
          {
            gchar *user, *timestamp;
            int syncing;
            gchar **ident = g_strsplit (cert_identification, "|", 6);
            gchar *selftest_result = NULL;

            syncing = openvas_current_sync (cert_script, &timestamp, &user);
            if (syncing < 0 || ident[0] == NULL || ident[1] == NULL
                || ident[2] == NULL || ident[3] == NULL)
              {
                g_strfreev (ident);
                SEND_TO_CLIENT_OR_FAIL (XML_INTERNAL_ERROR ("describe_cert"));
              }
            else
              {

                SENDF_TO_CLIENT_OR_FAIL ("<describe_cert_response" " status=\""
                                         STATUS_OK "\"" " status_text=\""
                                         STATUS_OK_TEXT "\">" "<cert>"
                                         "<name>%s</name>"
                                         "<version>%s</version>"
                                         "<description>%s</description>",
                                         ident[3], cert_version,
                                         cert_description);
                g_strfreev (ident);
                if (openvas_sync_script_perform_selftest
                    (scap_script, &selftest_result) == FALSE)
                  {
                    SENDF_TO_CLIENT_OR_FAIL ("<sync_not_available>"
                                             "<error>%s</error>"
                                             "</sync_not_available>",
                                             selftest_result ? selftest_result :
                                             "");
                    g_free (selftest_result);
                  }

                if (syncing > 0)
                  {
                    SENDF_TO_CLIENT_OR_FAIL ("<currently_syncing>"
                                             "<timestamp>%s</timestamp>"
                                             "<user>%s</user>"
                                             "</currently_syncing>",
                                             timestamp ? timestamp : "",
                                             user ? user : "");
                    g_free (timestamp);
                    g_free (user);
                  }
                SEND_TO_CLIENT_OR_FAIL ("</cert>" "</describe_cert_response>");
              }

            g_free (cert_identification);
            g_free (cert_version);
          }
        else
          {
            SEND_TO_CLIENT_OR_FAIL ("<describe_cert_response" " status=\""
                                    STATUS_OK "\"" " status_text=\""
                                    STATUS_OK_TEXT "\">");
            SEND_TO_CLIENT_OR_FAIL ("<cert>");
            SEND_TO_CLIENT_OR_FAIL ("<name></name>");
            SEND_TO_CLIENT_OR_FAIL ("<description></description>");
            SEND_TO_CLIENT_OR_FAIL ("</cert>");
            SEND_TO_CLIENT_OR_FAIL ("</describe_cert_response>");
          }
        g_free (cert_description);
        set_client_state (CLIENT_AUTHENTIC);
        break;
      }


    case CLIENT_DESCRIBE_AUTH:
      {
        assert (current_credentials.username);

        /* Get base 64 encoded content of the auth configuration file. */
        gchar *config_file = g_build_filename (OPENVAS_USERS_DIR,
                                               ".auth.conf",
                                               NULL);
        gchar *content = keyfile_to_auth_conf_settings_xml (config_file);
        if (content == NULL)
          {
            g_free (config_file);
            SEND_TO_CLIENT_OR_FAIL ("<describe_auth_response " " status=\""
                                    STATUS_ERROR_MISSING "\"" " status_text=\""
                                    STATUS_ERROR_MISSING_TEXT "\"/>");
            set_client_state (CLIENT_AUTHENTIC);
            break;
          }

        gchar *resp =
          g_strdup_printf ("<describe_auth_response " " status=\"" STATUS_OK
                           "\"" " status_text=\"" STATUS_OK_TEXT
                           "\">%s</describe_auth_response>",
                           content);
        g_free (content);
        g_free (config_file);

        SEND_TO_CLIENT_OR_FAIL (resp);
        g_free (resp);
        set_client_state (CLIENT_AUTHENTIC);
        break;
      }

    case CLIENT_GET_SETTINGS:
      {
        settings_iterator_t settings;

        if (init_settings_iterator (&settings, scanner_config_file, "Misc"))
          SENDF_TO_CLIENT_OR_FAIL (XML_INTERNAL_ERROR ("get_settings"));
        else
          {
            SENDF_TO_CLIENT_OR_FAIL ("<get_settings_response" " status=\""
                                     STATUS_OK "\"" " status_text=\""
                                     STATUS_OK_TEXT "\">" "<scanner_settings"
                                     " sourcefile=\"%s\"" " editable=\"%i\">",
                                     scanner_config_file,
                                     enable_modify_settings ? 1 : 0);

            while (settings_iterator_next (&settings))
              SENDF_TO_CLIENT_OR_FAIL ("<setting name=\"%s\">%s</setting>",
                                       settings_iterator_name (&settings),
                                       settings_iterator_value (&settings));

            SEND_TO_CLIENT_OR_FAIL ("</scanner_settings>"
                                    "</get_settings_response>");
            cleanup_settings_iterator (&settings);
          }
        set_client_state (CLIENT_AUTHENTIC);
        break;
      }

    case CLIENT_MODIFY_SETTINGS:
      {
        array_terminate (modify_settings_data->settings);
        switch (openvas_admin_modify_settings
                (scanner_config_file, "Misc", modify_settings_data->settings))
          {
          case 0:
            SEND_TO_CLIENT_OR_FAIL (XML_OK ("modify_settings"));
            break;
          default:
            assert (0);
          case -1:
            SEND_TO_CLIENT_OR_FAIL (XML_INTERNAL_ERROR ("modify_settings"));
            break;
          }
        modify_settings_data_reset (modify_settings_data);
        set_client_state (CLIENT_AUTHENTIC);
        break;
      }
    case CLIENT_MODIFY_SETTINGS_SETTING:
      assert (strcasecmp ("SETTING", element_name) == 0);

      array_add (modify_settings_data->settings,
                 setting_new (modify_settings_data->setting_name,
                              modify_settings_data->setting_value));

      modify_settings_data->setting_name = NULL;
      modify_settings_data->setting_value = NULL;
      set_client_state (CLIENT_MODIFY_SETTINGS);
      break;
    case CLIENT_MODIFY_SETTINGS_SETTING_NAME:
      assert (strcasecmp ("NAME", element_name) == 0);
      set_client_state (CLIENT_MODIFY_SETTINGS_SETTING);
      break;
    case CLIENT_MODIFY_SETTINGS_SETTING_VALUE:
      assert (strcasecmp ("VALUE", element_name) == 0);
      set_client_state (CLIENT_MODIFY_SETTINGS_SETTING);
      break;

    case CLIENT_GET_USERS:
      {
        GSList *users, *user;
        assert (strcasecmp ("GET_USERS", element_name) == 0);

          /** @todo Consider using user_t, find_user and user_iterator_t. */

        user = users =
          openvas_admin_list_users (users_dir, get_users_data->sort_order,
                                    get_users_data->name);
        if (get_users_data->name && users == NULL)
          {
            SEND_TO_CLIENT_OR_FAIL (XML_ERROR_SYNTAX
                                    ("get_users", "Failed to find user"));
            get_users_data_reset (get_users_data);
            set_client_state (CLIENT_AUTHENTIC);
            break;
          }

        SEND_TO_CLIENT_OR_FAIL ("<get_users_response" " status=\"" STATUS_OK
                                "\"" " status_text=\"" STATUS_OK_TEXT "\">");
        while (user)
          {
            gchar *hosts;
            gchar *sources;
            GSList *methods;
            int allow;
            if (openvas_admin_user_access
                (user->data, &hosts, &allow, users_dir))
              /* @todo Buffer all hosts before sending anything. */
              abort ();
            methods = openvas_auth_user_methods (user->data);
            sources = openvas_string_list_to_xml (methods, "sources", "source");
            SENDF_TO_CLIENT_OR_FAIL ("<user>" "<name>%s</name>"
                                     "<role>%s</role>"
                                     "<hosts allow=\"%i\">%s</hosts>",
                                     (gchar *) user->data,
                                     openvas_is_user_admin (user->data)
                                       ? "Admin"
                                       : (openvas_is_user_observer (user->data)
                                          ? "Observer"
                                          : "User"),
                                     allow,
                                     hosts ? hosts : "");
            SEND_TO_CLIENT_OR_FAIL (sources);
            SEND_TO_CLIENT_OR_FAIL ("</user>");
            g_free (hosts);
            g_free (user->data);
            g_free (sources);
            openvas_string_list_free (methods);
            user = g_slist_next (user);
          }
        g_slist_free (users);
        get_users_data_reset (get_users_data);
        SEND_TO_CLIENT_OR_FAIL ("</get_users_response>");
        set_client_state (CLIENT_AUTHENTIC);
        break;
      }

    case CLIENT_MODIFY_AUTH_GROUP_AUTHCONFSETTING:
      {
        set_client_state (CLIENT_MODIFY_AUTH_GROUP);
        break;
      }

    case CLIENT_MODIFY_AUTH_GROUP:
      {
        assert (strcasecmp ("GROUP", element_name) == 0);
        /* Group needs to have a name. */
        if (modify_auth_data->curr_group_name == NULL)
          {
            SEND_TO_CLIENT_OR_FAIL (XML_ERROR_SYNTAX
                                    ("group",
                                     "group requires a name attribute"));
            set_client_state (CLIENT_AUTHENTIC);
            modify_auth_data_reset (modify_auth_data);
            break;
          }

        /* Now, add this group with its settings. */
        auth_group_t *new_group = g_malloc0 (sizeof (auth_group_t));
        new_group->group_name = modify_auth_data->curr_group_name;
        if (modify_auth_data->curr_group_settings)
          new_group->settings = modify_auth_data->curr_group_settings;

        modify_auth_data->groups =
          g_slist_prepend (modify_auth_data->groups, new_group);
        /* Done with this group. */
        modify_auth_data->curr_group_name = NULL;
        modify_auth_data->curr_group_settings = NULL;

        /* More groups can come. */
        set_client_state (CLIENT_MODIFY_AUTH);
        break;
      }

    case CLIENT_MODIFY_AUTH:
      {
        assert (strcasecmp ("MODIFY_AUTH", element_name) == 0);
        assert (users_dir);

        GKeyFile *key_file = g_key_file_new ();

        /* Lets output the data for now. */
        GSList *item = modify_auth_data->groups;
        while (item)
          {
            auth_group_t *auth_group = (auth_group_t *) item->data;
            gchar *group = auth_group->group_name;
            GSList *setting = auth_group->settings;
            while (setting)
              {
                auth_conf_setting_t *kvp =
                  (auth_conf_setting_t *) setting->data;
                g_key_file_set_value (key_file, group, kvp->key, kvp->value);
                setting = g_slist_next (setting);
              }
            item = g_slist_next (item);
          }

          /** @todo Implement sighup in and send to openvas-manager in order
            *       for the changed config to take effect. */
        switch (openvas_auth_write_config (key_file))
          {
          case 0:
            SEND_TO_CLIENT_OR_FAIL (XML_OK ("modify_auth"));
            break;
          case 1:
            SEND_TO_CLIENT_OR_FAIL (XML_ERROR_SYNTAX
                                    ("modify_auth",
                                     "Valid authdn required"));
            break;
          default:
          case -1:
            SEND_TO_CLIENT_OR_FAIL (XML_INTERNAL_ERROR ("modify_auth"));
            break;
          }

        g_key_file_free (key_file);
        modify_auth_data_reset (modify_auth_data);
        set_client_state (CLIENT_AUTHENTIC);

        break;
      }

    case CLIENT_MODIFY_USER:
      {
        assert (strcasecmp ("MODIFY_USER", element_name) == 0);
        assert (users_dir);
        array_terminate (modify_user_data->sources);

        if (modify_user_data->name == NULL
            || strlen (modify_user_data->name) == 0)
          SEND_TO_CLIENT_OR_FAIL (XML_ERROR_SYNTAX
                                  ("modify_user",
                                   "MODIFY_USER requires a name"));
        else
          {
            gchar *errdesc = NULL;
            int was_admin = openvas_is_user_admin (modify_user_data->name);

            switch (openvas_admin_modify_user
                    (modify_user_data->name,
                     ((modify_user_data->modify_password
                       && modify_user_data->password) ? modify_user_data->
                      password
                      /* Leave the password as it is. */
                      : NULL), modify_user_data->role, modify_user_data->hosts,
                     modify_user_data->hosts_allow, users_dir,
                     modify_user_data->sources, &errdesc))
              {
              case 0:
                SEND_TO_CLIENT_OR_FAIL (XML_OK ("modify_user"));
                if (was_admin && (strcmp (modify_user_data->role, "User") == 0))
                  {
                    g_log ("event user", G_LOG_LEVEL_MESSAGE,
                           "Role of user %s has been changed from Admin to User",
                           modify_user_data->name);
                  }
                if (!was_admin
                    && (strcmp (modify_user_data->role, "Admin") == 0))
                  {
                    g_log ("event user", G_LOG_LEVEL_MESSAGE,
                           "Role of user %s has been changed from User to Admin",
                           modify_user_data->name);
                  }
                break;
              case -2:
                SEND_TO_CLIENT_OR_FAIL (XML_ERROR_SYNTAX
                                        ("modify_user", "Unknown role"));
                break;
              case -3:
                SEND_TO_CLIENT_OR_FAIL (XML_ERROR_SYNTAX
                                        ("modify_user", "User already exists"));
                break;
              case -1:
                if (errdesc)
                  {
                    char *buf = make_xml_error_syntax ("modify_user", errdesc);
                    SEND_TO_CLIENT_OR_FAIL (buf);
                    g_free (buf);
                    break;
                  }
              /* Fall through.  */
              default:
                SEND_TO_CLIENT_OR_FAIL (XML_INTERNAL_ERROR ("modify_user"));
                break;
              }
            g_free (errdesc);
          }
        modify_user_data_reset (modify_user_data);
        set_client_state (CLIENT_AUTHENTIC);
        break;
      }
    case CLIENT_MODIFY_USER_HOSTS:
      assert (strcasecmp ("HOSTS", element_name) == 0);
      set_client_state (CLIENT_MODIFY_USER);
      break;
    case CLIENT_MODIFY_USER_NAME:
      assert (strcasecmp ("NAME", element_name) == 0);
      set_client_state (CLIENT_MODIFY_USER);
      break;
    case CLIENT_MODIFY_USER_PASSWORD:
      assert (strcasecmp ("PASSWORD", element_name) == 0);
      set_client_state (CLIENT_MODIFY_USER);
      break;
    case CLIENT_MODIFY_USER_ROLE:
      assert (strcasecmp ("ROLE", element_name) == 0);
      set_client_state (CLIENT_MODIFY_USER);
      break;
    case CLIENT_MODIFY_USER_SOURCES:
      assert (strcasecmp ("SOURCES", element_name) == 0);
      array_terminate (modify_user_data->sources);
      set_client_state (CLIENT_MODIFY_USER);
      break;
    case CLIENT_MODIFY_USER_SOURCES_SOURCE:
      assert (strcasecmp ("SOURCE", element_name) == 0);
      array_add (modify_user_data->sources,
                 g_strdup (modify_user_data->current_source));
      g_free (modify_user_data->current_source);
      modify_user_data->current_source = NULL;
      set_client_state (CLIENT_MODIFY_USER_SOURCES);
      break;

    case CLIENT_SYNC_FEED:
      assert (current_credentials.username);
      if (forked == 2)
        /* Prevent the forked child from forking again, as then both
         * forked children would be using the same server session. */
        abort ();               // FIX respond with error or something
      else
        switch (openvas_sync_feed (sync_script, current_credentials.username,
                                   NVT_FEED))
          {
          case 0:
            SEND_TO_CLIENT_OR_FAIL (XML_OK_REQUESTED ("sync_feed"));
            forked = 1;
            break;
          case 1:
            SEND_TO_CLIENT_OR_FAIL (XML_ERROR_BUSY ("sync_feed"));
            break;
          case 2:
            /* Forked sync process: success. */
            current_error = 2;
            g_set_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_INVALID_CONTENT,
                         "Dummy error for current_error");
            break;
          case 11:
            /* Forked sync process: success busy. */
            current_error = 2;
            g_set_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_INVALID_CONTENT,
                         "Dummy error for current_error");
            break;
          case -10:
            /* Forked sync process: error. */
            current_error = -10;
            g_set_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_INVALID_CONTENT,
                         "Dummy error for current_error");
            break;
          default:
            assert (0);
          case -1:
            SEND_TO_CLIENT_OR_FAIL (XML_INTERNAL_ERROR ("sync_feed"));
            break;
          }
      set_client_state (CLIENT_AUTHENTIC);
      break;

    case CLIENT_SYNC_SCAP:
      assert (current_credentials.username);
      if (forked == 2)
        /* Prevent the forked child from forking again, as then both
         * forked children would be using the same server session. */
        abort ();               // FIX respond with error or something
      else
        switch (openvas_sync_feed (scap_script, current_credentials.username, SCAP_FEED))
          {
          case 0:
            SEND_TO_CLIENT_OR_FAIL (XML_OK_REQUESTED ("sync_scap"));
            forked = 1;
            break;
          case 1:
            SEND_TO_CLIENT_OR_FAIL (XML_ERROR_BUSY ("sync_scap"));
            break;
          case 2:
            /* Forked sync process: success. */
            current_error = 2;
            g_set_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_INVALID_CONTENT,
                         "Dummy error for current_error");
            break;
          case 11:
            /* Forked sync process: success busy. */
            current_error = 2;
            g_set_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_INVALID_CONTENT,
                         "Dummy error for current_error");
            break;
          case -10:
            /* Forked sync process: error. */
            current_error = -10;
            g_set_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_INVALID_CONTENT,
                         "Dummy error for current_error");
            break;
          default:
            assert (0);
          case -1:
            SEND_TO_CLIENT_OR_FAIL (XML_INTERNAL_ERROR ("sync_scap"));
            break;
          }
      set_client_state (CLIENT_AUTHENTIC);
      break;

    case CLIENT_SYNC_CERT:
      assert (current_credentials.username);
      if (forked == 2)
        /* Prevent the forked child from forking again, as then both
         * forked children would be using the same server session. */
        abort ();               // FIX respond with error or something
      else
        switch (openvas_sync_feed (cert_script, current_credentials.username, CERT_FEED))
          {
          case 0:
            SEND_TO_CLIENT_OR_FAIL (XML_OK_REQUESTED ("sync_cert"));
            forked = 1;
            break;
          case 1:
            SEND_TO_CLIENT_OR_FAIL (XML_ERROR_BUSY ("sync_cert"));
            break;
          case 2:
            /* Forked sync process: success. */
            current_error = 2;
            g_set_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_INVALID_CONTENT,
                         "Dummy error for current_error");
            break;
          case 11:
            /* Forked sync process: success busy. */
            current_error = 2;
            g_set_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_INVALID_CONTENT,
                         "Dummy error for current_error");
            break;
          case -10:
            /* Forked sync process: error. */
            current_error = -10;
            g_set_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_INVALID_CONTENT,
                         "Dummy error for current_error");
            break;
          default:
            assert (0);
          case -1:
            SEND_TO_CLIENT_OR_FAIL (XML_INTERNAL_ERROR ("sync_cert"));
            break;
          }
      set_client_state (CLIENT_AUTHENTIC);
      break;

    case CLIENT_VERSION:
    case CLIENT_VERSION_AUTHENTIC:
      SEND_TO_CLIENT_OR_FAIL ("<get_version_response" " status=\"" STATUS_OK
                              "\"" " status_text=\"" STATUS_OK_TEXT "\">"
                              "<version preferred=\"yes\">1.1</version>"
                              "</get_version_response>");
      if (client_state == CLIENT_VERSION_AUTHENTIC)
        set_client_state (CLIENT_AUTHENTIC);
      else
        set_client_state (CLIENT_TOP);
      break;

    default:
      assert (0);
      break;
    }
}

/**
 * @brief Handle the addition of text to an OAP XML element.
 *
 * React to the addition of text to the value of an XML element.
 * React according to the current value of \ref client_state,
 * usually appending the text to some part of the data
 * with functions like \ref openvas_append_text .
 *
 * @param[in]  context           Parser context.
 * @param[in]  text              The text.
 * @param[in]  text_len          Length of the text.
 * @param[in]  user_data         Dummy parameter.
 * @param[in]  error             Error parameter.
 */
static void
oap_xml_handle_text ( /*@unused@ */ GMarkupParseContext * context,
                     const gchar * text, gsize text_len,
                     /*@unused@ */ gpointer user_data,
                     /*@unused@ */ GError ** error)
{
  if (text_len == 0)
    return;
  tracef ("   XML   text: %s\n", text);
  switch (client_state)
    {
    case CLIENT_CREATE_USER_HOSTS:
      openvas_append_text (&create_user_data->hosts, text, text_len);
      break;
    case CLIENT_CREATE_USER_NAME:
      openvas_append_text (&create_user_data->name, text, text_len);
      break;
    case CLIENT_CREATE_USER_PASSWORD:
      openvas_append_text (&create_user_data->password, text, text_len);
      break;
    case CLIENT_CREATE_USER_ROLE:
      openvas_append_text (&create_user_data->role, text, text_len);
      break;
    case CLIENT_CREATE_USER_SOURCES_SOURCE:
      openvas_append_text (&create_user_data->current_source, text, text_len);
      break;

    case CLIENT_CREDENTIALS_USERNAME:
      append_to_credentials_username (&current_credentials, text, text_len);
      break;
    case CLIENT_CREDENTIALS_PASSWORD:
      append_to_credentials_password (&current_credentials, text, text_len);
      break;

    case CLIENT_MODIFY_SETTINGS_SETTING_NAME:
      openvas_append_text (&modify_settings_data->setting_name, text, text_len);
      break;
    case CLIENT_MODIFY_SETTINGS_SETTING_VALUE:
      openvas_append_text (&modify_settings_data->setting_value, text,
                           text_len);
      break;

    case CLIENT_MODIFY_USER_HOSTS:
      openvas_append_text (&modify_user_data->hosts, text, text_len);
      break;
    case CLIENT_MODIFY_USER_NAME:
      openvas_append_text (&modify_user_data->name, text, text_len);
      break;
    case CLIENT_MODIFY_USER_PASSWORD:
      openvas_append_text (&modify_user_data->password, text, text_len);
      break;
    case CLIENT_MODIFY_USER_ROLE:
      openvas_append_text (&modify_user_data->role, text, text_len);
      break;
    case CLIENT_MODIFY_USER_SOURCES_SOURCE:
      openvas_append_text (&modify_user_data->current_source, text, text_len);
      break;

    default:
      /* Just pass over the text. */
      break;
    }
}

/**
 * @brief Handle an OAP XML parsing error.
 *
 * Simply leave the error for the caller of the parser to handle.
 *
 * @param[in]  context           Parser context.
 * @param[in]  error             The error.
 * @param[in]  user_data         Dummy parameter.
 */
static void
oap_xml_handle_error ( /*@unused@ */ GMarkupParseContext * context,
                      GError * error,
                      /*@unused@ */ gpointer user_data)
{
  tracef ("   XML ERROR %s\n", error->message);
}


/* OAP input processor. */

// FIX probably should pass to process_oap_client_input
extern char from_client[];
extern buffer_size_t from_client_start;
extern buffer_size_t from_client_end;

/**
 * @brief Initialise OAP library.
 *
 * @param[in]  log_config              Logging configuration list.
 * @param[in]  users_directory         Directory containing user info.
 * @param[in]  nvt_sync_script         The script to use for nvt feed
 *                                     synchronization.
 * @param[in]  scap_sync_script        The script to use for scap feed
 *                                     synchronization.
 * @param[in]  configuration_file      Scanner configuration file.
 * @param[in]  modify_settings         If true enable OAP MODIFY_SETTINGS.
 *
 * @return 0 success, -1 error.
 */
int
init_oap (GSList * log_config, const gchar * users_directory,
          const gchar * nvt_sync_script, const gchar * scap_sync_script,
          const gchar * cert_sync_script, const gchar * configuration_file,
          gboolean modify_settings)
{
  if (users_directory == NULL)
    return -1;
  if (nvt_sync_script == NULL)
    return -1;
  if (scap_sync_script == NULL)
    return -1;
  if (cert_sync_script == NULL)
    return -1;
  if (configuration_file == NULL)
    return -1;
  g_log_set_handler (G_LOG_DOMAIN, ALL_LOG_LEVELS, (GLogFunc) openvas_log_func,
                     log_config);
  users_dir = users_directory;
  sync_script = nvt_sync_script;
  scap_script = scap_sync_script;
  cert_script = cert_sync_script;
  scanner_config_file = configuration_file;
  current_credentials.username = NULL;
  current_credentials.password = NULL;
  command_data_init (&command_data);
  enable_modify_settings = modify_settings;
  return 0;
}

/**
 * @brief Initialise OAP library data for a process.
 *
 * This should run once per process, before the first call to \ref
 * process_oap_client_input.
 */
void
init_oap_process ()
{
  forked = 0;
  /* Create the XML parser. */
  xml_parser.start_element = oap_xml_handle_start_element;
  xml_parser.end_element = oap_xml_handle_end_element;
  xml_parser.text = oap_xml_handle_text;
  xml_parser.passthrough = NULL;
  xml_parser.error = oap_xml_handle_error;
  if (xml_context)
    g_free (xml_context);
  xml_context = g_markup_parse_context_new (&xml_parser, 0, NULL, NULL);
}

/**
 * @brief Process any XML available in \ref from_client.
 *
 * \if STATIC
 *
 * Call the XML parser and let the callback functions do the work
 * (\ref oap_xml_handle_start_element, \ref oap_xml_handle_end_element,
 * \ref oap_xml_handle_text and \ref oap_xml_handle_error).
 *
 * The callback functions will queue any replies for the client in
 * \ref to_client (using \ref send_to_client).
 *
 * \endif
 *
 * @return 0 success, -1 error, -2 too little space in \ref to_client,
 *         -4 XML syntax error.
 */
int
process_oap_client_input ()
{
  gboolean success;
  GError *error = NULL;

  if (xml_context == NULL)
    return -1;

  current_error = 0;
  success =
    g_markup_parse_context_parse (xml_context, from_client + from_client_start,
                                  from_client_end - from_client_start, &error);
  if (success == FALSE)
    {
      int err;
      if (error)
        {
          err = -4;
          if (g_error_matches
              (error, G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ELEMENT))
            tracef ("   client error: G_MARKUP_ERROR_UNKNOWN_ELEMENT\n");
          else
            if (g_error_matches
                (error, G_MARKUP_ERROR, G_MARKUP_ERROR_INVALID_CONTENT))
            {
              if (current_error)
                {
                  /* This is the return status for a forked child. */
                  forked = 2;   /* Prevent further forking. */
                  g_error_free (error);
                  return current_error;
                }
              tracef ("   client error: G_MARKUP_ERROR_INVALID_CONTENT\n");
            }
          else
            if (g_error_matches
                (error, G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ATTRIBUTE))
            tracef ("   client error: G_MARKUP_ERROR_UNKNOWN_ATTRIBUTE\n");
          else
            err = -1;
          g_message ("   Failed to parse client XML: %s\n", error->message);
          g_error_free (error);
        }
      else
        err = -1;
      /* In all error cases the caller must cease to call this function as it
       * would be too hard, if possible at all, to figure out the position of
       * start of the next command. */
      return err;
    }
  from_client_end = from_client_start = 0;
  /* The Manager returns a special value here if a fork happened, because
   * the caller needs to recreate the Scanner connection in the parent. */
  return 0;
}
