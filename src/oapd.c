/* OpenVAS Administrator
 * $Id$
 * Description: Module for OpenVAS Administrator: the OAP daemon.
 *
 * Authors:
 * Matthew Mundell <matthew.mundell@greenbone.net>
 *
 * Copyright:
 * Copyright (C) 2009 Greenbone Networks GmbH
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * or, at your option, any later version as published by the Free
 * Software Foundation
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file  oapd.c
 * @brief The OpenVAS Administrator OAP daemon.
 *
 * This file defines the OpenVAS Administrator Protocol (OAP) server for the
 * OpenVAS Administrator.
 *
 * The library provides two functions: \ref init_oapd and \ref serve_oap.
 * \ref init_oapd initialises the daemon.
 * \ref serve_oap serves OAP to a single client socket until end of file is
 * reached on the socket.
 */

#include "oapd.h"
#include "oxpd.h"
#include "logf.h"
#include "oap.h"
#include "tracef.h"

#include <assert.h>
#include <errno.h>
#include <string.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include <openvas/misc/openvas_server.h>

#ifdef S_SPLINT_S
/* FIX Weird that these are missing. */
/*@-exportheader@*/
int socket (int domain, int type, int protocol);
/*@=exportheader@*/
#endif

/**
 * @brief Seconds of client idleness before daemon closes client connection.
 */
#define CLIENT_TIMEOUT 900

/**
 * @brief File descriptor set mask: selecting on client read.
 */
#define FD_CLIENT_READ  1
/**
 * @brief File descriptor set mask: selecting on client write.
 */
#define FD_CLIENT_WRITE 2

/**
 * @brief Initialise the OAP library for the OAP daemon.
 *
 * @param[in]  log_config              Logging configuration.
 * @param[in]  users_dir               Directory containing user information.
 * @param[in]  nvt_sync_script         The script to use for nvt feed sync.
 * @param[in]  scap_sync_script        The script to use for scap feed sync.
 *                                     synchronization.
 * @param[in]  configuration_file      Configuration file.
 * @param[in]  enable_modify_settings  If true enable OAP MODIFY_SETTINGS.
 *
 * @return 0 success, -1 error, -2 database is wrong version.
 */
int
init_oapd (GSList * log_config, const gchar * users_dir,
           const gchar * nvt_sync_script, const gchar * scap_sync_script,
           const gchar * cert_sync_script, const gchar * configuration_file,
           gboolean enable_modify_settings)
{
  return init_oap (log_config, users_dir, nvt_sync_script, scap_sync_script,
                   cert_sync_script, configuration_file,
                   enable_modify_settings);
}

/**
 * @brief Read as much from the client as the \ref from_client buffer will hold.
 *
 * @param[in]  client_session  The TLS session with the client.
 * @param[in]  client_socket   The socket connected to the client.
 *
 * @return 0 on reading everything available, -1 on error, -2 if
 * from_client buffer is full or -3 on reaching end of file.
 */
static int
read_from_client (gnutls_session_t * client_session,
                  /*@unused@ */ int client_socket)
{
  while (from_client_end < from_buffer_size)
    {
      ssize_t count;
      count =
        gnutls_record_recv (*client_session, from_client + from_client_end,
                            from_buffer_size - from_client_end);
      if (count < 0)
        {
          if (count == GNUTLS_E_AGAIN)
            /* Got everything available, return to `select'. */
            return 0;
          if (count == GNUTLS_E_INTERRUPTED)
            /* Interrupted, try read again. */
            continue;
          if (count == GNUTLS_E_REHANDSHAKE)
            {
              /* \todo Rehandshake. */
              tracef ("   FIX should rehandshake\n");
              continue;
            }
          if (gnutls_error_is_fatal ((int) count) == 0
              && (count == GNUTLS_E_WARNING_ALERT_RECEIVED
                  || count == GNUTLS_E_FATAL_ALERT_RECEIVED))
            {
              int alert = gnutls_alert_get (*client_session);
              /*@dependent@ */
              const char *alert_name = gnutls_alert_get_name (alert);
              g_warning ("%s: TLS Alert %d: %s\n", __FUNCTION__, alert,
                         alert_name);
            }
          g_warning ("%s: failed to read from client: %s\n", __FUNCTION__,
                     gnutls_strerror ((int) count));
          return -1;
        }
      if (count == 0)
        /* End of file. */
        return -3;
      from_client_end += count;
    }

  /* Buffer full. */
  return -2;
}

// @todo As in the manager, this is similar to base/openvas_server_send.
/**
 * @brief Write as much as possible from \ref to_client to the client.
 *
 * @param[in]  client_session  The client session.
 *
 * @return 0 wrote everything, -1 error, -2 wrote as much as server accepted.
 */
static int
write_to_client (gnutls_session_t * client_session)
{
  while (to_client_start < to_client_end)
    {
      ssize_t count;
      count =
        gnutls_record_send (*client_session, to_client + to_client_start,
                            to_client_end - to_client_start);
      if (count < 0)
        {
          if (count == GNUTLS_E_AGAIN)
            /* Wrote as much as server would accept. */
            return -2;
          if (count == GNUTLS_E_INTERRUPTED)
            /* Interrupted, try write again. */
            continue;
          if (count == GNUTLS_E_REHANDSHAKE)
            /* \todo Rehandshake. */
            continue;
          g_warning ("%s: failed to write to client: %s\n", __FUNCTION__,
                     gnutls_strerror ((int) count));
          return -1;
        }
      logf ("=> client %.*s\n", to_client_end - to_client_start,
            to_client + to_client_start);
      to_client_start += count;
      tracef ("=> client  %u bytes\n", (unsigned int) count);
    }
  tracef ("=> client  done\n");
  to_client_start = to_client_end = 0;

  /* Wrote everything. */
  return 0;
}

/**
 * @brief Serve the OpenVAS Admin Protocol (OAP).
 *
 * Loop reading input from the socket, processing the input, and writing
 * any results back to the socket.  Exit the loop on reaching end of file
 * on the socket.
 *
 * \if STATIC
 *
 * Read input with \ref read_from_client.  Process the input with
 * \ref process_oap_client_input.  Write the results with
 * \ref write_to_client .
 *
 * \endif
 *
 * If compiled with logging (\ref LOG) then log all input and output
 * with \ref logf.
 *
 * @param[in]  client_session      The TLS session with the client.
 * @param[in]  client_socket       The socket connected to the client, if any.
 * @param[in]  client_credentials  The client credentials.
 *
 * @return 0 on success, -1 on error.
 */
/** @todo It's usually (socket, session). */
int
serve_oap (gnutls_session_t * client_session, int client_socket,
           gnutls_certificate_credentials_t * client_credentials)
{
  int nfds, ret;
  time_t last_client_activity_time;
  fd_set readfds, exceptfds, writefds;
  /* True if processing of the client input is waiting for space in the
   * to_client buffer. */
  short client_input_stalled;

  tracef ("   Serving OAP.\n");

  /* Initialise the XML parser. */
  init_oap_process ();

  /* Process any client input already read.  This is necessary because the
   * caller may have called read_protocol, which may have read an entire OAP
   * command.  If only one command was sent and the daemon selected here,
   * then the daemon would sit waiting for more input from the client
   * before processing the one command.
   */
  ret = process_oap_client_input ();
  if (ret == 0)
    /* Processed all input. */
    client_input_stalled = 0;
  else if (ret == 2)
    {
      /* Now in a process forked to run a sync, which has
       * successfully finished the sync (or where the sync was
       * already in progress).  Close the client
       * connection, as the parent process has continued the
       * session with the client. */
#if 0
      // FIX seems to close parent connections, maybe just do part of this
      openvas_server_free (client_socket, *client_session, *client_credentials);
#endif
      return 0;
    }
  else if (ret == -10)
    {
      /* Now in a process forked to run a sync, which has
       * failed in starting the sync. */
#if 0
      // FIX as above
      openvas_server_free (client_socket, *client_session, *client_credentials);
#endif
      return -1;
    }
  else if (ret == -1 || ret == -4)
    {
      /* Error.  Write rest of to_client to client, so that the
       * client gets any buffered output and the response to the
       * error. */
      write_to_client (client_session);
      openvas_server_free (client_socket, *client_session, *client_credentials);
      return -1;
    }
  else if (ret == -3)
    {
      /* to_client buffer full. */
      tracef ("   client input stalled 0\n");
      client_input_stalled = 2;
    }
  else
    {
      /* Programming error. */
      assert (0);
      client_input_stalled = 0;
    }

  /* Record the start time. */
  if (time (&last_client_activity_time) == -1)
    {
      g_warning ("%s: failed to get current time: %s\n", __FUNCTION__,
                 strerror (errno));
      openvas_server_free (client_socket, *client_session, *client_credentials);
      return -1;
    }

  /* Loop handling input from the socket.
   *
   * That is, select on the socket fd and then, as necessary
   *   - read from the client into buffer from_client
   *   - write to the client from buffer to_client.
   *
   * On reading from the fd, immediately try react to the input.  Call
   * process_oap_client_input, which parses OAP commands and may write
   * to_client.
   *
   * There are a few complications here
   *   - the program must read from or write to an fd returned by select
   *     before selecting on the fd again,
   *   - the program need only select on the fd for writing if there is
   *     something to write,
   *   - similarly, the program need only select on the fds for reading
   *     if there is buffer space available,
   *   - the buffer from_client can become full during reading.
   */

  nfds = 1 + client_socket;
  while (1)
    {
      int ret;
      struct timeval timeout;
      uint8_t fds;              /* What `select' is going to watch. */

      /* Setup for select. */

      fds = 0;
      FD_ZERO (&exceptfds);
      FD_ZERO (&readfds);
      FD_ZERO (&writefds);

      // FIX shutdown if any eg read fails

      timeout.tv_usec = 0;
      // FIX time check error
      timeout.tv_sec =
        CLIENT_TIMEOUT - (time (NULL) - last_client_activity_time);
      if (timeout.tv_sec <= 0)
        {
          tracef ("client timeout (1)\n");
          openvas_server_free (client_socket, *client_session,
                               *client_credentials);
          return 0;
        }
      else
        {
          FD_SET (client_socket, &exceptfds);
          if (from_client_end < from_buffer_size)
            {
              FD_SET (client_socket, &readfds);
              fds |= FD_CLIENT_READ;
            }
          if (to_client_start < to_client_end)
            {
              FD_SET (client_socket, &writefds);
              fds |= FD_CLIENT_WRITE;
            }
        }

      /* Select, then handle result.  Due to GNUTLS internal buffering
         we test for pending records first and emulate a select call
         in that case.  Note, that GNUTLS guarantees that writes are
         not buffered.  Note also that GNUTLS versions < 3 did not
         exhibit a problem in OpenVAS due to a different buffering
         strategy.  */
      ret = 0;
      if ((fds & FD_CLIENT_READ)
          && gnutls_record_check_pending (*client_session))
        {
          if (!ret)
            {
              FD_ZERO (&exceptfds);
              FD_ZERO (&readfds);
              FD_ZERO (&writefds);
            }
          ret++;
          FD_SET (client_socket, &readfds);
        }

      if (!ret)
        ret = select (nfds, &readfds, &writefds, &exceptfds, &timeout);
      if (ret < 0)
        {
          if (errno == EINTR)
            continue;
          g_warning ("%s: child select failed: %s\n", __FUNCTION__,
                     strerror (errno));
          openvas_server_free (client_socket, *client_session,
                               *client_credentials);
          return -1;
        }
      if (ret == 0)
        continue;

      if (FD_ISSET (client_socket, &exceptfds))
        {
          char ch;
          if (recv (client_socket, &ch, 1, MSG_OOB) < 1)
            {
              g_warning ("%s: after exception on client in child select:"
                         " recv failed\n", __FUNCTION__);
              openvas_server_free (client_socket, *client_session,
                                   *client_credentials);
              return -1;
            }
          g_warning ("%s: after exception on client in child select:"
                     " recv: %c\n", __FUNCTION__, ch);
        }

      if ((fds & FD_CLIENT_READ) == FD_CLIENT_READ
          && FD_ISSET (client_socket, &readfds))
        {
#if TRACE || LOG
          buffer_size_t initial_start = from_client_end;
#endif
          tracef ("   FD_CLIENT_READ\n");

          switch (read_from_client (client_session, client_socket))
            {
            case 0:            /* Read everything. */
              break;
            case -1:           /* Error. */
              openvas_server_free (client_socket, *client_session,
                                   *client_credentials);
              return -1;
            case -2:           /* from_client buffer full. */
              /* There may be more to read. */
              break;
            case -3:           /* End of file. */
              tracef ("   EOF reading from client.\n");
              openvas_server_free (client_socket, *client_session,
                                   *client_credentials);
              return 0;
            default:           /* Programming error. */
              assert (0);
            }

          if (time (&last_client_activity_time) == -1)
            {
              g_warning ("%s: failed to get current time (1): %s\n",
                         __FUNCTION__, strerror (errno));
              openvas_server_free (client_socket, *client_session,
                                   *client_credentials);
              return -1;
            }

#if TRACE || LOG
          /* This check prevents output in the "asynchronous network
           * error" case. */
          if (from_client_end > initial_start)
            {
              logf ("<= client %.*s\n", from_client_end - initial_start,
                    from_client + initial_start);
#if TRACE_TEXT
              if (g_strstr_len
                  (from_client + initial_start, from_client_end - initial_start,
                   "<password>"))
                tracef ("<= client  Input may contain password, suppressed.\n");
              else
                tracef ("<= client  \"%.*s\"\n",
                        from_client_end - initial_start,
                        from_client + initial_start);
#else
              tracef ("<= client  %i bytes\n", from_client_end - initial_start);
#endif
            }
#endif /* TRACE || LOG */

          ret = process_oap_client_input ();
          if (ret == 0)
            /* Processed all input. */
            client_input_stalled = 0;
          else if (ret == 2)
            {
              /* Now in a process forked to run a sync, which has
               * successfully finished the sync (or where the sync was
               * already in progress).  Close the client
               * connection, as the parent process has continued the
               * session with the client. */
#if 0
              // FIX seems to close parent connections, maybe just do part of this
              openvas_server_free (client_socket, *client_session,
                                   *client_credentials);
#endif
              return 0;
            }
          else if (ret == -10)
            {
              /* Now in a process forked to run a sync, which has
               * failed in starting the sync. */
#if 0
              // FIX as above
              openvas_server_free (client_socket, *client_session,
                                   *client_credentials);
#endif
              return -1;
            }
          else if (ret == -1 || ret == -4)
            {
              /* Error.  Write rest of to_client to client, so that the
               * client gets any buffered output and the response to the
               * error. */
              write_to_client (client_session);
              openvas_server_free (client_socket, *client_session,
                                   *client_credentials);
              return -1;
            }
          else if (ret == -3)
            {
              /* to_client buffer full. */
              tracef ("   client input stalled 2\n");
              client_input_stalled = 2;
              /* Break to write to_client. */
              break;
            }
          else
            /* Programming error. */
            assert (0);
        }

      if ((fds & FD_CLIENT_WRITE) == FD_CLIENT_WRITE
          && FD_ISSET (client_socket, &writefds))
        {
          /* Write as much as possible to the client. */

          switch (write_to_client (client_session))
            {
            case 0:            /* Wrote everything in to_client. */
              break;
            case -1:           /* Error. */
              openvas_server_free (client_socket, *client_session,
                                   *client_credentials);
              return -1;
            case -2:           /* Wrote as much as client was willing to accept. */
              break;
            default:           /* Programming error. */
              assert (0);
            }

          if (time (&last_client_activity_time) == -1)
            {
              g_warning ("%s: failed to get current time (2): %s\n",
                         __FUNCTION__, strerror (errno));
              openvas_server_free (client_socket, *client_session,
                                   *client_credentials);
              return -1;
            }
        }

      if (client_input_stalled)
        {
          /* Try process the client input, in case writing to the client
           * has freed some space in to_client. */

          ret = process_oap_client_input ();
          if (ret == 0)
            /* Processed all input. */
            client_input_stalled = 0;
          else if (ret == -1)
            {
              /* Error.  Write rest of to_client to client, so that the
               * client gets any buffered output and the response to the
               * error. */
              write_to_client (client_session);
              openvas_server_free (client_socket, *client_session,
                                   *client_credentials);
              return -1;
            }
          else if (ret == -3)
            {
              /* to_client buffer full. */
              tracef ("   client input still stalled (2)\n");
              client_input_stalled = 2;
            }
          else
            /* Programming error. */
            assert (0);
        }

      /* Check if client connection is out of time. */
      {
        time_t current_time;
        if (time (&current_time) == -1)
          {
            g_warning ("%s: failed to get current time (3): %s\n", __FUNCTION__,
                       strerror (errno));
            openvas_server_free (client_socket, *client_session,
                                 *client_credentials);
            return -1;
          }
        if (last_client_activity_time - current_time >= CLIENT_TIMEOUT)
          {
            tracef ("client timeout (1)\n");
            openvas_server_free (client_socket, *client_session,
                                 *client_credentials);
            return 0;
          }
      }

    }                           /* while (1) */

  openvas_server_free (client_socket, *client_session, *client_credentials);
  return 0;
}
