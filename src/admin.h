/* OpenVAS Administrator
 * $Id$
 * Description: Headers for OpenVAS Administrator: the Admin library.
 *
 * Authors:
 * Matthew Mundell <matthew.mundell@greenbone.net>
 *
 * Copyright:
 * Copyright (C) 2009 Greenbone Networks GmbH
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * or, at your option, any later version as published by the Free
 * Software Foundation
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef _OPENVAS_ADMINISTRATOR_ADMIN_H
#define _OPENVAS_ADMINISTRATOR_ADMIN_H

#include <glib.h>
#include <openvas/base/array.h>

/* For feed syncing */
#define NVT_FEED 1
#define SCAP_FEED 2
#define CERT_FEED 3

typedef struct
{
  gchar *name;
  gchar *value;
} admin_setting_t;

GSList *openvas_admin_list_users (const gchar *, int, const gchar *);
int openvas_admin_user_access (const gchar *, gchar **, int *, const gchar *);

int openvas_admin_add_user (const gchar *, const gchar *, const gchar *,
                            const gchar *, int, const gchar *, const array_t *,
                            gchar **);
int openvas_admin_modify_user (const gchar *, const gchar *, const gchar *,
                               const gchar *, int, const gchar *,
			       const array_t *,
                               gchar **);
int openvas_admin_remove_user (const gchar *, const gchar *);
gboolean openvas_admin_set_rules (const gchar *, const gchar *, const gchar *);

int openvas_sync_feed (const gchar *, const gchar *, int);
int openvas_current_sync (const gchar *, gchar **, gchar **);
gboolean openvas_sync_script_perform_selftest (const gchar *, gchar **);
gboolean openvas_get_sync_script_identification (const gchar *, gchar **, int);
gboolean openvas_get_sync_script_description (const gchar *, gchar **);
gboolean openvas_get_sync_script_feed_version (const gchar *, gchar **);

GString *print_users_xml (GSList *);
GString *print_users_text (GSList *);

int openvas_admin_modify_settings (const char *, const char *, const array_t *);

int openvas_admin_schema (gchar *, gchar **, gsize *, gchar **, gchar **);

#endif /* not _OPENVAS_ADMINISTRATOR_ADMIN_H */
