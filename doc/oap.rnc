#### OpenVAS Administrator Protocol (OAP)
####
#### Version: 1.1
####
#### The "administrator" protocol of the Open Vulnerability Assessment System.

### Preamble

start = command | response

command
  = authenticate
    | commands
    | create_user
    | delete_user
    | describe_auth
    | describe_feed
    | describe_scap
    | describe_cert
    | get_settings
    | get_users
    | get_version
    | help
    | modify_auth
    | modify_user
    | sync_feed
    | sync_scap
    | sync_cert

response
  = authenticate_response
    | commands_response
    | create_user_response
    | delete_user_response
    | describe_auth_response
    | describe_feed_response
    | describe_scap_response
    | describe_cert_response
    | get_settings_response
    | get_users_response
    | get_version_response
    | help_response
    | modify_auth_response
    | modify_user_response
    | sync_feed_response
    | sync_scap_response
    | sync_cert_response

### Data Types

# Base64 encoded data.
base64 = xsd:base64Binary
# A true or false value.
boolean = text
# A true or false value, after conversion to an integer.
boolean_atoi = text
# A date and time, in the C `ctime' format.
ctime = text
# A name of a data type.
type_name = xsd:string - xsd:whitespace
# An integer.
integer = xsd:integer
# A string that may include the characters h, m, l, g and d.
levels = xsd:token { pattern = "h?m?l?g?d?" }
# A name.
name = xsd:string
# An Object Identifier (OID).
oid = xsd:token { pattern = "[0-9\.]{1,80}" }
# A string describing an order for sorting.
sort_order = text
# The success or failure status of a command.
status = xsd:token { pattern = "200|201|202|400|401|403|404|409|500|503" }
# A task run status.
task_status = xsd:token { pattern = "Delete Requested|Done|New|Pause Requested|Paused|Requested|Resume Requested|Running|Stop Requested|Stopped|Internal Error" }
# The trend of results for a task.
task_trend = xsd:token { pattern = "up|down|more|less|same" }
# A threat level.
threat = xsd:token { pattern = "High|Medium|Low|Log|Debug" }
# A unit of time.
time_unit = xsd:token { pattern = "second|minute|hour|day|week|month|year|decade" }
# A Universally Unique Identifier (UUID).
uuid = xsd:token { pattern = "[0-9abcdefABCDEF\-]{1,40}" }
# A Universally Unique Identifier (UUID), or the empty string.
uuid_or_empty = xsd:token { pattern = "[0-9abcdefABCDEF\-]{0,40}" }

### Element Types

## Element Type c
##
## A reference to a command.

c
 = element c
     {
       text
     }

## Element Type e
##
## A reference to an element.

e
 = element e
     {
       text
     }

## Element Type r
##
## A reference to a response.

r
 = element r
     {
       text
     }

## Element Type o
##
## An optional pattern item.

o
 = element o
     {
       ( c
           | e
           | r
           | g )
     }

## Element Type g
##
## A group of pattern items.

g
 = element g
     {
       c*
       & e*
       & r*
       & o*
       & any*
     }

## Element Type any
##
## Pattern item indicating "any number of".

any
 = element any
     {
       ( c
           | e
           | r
           | g )
     }

## Element Type pattern
##
## The pattern element of a command or command descendant.

pattern
 = element pattern
     {
       text
       & pattern_attrib*
       & c*
       & e*
       & r*
       & o*
       & g*
       & any*
       & pattern_t?
     }

# An attribute.
pattern_attrib
 = element attrib
     {
       pattern_attrib_name
       & pattern_attrib_type
       & pattern_attrib_required
     }

pattern_attrib_name
 = element name
     {
       type_name
     }

pattern_attrib_type
 = element type
     {
       type_name
     }

pattern_attrib_required
 = element required
     {
       boolean
     }

# The type of the text of the element.
pattern_t
 = element t
     {
       type_name
     }

## Element Type command_definition
##
## Definition of a command in OAP describing HELP command.

command_definition
 = element command_definition
     {
       command_definition_name
       & command_definition_summary?
       & command_definition_description?
       & ( ( command_definition_pattern
           & command_definition_response )
           | ( command_definition_type
           & command_definition_ele* ) )
       & command_definition_example*
     }

# The name of the command.
command_definition_name
 = element name
     {
       type_name
     }

# A summary of the command.
command_definition_summary
 = element summary
     {
       text
     }

# A description of the command.
command_definition_description
 = element description
     {
       text
       & command_definition_description_p*
     }

# A paragraph.
command_definition_description_p
 = element p
     {
       text
     }

command_definition_pattern
 = element pattern    # type pattern
     {
       text
       & pattern_attrib*
       & c*
       & e*
       & r*
       & o*
       & g*
       & any*
       & pattern_t?
     }

command_definition_response
 = element response    # type pattern
     {
       text
       & pattern_attrib*
       & c*
       & e*
       & r*
       & o*
       & g*
       & any*
       & pattern_t?
     }

command_definition_example
 = element example
     {
       command_definition_example_summary?
       & command_definition_example_description?
       & command_definition_example_request
       & command_definition_example_response
     }

command_definition_example_summary
 = element summary
     {
       text
     }

command_definition_example_description
 = element description
     {
       text
     }

# Example request.
command_definition_example_request
 = element request
     {
       text
     }

# Response to example request.
command_definition_example_response
 = element response
     {
       text
     }

# The type of the element.
command_definition_type
 = element type
     {
       text
     }

# An element.
command_definition_ele
 = element ele    # type command_definition
     {
       command_definition_name
       & command_definition_summary?
       & command_definition_description?
       & ( ( command_definition_pattern
           & command_definition_response )
           | ( command_definition_type
           & command_definition_ele* ) )
       & command_definition_example*
     }

## Element Type sources
##
## List of authentication sources associated to a user.

sources
 = element sources
     {
       sources_source*
     }

# The name of the authentication source.
sources_source
 = element source
     {
       text
     }

### Commands

## Command authenticate
##
## Authenticate with the Administrator.

authenticate
 = element authenticate
     {
       authenticate_credentials
     }

authenticate_credentials
 = element credentials
     {
       authenticate_credentials_username
       & authenticate_credentials_password
     }

# The login name of the user.
authenticate_credentials_username
 = element username
     {
       text
     }

# The user's password.
authenticate_credentials_password
 = element password
     {
       text
     }

## Command commands
##
## Run a list of commands.

commands
 = element commands
     {
       ( authenticate
           | commands
           | create_user
           | delete_user
           | describe_auth
           | describe_feed
           | describe_scap
           | describe_cert
           | get_settings
           | get_users
           | get_version
           | help
           | modify_auth
           | modify_user
           | sync_feed
           | sync_scap
           | sync_cert )*
     }

## Command create_user
##
## Create a user.

create_user
 = element create_user
     {
       create_user_name
       & create_user_hosts?
       & create_user_password?
       & create_user_role?
     }

# The name of the user to be created.
create_user_name
 = element name
     {
       text
     }

# User access rules: a textual list of hosts.
create_user_hosts
 = element hosts
     {
       text
       & attribute allow { boolean }?
     }

# The password for the user.
create_user_password
 = element password
     {
       text
     }

# The role of the user.
create_user_role
 = element role
     {
       xsd:token { pattern = "Administrator|User|Observer" }
     }

## Command delete_user
##
## Delete a user.

delete_user
 = element delete_user
     {
       delete_user_name
     }

# The name of the user to be deleted.
delete_user_name
 = element name
     {
       text
     }

## Command describe_auth
##
## Describe a auth.

describe_auth
 = element describe_auth
     {
       ""
     }

## Command describe_feed
##
## Describe the NVT feed.

describe_feed
 = element describe_feed
     {
       ""
     }

## Command describe_scap
##
## Describe the SCAP feed.

describe_scap
 = element describe_scap
     {
       ""
     }

## Command describe_cert
##
## Describe the CERT feed.

describe_cert
 = element describe_cert
     {
       ""
     }

## Command get_settings
##
## Get all settings.

get_settings
 = element get_settings
     {
       ""
     }

## Command get_users
##
## Get all users.

get_users
 = element get_users
     {
       ""
     }

## Command get_version
##
## Get the OpenVAS Administrator Protocol version.

get_version
 = element get_version
     {
       ""
     }

## Command help
##
## Get the help text.

help
 = element help
     {
       # Required help format.
       attribute format { xsd:token { pattern = "html|HTML|rnc|RNC|text|TEXT|xml|XML" } }?
     }

## Command modify_auth
##
## Modify the authentication methods.

modify_auth
 = element modify_auth
     {
       modify_auth_group
     }

modify_auth_group
 = element group
     {
       modify_auth_group_auth_conf_setting*
     }

# A setting to modify.
modify_auth_group_auth_conf_setting
 = element auth_conf_setting
     {
       attribute key { text }
       & attribute value { text }
     }

## Command modify_user
##
## Modify a user.

modify_user
 = element modify_user
     {
       modify_user_name
       & modify_user_password?
       & modify_user_role
       & modify_user_hosts?
       & modify_user_sources?
     }

# The name of the user to be modified.
modify_user_name
 = element name
     {
       text
     }

# User access rules: a textual list of hosts.
modify_user_hosts
 = element hosts
     {
       text
       & attribute allow { boolean }?
     }

# The password for the user.
modify_user_password
 = element password
     {
       text
     }

# The role of the user.
modify_user_role
 = element role
     {
       xsd:token { pattern = "Administrator|User|Observer" }
     }

# List of authentication sources for this user (if omitted, no changes).
modify_user_sources
 = element sources    # type sources
     {
       sources_source*
     }

## Command sync_feed
##
## Synchronize with an NVT feed.

sync_feed
 = element sync_feed
     {
       ""
     }

## Command sync_scap
##
## Synchronize with a SCAP feed.

sync_scap
 = element sync_scap
     {
       ""
     }

## Command sync_cert
##
## Synchronize with a CERT feed.

sync_cert
 = element sync_cert
     {
       ""
     }

### Responses

## Response to authenticate

authenticate_response
 = element authenticate_response
     {
       attribute status { status }
       & attribute status_text { text }
     }

## Response to commands

commands_response
 = element commands_response
     {
       attribute status { status }
       & attribute status_text { text }
       & ( authenticate_response
           | commands_response
           | create_user_response
           | delete_user_response
           | describe_auth_response
           | describe_feed_response
           | describe_scap_response
           | describe_cert_response
           | get_settings_response
           | get_users_response
           | get_version_response
           | help_response
           | modify_auth_response
           | modify_user_response
           | sync_feed_response
           | sync_scap_response
           | sync_cert_response )*
     }

## Response to create_user

create_user_response
 = element create_user_response
     {
       attribute status { status }
       & attribute status_text { text }
       & attribute id { uuid }
     }

## Response to delete_user

delete_user_response
 = element delete_user_response
     {
       attribute status { status }
       & attribute status_text { text }
     }

## Response to describe_auth

describe_auth_response
 = element describe_auth_response
     {
       attribute status { status }
       & attribute status_text { text }
       & describe_auth_response_group
     }

# Config group.
describe_auth_response_group
 = element group
     {
       attribute name { text }
       & describe_auth_response_group_auth_conf_setting
     }

describe_auth_response_group_auth_conf_setting
 = element auth_conf_setting
     {
       attribute key { text }
       & attribute value { text }
     }

## Response to describe_feed

describe_feed_response
 = element describe_feed_response
     {
       attribute status { status }
       & attribute status_text { text }
       & describe_feed_response_feed
     }

describe_feed_response_feed
 = element feed
     {
       describe_feed_response_feed_name
       & describe_feed_response_feed_version
       & describe_feed_response_feed_description
     }

# The name of the feed.
describe_feed_response_feed_name
 = element name
     {
       text
     }

# The version of the feed.
describe_feed_response_feed_version
 = element version
     {
       text
     }

# A description of the feed.
describe_feed_response_feed_description
 = element description
     {
       text
     }

## Response to describe_scap

describe_scap_response
 = element describe_scap_response
     {
       attribute status { status }
       & attribute status_text { text }
       & describe_scap_response_scap
     }

describe_scap_response_scap
 = element scap
     {
       describe_scap_response_scap_name
       & describe_scap_response_scap_version
       & describe_scap_response_scap_description
     }

# The name of the scap feed.
describe_scap_response_scap_name
 = element name
     {
       text
     }

# The version of the scap feed.
describe_scap_response_scap_version
 = element version
     {
       text
     }

# A description of the scap feed.
describe_scap_response_scap_description
 = element description
     {
       text
     }

## Response to describe_cert

describe_cert_response
 = element describe_cert_response
     {
       attribute status { status }
       & attribute status_text { text }
       & describe_cert_response_cert
     }

describe_cert_response_cert
 = element cert
     {
       describe_cert_response_cert_name
       & describe_cert_response_cert_version
       & describe_cert_response_cert_description
     }

# The name of the CERT feed.
describe_cert_response_cert_name
 = element name
     {
       text
     }

# The version of the CERT feed.
describe_cert_response_cert_version
 = element version
     {
       text
     }

# A description of the CERT feed.
describe_cert_response_cert_description
 = element description
     {
       text
     }

## Response to get_settings

get_settings_response
 = element get_settings_response
     {
       attribute status { status }
       & attribute status_text { text }
       & get_settings_response_scanner_settings
     }

get_settings_response_scanner_settings
 = element scanner_settings
     {
       get_settings_response_scanner_settings_setting*
     }

# The name of the setting.
get_settings_response_scanner_settings_setting
 = element setting
     {
       text
       & attribute name { text }
     }

## Response to get_users

get_users_response
 = element get_users_response
     {
       attribute status { status }
       & attribute status_text { text }
       & get_users_response_user*
     }

get_users_response_user
 = element user
     {
       get_users_response_user_name
       & get_users_response_user_role
       & get_users_response_user_hosts
       & get_users_response_user_sources
     }

# The name of the user.
get_users_response_user_name
 = element name
     {
       text
     }

# The role of the user.
get_users_response_user_role
 = element role
     {
       xsd:token { pattern = "Admin|User|Observer" }
     }

# Host access rule for the user.
get_users_response_user_hosts
 = element hosts
     {
       text
       & # 0 forbidden, 1 allowed, 2 all allowed, 3 custom.
       attribute allow { xsd:token { pattern = "0|1|2|3" } }
     }

# Sources allowed for authentication for this user.
get_users_response_user_sources
 = element sources    # type sources
     {
       sources_source*
     }

## Response to get_version

get_version_response
 = element get_version_response
     {
       attribute status { status }
       & attribute status_text { text }
       & get_version_response_version
     }

get_version_response_version
 = element version
     {
       text
     }

## Response to help

help_response
 = element help_response
     {
       text
       & attribute status { status }
       & attribute status_text { text }
       & help_response_schema?
     }

help_response_schema
 = element schema
     {
       help_response_schema_protocol
     }

help_response_schema_protocol
 = element protocol
     {
       help_response_schema_protocol_name
       & help_response_schema_protocol_abbreviation?
       & help_response_schema_protocol_summary?
       & help_response_schema_protocol_version?
       & help_response_schema_protocol_type*
       & help_response_schema_protocol_command*
     }

# The full name of the protocol.
help_response_schema_protocol_name
 = element name
     {
       text
     }

# The abbreviated name of the protocol.
help_response_schema_protocol_abbreviation
 = element abbreviation
     {
       text
     }

# A summary of the protocol.
help_response_schema_protocol_summary
 = element summary
     {
       text
     }

# The version number of the protocol.
help_response_schema_protocol_version
 = element version
     {
       text
     }

# A data type.
help_response_schema_protocol_type
 = element type
     {
       help_response_schema_protocol_type_name
       & help_response_schema_protocol_type_summary?
       & help_response_schema_protocol_type_description?
       & help_response_schema_protocol_type_pattern
     }

# The name of the data type.
help_response_schema_protocol_type_name
 = element name
     {
       type_name
     }

# A summary of the data type.
help_response_schema_protocol_type_summary
 = element summary
     {
       text
     }

# A description of the data type.
help_response_schema_protocol_type_description
 = element description
     {
       text
       & help_response_schema_protocol_type_description_p*
     }

# A paragraph.
help_response_schema_protocol_type_description_p
 = element p
     {
       text
     }

# The RNC pattern for the data type.
help_response_schema_protocol_type_pattern
 = element pattern
     {
       text
     }

# A command.
help_response_schema_protocol_command
 = element command    # type command_definition
     {
       command_definition_name
       & command_definition_summary?
       & command_definition_description?
       & ( ( command_definition_pattern
           & command_definition_response )
           | ( command_definition_type
           & command_definition_ele* ) )
       & command_definition_example*
     }

## Response to modify_auth

modify_auth_response
 = element modify_auth_response
     {
       attribute status { status }
       & attribute status_text { text }
     }

## Response to modify_user

modify_user_response
 = element modify_user_response
     {
       attribute status { status }
       & attribute status_text { text }
     }

## Response to sync_feed

sync_feed_response
 = element sync_feed_response
     {
       attribute status { status }
       & attribute status_text { text }
     }

## Response to sync_scap

sync_scap_response
 = element sync_scap_response
     {
       attribute status { status }
       & attribute status_text { text }
     }

## Response to sync_cert

sync_cert_response
 = element sync_cert_response
     {
       attribute status { status }
       & attribute status_text { text }
     }
