<?xml version="1.0" standalone='no'?>
<!DOCTYPE manpage SYSTEM "/usr/share/xmltoman/xmltoman.dtd">
<?xml-stylesheet type="text/xsl" href="/usr/share/xmltoman/xmltoman.xsl" ?>

<!-- OpenVAS
 $Id$
 Description: openvasad manpage

 Authors:
 Matthew Mundell <matthew.mundell@greenbone.net>
 Michael Wiegand <michael.wiegand@greenbone.net>

 Copyright:
 Copyright (C) 2011 Greenbone Networks GmbH

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 2,
 or, at your option, any later version as published by the Free
 Software Foundation

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
-->

<manpage name="openvasad" section="8" desc="Administrator daemon of the Open Vulnerability Assessment System (OpenVAS)">

  <synopsis>
    <cmd>openvasad OPTIONS</cmd>
  </synopsis>

  <description>
    <p>
      The core task of OpenVAS Scanner (openvassd(8)) within the Open
      Vulnerability Assessment System (OpenVAS) is the actual scan
      process.
    </p>

    <p>
      The OpenVAS Administrator is intended to simplify the
      configuration and administration of an OpenVAS Scanner both on a
      local installation as well as on a remote system by supplying
      functionality for user management and feed management. It can
      run as a daemon as well a standalone commandline interface.
    </p>

    <p>
      When run as a daemon, OpenVAS Administrator will serve clients
      using the OpenVAS Administrator Protocol (OAP).
    </p>
  </description>

  <options>
    <option>
      <p><opt>-a, --listen=<arg>ADDRESS</arg></opt></p>
      <optdesc><p>Listen on ADDRESS.</p></optdesc>
    </option>

    <option>
      <p><opt>-p, --port=<arg>NUMBER</arg></opt></p>
      <optdesc>
        <p>Listen on port number NUMBER.</p>
      </optdesc>
    </option>

    <option>
      <p><opt>-f, --foreground</opt></p>
      <optdesc><p>Run as daemon in the foreground.</p></optdesc>
    </option>

    <option>
      <p><opt>-c, --command=<arg>COMMAND</arg></opt></p>
      <optdesc>
        <p>Execute OAP command COMMAND (e.g. add_user, remove_user, list_users).</p>
      </optdesc>
    </option>

    <option>
      <p><opt>-u, --username=<arg>NAME</arg></opt></p>
      <optdesc>
        <p>Username when creating, editing or removing a user.</p>
      </optdesc>
    </option>

    <option>
      <p><opt>-w, --password=<arg>PASSWORD</arg></opt></p>
      <optdesc>
        <p>
          Password for the new user when creating a new user. If the
          password is not supplied on the command line, it will be
          requested interactively.
        </p>
      </optdesc>
    </option>

    <option>
      <p><opt>-t, --account=<arg>NAME:PASSWORD</arg></opt></p>
      <optdesc>
        <p>Username and password for new user (overrides -n and -w).</p>
      </optdesc>
    </option>

    <option>
      <p><opt>-r, --role=<arg>ROLE</arg></opt></p>
      <optdesc>
        <p>
          Role when creating or modifying a user. Must be either User
          or Admin.
        </p>
      </optdesc>
    </option>

    <option>
      <p><opt>--rules-file=<arg>FILE</arg></opt></p>
      <optdesc>
        <p>File containing the rules for the user.</p>
      </optdesc>
    </option>

    <option>
      <p><opt>--users-dir=<arg>DIRECTORY</arg></opt></p>
      <optdesc>
        <p>Directory containing the OpenVAS user data.</p>
      </optdesc>
    </option>

    <option>
      <p><opt>--scanner-config-file=<arg>FILE</arg></opt></p>
      <optdesc>
        <p>File containing supplemental OpenVAS Scanner configuration.</p>
      </optdesc>
    </option>

    <option>
      <p><opt>-s, --sync-script=<arg>FILE</arg></opt></p>
      <optdesc>
        <p>Script to use for NVT feed synchronization.</p>
      </optdesc>
    </option>

    <option>
      <p><opt>-F, --feed-version</opt></p>
      <optdesc>
        <p>Print version of the installed NVT feed.</p>
      </optdesc>
    </option>

    <option>
      <p><opt>-S, --sync-feed</opt></p>
      <optdesc>
        <p>Synchronize the installed NVT feed.</p>
      </optdesc>
    </option>

    <option>
      <p><opt>-T, --print-sync-status</opt></p>
      <optdesc>
        <p>Print the synchronization status of the installed NVT feed.</p>
      </optdesc>
    </option>

    <option>
      <p><opt>--enable-modify-settings</opt></p>
      <optdesc>
        <p>Enable the OAP MODIFY_SETTINGS command.</p>
      </optdesc>
    </option>

    <option>
      <p><opt>-v, --verbose</opt></p>
      <optdesc><p>Verbose messages.</p></optdesc>
    </option>

    <option>
      <p><opt>-V, --version</opt></p>
      <optdesc><p>Print version.</p></optdesc>
    </option>

    <option>
      <p><opt>-?, --help</opt></p>
      <optdesc><p>Show help.</p></optdesc>
    </option>
  </options>

  <section name="EXAMPLES">
    <p>openvasad --port 9393</p>
    <p>Serve OMP clients on port 9393.</p>
  </section>

  <section name="SEE ALSO">
    <p>
      <manref name="openvassd" section="8"/>
      <manref name="openvasmd" section="8"/>
      <manref name="gsad" section="8"/>
      <manref name="omp" section="8"/>
      <manref name="gsd" section="8"/>
    </p>
  </section>

  <section name="MORE INFORMATION ABOUT THE OpenVAS PROJECT">
    <p>
      The canonical places where you will find more information
      about the OpenVAS project are:

      <url href="http://www.openvas.org/"/>
        (Official site)

      <url href="http://wald.intevation.org/projects/openvas/"/>
        (Development Platform)
    </p>
  </section>

  <section name="COPYRIGHT">
    <p>
      The OpenVAS Administrator is released under the GNU GPL, version
      2, or, at your option, any later version.
    </p>
  </section>

</manpage>
